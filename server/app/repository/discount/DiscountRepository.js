/**
* This module represents a repository for the collection Discount
* @module repository/discount/DiscountRepository
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepository_DiscountRepository_init) ENABLED START */

var logger = console;
var mongoose = require('mongoose');

var Q = require('q');

var ObjectId = require('mongoose').Types.ObjectId;
/* PROTECTED REGION END */

var Discount = mongoose.model('Discount');


/**
 * Returns all discounts with pagination
* @param {Array} optional optional params for the query (offset, limit, id, name, brand, branch etc)
* @returns {Discount} 
 */
module.exports.getAllDiscounts = function(optional) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepository_DiscountRepository__Operation_getAllDiscounts_body) ENABLED START */
    var limit = optional.limit;
    var offset = optional.offset;
    var orderBy = optional.orderBy;
    var orderType = optional.orderType;

    if (typeof limit === 'undefined' || limit.length == 0 || parseInt(limit) > 100) {
        limit = 10;
    }

    var searchParams = {};

    if (typeof  optional._id != 'undefined') {
        searchParams._id = new ObjectId(optional._id);
    }

    if (typeof  optional.category != 'undefined') {
        searchParams.category = new ObjectId(optional.category);
    }

    if (typeof  optional.categoryName != 'undefined') {
        var regex = new RegExp(optional.categoryName, 'i');
        searchParams['categoryInfo.name'] = regex;
    }

    if (typeof  optional.subcategory != 'undefined') {
        searchParams.subcategory = new ObjectId(optional.subcategory);
    }

    if (typeof  optional.subcategoryName != 'undefined') {
        var regex = new RegExp(optional.subcategoryName, 'i');
        searchParams['subcategoryInfo.name'] = regex;
    }

    if (typeof  optional.brand != 'undefined') {
        searchParams.brand = new ObjectId(optional.brand);
    }

    if (typeof  optional.brandName != 'undefined') {
        var regex = new RegExp(optional.brandName, 'i');
        searchParams['brandInfo.name'] = regex;
    }

    if (typeof  optional.branch != 'undefined') {
        searchParams.branch = new ObjectId(optional.branch);
    }

    if (typeof  optional.branchName != 'undefined') {
        var regex = new RegExp(optional.branchName, 'i');
        searchParams['branchInfo.name'] = regex;
    }

    if (typeof  optional.state != 'undefined') {
        searchParams['branchInfo.state'] = new ObjectId(optional.state);
    }

    if (typeof  optional.zone != 'undefined') {
        searchParams['branchInfo.zone'] = new ObjectId(optional.zone);
    }

    if (typeof  optional.colonyName != 'undefined') {
        var regex = new RegExp(optional.colonyName, 'i');
        searchParams['branchInfo.colony'] = regex;
    }

    var count = Discount.count(searchParams);

    var countPromise = count.count().exec();

    var result = {};

    return countPromise.then(function (countResult) {

        result.count = countResult;

        var find = Discount.find(searchParams).populate('brand').populate('branch').populate('category').populate('subcategory');

        if (typeof  limit !== 'undefined') {
            find = find.limit(limit);
        }

        if (typeof  offset !== 'undefined' && parseInt(offset) > 0) {
            find = find.skip(offset);
        }

        if (typeof  orderBy !== 'undefined') {
            var sort = {};
            if (orderType == 1) {
                sort[orderBy] = 1;
                find = find.sort(sort);
            } else {
                sort[orderBy] = -1;
                find = find.sort(sort);
            }
        }

        return find.exec();

    }).then(function (resultList) {
        result.list = resultList;

        var deferred = Q.defer();

        deferred.resolve(result);

        return deferred.promise;

    });
    /* PROTECTED REGION END */
};

/**
 * Returns all near by discounts with pagination
* @param {Array} optional optional params for the query (offset, limit, id, name, brand, branch etc)
* @returns {Discount} 
 */
module.exports.getAllNearByDiscounts = function(optional) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepository_DiscountRepository__Operation_getAllNearByDiscounts_body) ENABLED START */

    var limit = optional.limit;

    if (typeof limit === 'undefined' || limit.length == 0 || parseInt(limit) > 100) {
        limit = 10;
    }

    if (typeof optional.minDistance == 'undefined') {
        optional.minDistance = 0;
    }

    var searchParams = {};

    if (typeof  optional._id != 'undefined') {
        searchParams._id = new ObjectId(optional._id);
    }

    if (typeof  optional.category != 'undefined' && optional.category != null && optional.category.length > 0) {
        searchParams.category = new ObjectId(optional.category);
    }

    if (typeof  optional.categoryName != 'undefined') {
        var regex = new RegExp(optional.categoryName, 'i');
        searchParams['categoryInfo.name'] = regex;
    }

    if (typeof  optional.subcategory != 'undefined' && optional.subcategory != null && optional.subcategory.length > 0) {
        searchParams.subcategory = new ObjectId(optional.subcategory);
    }

    if (typeof  optional.subcategoryName != 'undefined') {
        var regex = new RegExp(optional.subcategoryName, 'i');
        searchParams['subcategoryInfo.name'] = regex;
    }

    if (typeof  optional.brand != 'undefined') {
        searchParams.brand = new ObjectId(optional.brand);
    }

    if (typeof  optional.brandName != 'undefined') {
        var regex = new RegExp(optional.brandName, 'i');
        searchParams['brandInfo.name'] = regex;
    }

    if (typeof  optional.branch != 'undefined') {
        searchParams.branch = new ObjectId(optional.branch);
    }

    if (typeof  optional.branchName != 'undefined') {
        var regex = new RegExp(optional.branchName, 'i');
        searchParams['branchInfo.name'] = regex;
    }

    if (typeof  optional.state != 'undefined' && optional.state != null && optional.state.length > 0) {
        searchParams['branchInfo.state'] = new ObjectId(optional.state);
    }

    if (typeof  optional.zone != 'undefined' && optional.zone != null && optional.zone.length > 0) {
        searchParams['branchInfo.zone'] = new ObjectId(optional.zone);
    }

    if (typeof  optional.colonyName != 'undefined') {
        var regex = new RegExp(optional.colonyName, 'i');
        searchParams['branchInfo.colony'] = regex;
    }

    var options = {
        num: parseFloat(limit),
        minDistance: parseFloat(optional.minDistance / 6371),
        distanceMultiplier: 6371,
        spherical: true,
        query: searchParams
    };

    if (typeof optional.maxDistance != 'undefined') {
        options.maxDistance = parseFloat(optional.maxDistance / 6371);
    }

    return Discount.geoNear([parseFloat(optional.longitude), parseFloat(optional.latitude)], options);
    /* PROTECTED REGION END */
};

/**
 * creates a new discount
* @param {Discount} discount the discount to be created
* @returns {Discount} 
 */
module.exports.createDiscount = function(discount) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepository_DiscountRepository__Operation_createDiscount_body) ENABLED START */
    var promise = Discount.create(discount);
    return promise;
    /* PROTECTED REGION END */
};

/**
 * updates a discount
* @param {string} id the _id of the discount
* @param {Discount} discount the discount to be updated
* @returns {Discount} 
 */
module.exports.updateDiscount = function(id, discount) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepository_DiscountRepository__Operation_updateDiscount_body) ENABLED START */
    var promise = Discount.findByIdAndUpdate(id, discount).exec();
    return promise;
    /* PROTECTED REGION END */
};

/**
 * Returns the discount according to the _id
* @param {string} id the _id of the discount to be retrieved
* @returns {Discount} 
 */
module.exports.getDiscountById = function(id) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepository_DiscountRepository__Operation_getDiscountById_body) ENABLED START */
    var promise = Discount.findOne({_id: id}).populate('brand').populate('branch').populate('category').populate('subcategory').exec();
    return promise;
    /* PROTECTED REGION END */
};

/**
 * returns a discount by branch
* @param {string} branch the branch
* @returns {Discount} 
 */
module.exports.getDiscountByBranch = function(branch) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepository_DiscountRepository__Operation_getDiscountByBranch_body) ENABLED START */
    var promise = Discount.findOne({branch: branch}).exec();
    return promise;
    /* PROTECTED REGION END */
};

/**
 * Mark all discount as updated or not updated
* @param {boolean} updated the updated property to mark every discount
* @returns {Discount} 
 */
module.exports.markAllUpdated = function(updated) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepository_DiscountRepository__Operation_markAllUpdated_body) ENABLED START */
    var promise = Discount.update({}, {updated: updated}, {multi: true}).exec();
    return promise;
    /* PROTECTED REGION END */
};

/**
 * Remove all discounts with the specified updated property
* @param {boolean} updated the updated property to search for
* @returns {Discount} 
 */
module.exports.removeAllWithUpdated = function(updated) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepository_DiscountRepository__Operation_removeAllWithUpdated_body) ENABLED START */
    var promise = Discount.remove({updated: updated}).exec();
    return promise;
    /* PROTECTED REGION END */
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepository_DiscountRepository_additional) ENABLED START */
/* PROTECTED REGION END */
