/**
* This module represents a repository for the collection Branch
* @module repository/brand/BranchRepository
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BranchRepository_init) ENABLED START */

var logger = console;
var mongoose = require('mongoose');

var Q = require('q');

/* PROTECTED REGION END */

var Branch = mongoose.model('Branch');


/**
 * Returns all branches with pagination
* @param {string} brand the id of the brand
* @param {Array} optional optional params for the query (offset, limit, id, name, etc)
* @returns {Branch} 
 */
module.exports.getAllBranches = function(brand, optional) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BranchRepository__Operation_getAllBranches_body) ENABLED START */
	var limit = optional.limit;
	var offset = optional.offset;
	var orderBy = optional.orderBy;
	var orderType = optional.orderType;

	if(typeof limit === 'undefined' || limit.length == 0 || parseInt(limit) > 100){
		limit = 10;
	}

	var searchParams = {brand: brand};

	if (typeof  optional.name != 'undefined') {
		var regex = new RegExp(optional.name, 'i');
		searchParams.name = regex;
	}

	var count = Branch.count(searchParams);

	var countPromise = count.count().exec();

	var result = {};

	return countPromise.then(function (countResult) {

		result.count = countResult;

		var find = Branch.find(searchParams);

		if (typeof  limit !== 'undefined') {
			find = find.limit(limit);
		}

		if (typeof  offset !== 'undefined' && parseInt(offset) > 0) {
			find = find.skip(offset);
		}

		if (typeof  orderBy !== 'undefined') {
			var sort = {};
			if (orderType == 1) {
				sort[orderBy] = 1;
				find = find.sort(sort);
			} else {
				sort[orderBy] = -1;
				find = find.sort(sort);
			}
		}

		return find.exec();

	}).then(function (resultList) {
		result.list = resultList;

		var deferred = Q.defer();

		deferred.resolve(result);

		return deferred.promise;

	});
	/* PROTECTED REGION END */
};

/**
 * creates a new branch
* @param {string} brand the id of the brand
* @param {Branch} branch the branch to be created
* @returns {Branch} 
 */
module.exports.createBranch = function(brand, branch) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BranchRepository__Operation_createBranch_body) ENABLED START */
	var promise = Branch.create(branch);
	return promise;
	/* PROTECTED REGION END */
};

/**
 * updates a branch
* @param {string} brand the id of the brand where the branch belongs to
* @param {string} id the _id of the branch
* @param {Branch} branch the branch to be updated
* @returns {Branch} 
 */
module.exports.updateBranch = function(brand, id, branch) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BranchRepository__Operation_updateBranch_body) ENABLED START */
	var promise = Branch.findByIdAndUpdate(id, branch).exec();
	return promise;
	/* PROTECTED REGION END */
};

/**
 * Returns the branch according to the _id
* @param {string} brand the id of the brand
* @param {string} id the _id of the branch to be retrieved
* @returns {Branch} 
 */
module.exports.getBranchById = function(brand, id) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BranchRepository__Operation_getBranchById_body) ENABLED START */
	var promise = Branch.findOne ({_id: id}).exec ();
	return promise;
	/* PROTECTED REGION END */
};

/**
 * Returns the branch according to the desclub id
* @param {string} id the desclub id
* @returns {Branch} 
 */
module.exports.getBranchByDesclubId = function(id) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BranchRepository__Operation_getBranchByDesclubId_body) ENABLED START */
	var promise = Branch.findOne ({branchId: id}).exec ();
	return promise;
	/* PROTECTED REGION END */
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BranchRepository_additional) ENABLED START */
/* PROTECTED REGION END */
