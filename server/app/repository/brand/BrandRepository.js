/**
* This module represents a repository for the collection Brand
* @module repository/brand/BrandRepository
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BrandRepository_init) ENABLED START */

var logger = console;
var mongoose = require('mongoose');

var Q = require('q');

/* PROTECTED REGION END */

var Brand = mongoose.model('Brand');


/**
 * Returns all brands with pagination
* @param {Array} optional optional params for the query (offset, limit, id, name, etc)
* @returns {Brand} 
 */
module.exports.getAllBrands = function(optional) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BrandRepository__Operation_getAllBrands_body) ENABLED START */
	var limit = optional.limit;
	var offset = optional.offset;
	var orderBy = optional.orderBy;
	var orderType = optional.orderType;

	if(typeof limit === 'undefined' || limit.length == 0 || parseInt(limit) > 100){
		limit = 10;
	}

	var searchParams = {};

	if (typeof  optional.name != 'undefined') {
		var regex = new RegExp(optional.name, 'i');
		searchParams.name = regex;
	}

	var count = Brand.count(searchParams);

	var countPromise = count.count().exec();

	var result = {};

	return countPromise.then(function (countResult) {

		result.count = countResult;

		var find = Brand.find(searchParams);

		if (typeof  limit !== 'undefined') {
			find = find.limit(limit);
		}

		if (typeof  offset !== 'undefined' && parseInt(offset) > 0) {
			find = find.skip(offset);
		}

		if (typeof  orderBy !== 'undefined') {
			var sort = {};
			if (orderType == 1) {
				sort[orderBy] = 1;
				find = find.sort(sort);
			} else {
				sort[orderBy] = -1;
				find = find.sort(sort);
			}
		}

		return find.exec();

	}).then(function (resultList) {
		result.list = resultList;

		var deferred = Q.defer();

		deferred.resolve(result);

		return deferred.promise;

	});
	/* PROTECTED REGION END */
};

/**
 * creates a new brand
* @param {Brand} brand the brand to be created
* @returns {Brand} 
 */
module.exports.createBrand = function(brand) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BrandRepository__Operation_createBrand_body) ENABLED START */
	var promise = Brand.create(brand);
	return promise;
	/* PROTECTED REGION END */
};

/**
 * Updates a brand according to the _id
* @param {string} id the _id of the brand to be updated
* @param {Brand} brand the brand to be updated
* @returns {Brand} 
 */
module.exports.updateBrand = function(id, brand) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BrandRepository__Operation_updateBrand_body) ENABLED START */
	var promise = Brand.findByIdAndUpdate(id, brand).exec();
	return promise;
	/* PROTECTED REGION END */
};

/**
 * Returns the brand according to the _id
* @param {string} id the _id of the brand to be retrieved
* @returns {Brand} 
 */
module.exports.getBrandById = function(id) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BrandRepository__Operation_getBrandById_body) ENABLED START */
	var promise = Brand.findOne ({_id: id}).exec ();
	return promise;
	/* PROTECTED REGION END */
};

/**
 * Returns the brand according to the desclub id
* @param {string} id the desclub id
* @returns {Brand} 
 */
module.exports.getBrandByDesclubId = function(id) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BrandRepository__Operation_getBrandByDesclubId_body) ENABLED START */
	var promise = Brand.findOne ({brandId: id}).exec ();
	return promise;
	/* PROTECTED REGION END */
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepository_BrandRepository_additional) ENABLED START */
/* PROTECTED REGION END */
