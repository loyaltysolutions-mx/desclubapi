/**
* This module represents a repository for the collection Category
* @module repository/category/CategoryRepository
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepository_CategoryRepository_init) ENABLED START */

var logger = console;
var mongoose = require('mongoose');

var Q = require('q');

/* PROTECTED REGION END */

var Category = mongoose.model('Category');


/**
 * Returns all categories with pagination
* @param {Array} optional optional params for the query (offset, limit, id, name, etc)
* @returns {Category} 
 */
module.exports.getAllCategories = function(optional) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepository_CategoryRepository__Operation_getAllCategories_body) ENABLED START */

    var limit = optional.limit;
    var offset = optional.offset;
    var orderBy = optional.orderBy;
    var orderType = optional.orderType;

    if(typeof limit === 'undefined' || limit.length == 0 || parseInt(limit) > 100){
        limit = 10;
    }

    var searchParams = {};

    if (typeof  optional.name != 'undefined') {
        var regex = new RegExp(optional.name, 'i');
        searchParams.name = regex;
    }

    var count = Category.count(searchParams);

    var countPromise = count.count().exec();

    var result = {};

    return countPromise.then(function (countResult) {

        result.count = countResult;

        var find = Category.find(searchParams);

        if (typeof  limit !== 'undefined') {
            find = find.limit(limit);
        }

        if (typeof  offset !== 'undefined' && parseInt(offset) > 0) {
            find = find.skip(offset);
        }

        if (typeof  orderBy !== 'undefined') {
            var sort = {};
            if (orderType == 1) {
                sort[orderBy] = 1;
                find = find.sort(sort);
            } else {
                sort[orderBy] = -1;
                find = find.sort(sort);
            }
        }

        return find.exec();

    }).then(function (resultList) {
        result.list = resultList;

        var deferred = Q.defer();

        deferred.resolve(result);

        return deferred.promise;

    });

	/* PROTECTED REGION END */
};

/**
 * creates a new category
* @param {Category} category the Category to be created
* @returns {Category} 
 */
module.exports.createCategory = function(category) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepository_CategoryRepository__Operation_createCategory_body) ENABLED START */
    var promise = Category.create(category);
    return promise;
	/* PROTECTED REGION END */
};

/**
 * Returns the category according to the _id
* @param {string} id the _id of the category to be retrieved
* @returns {Category} 
 */
module.exports.getCategoryById = function(id) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepository_CategoryRepository__Operation_getCategoryById_body) ENABLED START */
    var promise = Category.findOne({_id: id}).exec();
    return promise;
	/* PROTECTED REGION END */
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepository_CategoryRepository_additional) ENABLED START */
/* PROTECTED REGION END */
