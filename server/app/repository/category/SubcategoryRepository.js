/**
* This module represents a repository for the collection Subcategory
* @module repository/category/SubcategoryRepository
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepository_SubcategoryRepository_init) ENABLED START */

var logger = console;
var mongoose = require('mongoose');

var Q = require('q');


/* PROTECTED REGION END */

var Subcategory = mongoose.model('Subcategory');


/**
 * Returns all subcategories with pagination
* @param {string} category the _id of the category
* @param {Array} optional optional params for the query (offset, limit, id, name, etc)
* @returns {Subcategory} 
 */
module.exports.getAllSubcategories = function(category, optional) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepository_SubcategoryRepository__Operation_getAllSubcategories_body) ENABLED START */
	var limit = optional.limit;
	var offset = optional.offset;
	var orderBy = optional.orderBy;
	var orderType = optional.orderType;

	if(typeof limit === 'undefined' || limit.length == 0 || parseInt(limit) > 100){
		limit = 10;
	}

	var searchParams = {category: category};

	if (typeof  optional.name != 'undefined') {
		var regex = new RegExp(optional.name, 'i');
		searchParams.name = regex;
	}

	var count = Subcategory.count(searchParams);

	var countPromise = count.count().exec();

	var result = {};

	return countPromise.then(function (countResult) {

		result.count = countResult;

		var find = Subcategory.find(searchParams);

		if (typeof  limit !== 'undefined') {
			find = find.limit(limit);
		}

		if (typeof  offset !== 'undefined' && parseInt(offset) > 0) {
			find = find.skip(offset);
		}

		if (typeof  orderBy !== 'undefined') {
			var sort = {};
			if (orderType == 1) {
				sort[orderBy] = 1;
				find = find.sort(sort);
			} else {
				sort[orderBy] = -1;
				find = find.sort(sort);
			}
		}

		return find.exec();

	}).then(function (resultList) {
		result.list = resultList;

		var deferred = Q.defer();

		deferred.resolve(result);

		return deferred.promise;

	});
	/* PROTECTED REGION END */
};

/**
 * creates a new branch
* @param {string} category the _id of the category
* @param {Subcategory} subcategory the subcategory to be created
* @returns {Subcategory} 
 */
module.exports.createSubcategory = function(category, subcategory) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepository_SubcategoryRepository__Operation_createSubcategory_body) ENABLED START */
	var promise = Subcategory.create(subcategory);
	return promise;
	/* PROTECTED REGION END */
};

/**
 * Returns the subcategory according to the desclub id
* @param {string} id the desclub category id
* @returns {Subcategory} 
 */
module.exports.getSubcategoryByDesclubId = function(id) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepository_SubcategoryRepository__Operation_getSubcategoryByDesclubId_body) ENABLED START */
	var promise = Subcategory.findOne ({subcategoryId: id}).populate('category').exec ();
	return promise;
	/* PROTECTED REGION END */
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepository_SubcategoryRepository_additional) ENABLED START */
/* PROTECTED REGION END */
