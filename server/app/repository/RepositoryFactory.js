
	/**
	 * A factory module for repositories
	 * @module repository/RepositoryFactory
	 */

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub__repository_RepositoryFactory_init) ENABLED START */
	var logger = console;
	var appReference = null;
	
	/* PROTECTED REGION END */
	
	/**
	 * Initializes the repository factory passing the express app as dependency
	 * @param {Express} app - The express app
	 */
	module.exports.init = function(app) {
	    appReference = app;
	};
	
	/**
	 * Returns the repository factory object with the appropiate methods
	 * for each repository
	 * @returns {object} a repositoryFactory object
	 */
	module.exports.getRepositoryFactory = function() {
		var repositoryFactory = {
			/**
			 * Creates a new BrandRepository module. It corresponds to
			 * the document Brand
			 * @returns {BrandRepository} a BrandRepository module
			 */
			getBrandRepository: function(){
				return require("./brand/BrandRepository");
			},
			/**
			 * Creates a new BranchRepository module. It corresponds to
			 * the document Branch
			 * @returns {BranchRepository} a BranchRepository module
			 */
			getBranchRepository: function(){
				return require("./brand/BranchRepository");
			},
			/**
			 * Creates a new CategoryRepository module. It corresponds to
			 * the document Category
			 * @returns {CategoryRepository} a CategoryRepository module
			 */
			getCategoryRepository: function(){
				return require("./category/CategoryRepository");
			},
			/**
			 * Creates a new SubcategoryRepository module. It corresponds to
			 * the document Subcategory
			 * @returns {SubcategoryRepository} a SubcategoryRepository module
			 */
			getSubcategoryRepository: function(){
				return require("./category/SubcategoryRepository");
			},
			/**
			 * Creates a new DiscountRepository module. It corresponds to
			 * the document Discount
			 * @returns {DiscountRepository} a DiscountRepository module
			 */
			getDiscountRepository: function(){
				return require("./discount/DiscountRepository");
			},
			/**
			 * Creates a new ZoneRepository module. It corresponds to
			 * the document Zone
			 * @returns {ZoneRepository} a ZoneRepository module
			 */
			getZoneRepository: function(){
				return require("./zone/ZoneRepository");
			},
			/**
			 * Creates a new StateRepository module. It corresponds to
			 * the document State
			 * @returns {StateRepository} a StateRepository module
			 */
			getStateRepository: function(){
				return require("./state/StateRepository");
			},
			/**
			 * Creates a new CorporateRepository module. It corresponds to
			 * the document Corporate
			 * @returns {CorporateRepository} a CorporateRepository module
			 */
			getCorporateRepository: function(){
				return require("./corporate/CorporateRepository");
			},
			/**
			 * Creates a new CorporateMembershipRepository module. It corresponds to
			 * the document CorporateMembership
			 * @returns {CorporateMembershipRepository} a CorporateMembershipRepository module
			 */
			getCorporateMembershipRepository: function(){
				return require("./corporate/CorporateMembershipRepository");
			}
		};
		
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub__repository_RepositoryFactory_additionalRepos) ENABLED START */
		/* PROTECTED REGION END */
		
		return repositoryFactory;
	};
	
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub__repository_RepositoryFactory_additional) ENABLED START */
	/* PROTECTED REGION END */
