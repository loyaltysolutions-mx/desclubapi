/**
* This module represents a repository for the collection State
* @module repository/state/StateRepository
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentalRepository_StateRepository_init) ENABLED START */

var logger = console;
var mongoose = require('mongoose');

var Q = require('q');
/* PROTECTED REGION END */

var State = mongoose.model('State');


/**
 * Returns all states with pagination
* @param {Array} optional optional params for the query (offset, limit, stateId, name)
* @returns {State} 
 */
module.exports.getAllStates = function(optional) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentalRepository_StateRepository__Operation_getAllStates_body) ENABLED START */
	var limit = optional.limit;
	var offset = optional.offset;
	var orderBy = optional.orderBy;
	var orderType = optional.orderType;

	if(typeof limit === 'undefined' || limit.length == 0 || parseInt(limit) > 100){
		limit = 10;
	}

	var searchParams = {};

	if (typeof  optional.name != 'undefined') {
		var regex = new RegExp(optional.name, 'i');
		searchParams.name = regex;
	}

	var count = State.count(searchParams);

	var countPromise = count.count().exec();

	var result = {};

	return countPromise.then(function (countResult) {

		result.count = countResult;

		var find = State.find(searchParams);

		if (typeof  limit !== 'undefined') {
			find = find.limit(limit);
		}

		if (typeof  offset !== 'undefined' && typeof offset == 'number') {
			find = find.skip(offset);
		}

		if (typeof  orderBy !== 'undefined') {
			var sort = {};
			if (orderType == 1) {
				sort[orderBy] = 1;
				find = find.sort(sort);
			} else {
				sort[orderBy] = -1;
				find = find.sort(sort);
			}
		}

		return find.exec();

	}).then(function (resultList) {
		result.list = resultList;

		var deferred = Q.defer();

		deferred.resolve(result);

		return deferred.promise;

	});
	/* PROTECTED REGION END */
};

/**
 * Returns the state according to the _id
* @param {string} id the _id of the state to be retrieved
* @returns {State} 
 */
module.exports.getStateById = function(id) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentalRepository_StateRepository__Operation_getStateById_body) ENABLED START */
	var promise = State.findOne ({_id: id}).exec ();
	return promise;
	/* PROTECTED REGION END */
};

/**
 * Returns the state according to the stateId
* @param {string} stateId the desclub id of the state to be retrieved
* @returns {State} 
 */
module.exports.getStateByStateId = function(stateId) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentalRepository_StateRepository__Operation_getStateByStateId_body) ENABLED START */
	var promise = State.findOne ({stateId: stateId}).exec ();
	return promise;
	/* PROTECTED REGION END */
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentalRepository_StateRepository_additional) ENABLED START */
/* PROTECTED REGION END */
