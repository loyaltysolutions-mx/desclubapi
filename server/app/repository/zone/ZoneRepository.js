/**
* This module represents a repository for the collection Zone
* @module repository/zone/ZoneRepository
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentalRepository_ZoneRepository_init) ENABLED START */

var logger = console;
var mongoose = require('mongoose');

var Q = require('q');
/* PROTECTED REGION END */

var Zone = mongoose.model('Zone');


/**
 * Returns all zones with pagination
* @param {Array} optional optional params for the query (offset, limit, zoneId, name)
* @returns {Zone} 
 */
module.exports.getAllZones = function(optional) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentalRepository_ZoneRepository__Operation_getAllZones_body) ENABLED START */
	var limit = optional.limit;
	var offset = optional.offset;
	var orderBy = optional.orderBy;
	var orderType = optional.orderType;

	if(typeof limit === 'undefined' || limit.length == 0 || parseInt(limit) > 100){
		limit = 10;
	}

	var searchParams = {};

	if (typeof  optional.name != 'undefined' && optional.name.length > 0) {
		var regex = new RegExp(optional.name, 'i');
		searchParams.name = regex;
	}

	if (typeof  optional.stateId != 'undefined' && optional.stateId.length > 0) {
		searchParams.stateId = optional.stateId;
	}

	var count = Zone.count(searchParams);

	var countPromise = count.count().exec();

	var result = {};

	return countPromise.then(function (countResult) {

		result.count = countResult;

		var find = Zone.find(searchParams);

		if (typeof  limit !== 'undefined') {
			find = find.limit(limit);
		}

		if (typeof  offset !== 'undefined' && parseInt(offset) > 0) {
			find = find.skip(offset);
		}

		if (typeof  orderBy !== 'undefined') {
			var sort = {};
			if (orderType == 1) {
				sort[orderBy] = 1;
				find = find.sort(sort);
			} else {
				sort[orderBy] = -1;
				find = find.sort(sort);
			}
		}

		return find.exec();

	}).then(function (resultList) {
		result.list = resultList;

		var deferred = Q.defer();

		deferred.resolve(result);

		return deferred.promise;

	});
	/* PROTECTED REGION END */
};

/**
 * Returns the zone according to the _id
* @param {string} id the _id of the zone to be retrieved
* @returns {Zone} 
 */
module.exports.getZoneById = function(id) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentalRepository_ZoneRepository__Operation_getZoneById_body) ENABLED START */
	var promise = Zone.findOne ({_id: id}).exec ();
	return promise;
	/* PROTECTED REGION END */
};

/**
 * Returns the zone according to the zoneId
* @param {string} zoneId the zoneId of the zone to be retrieved
* @returns {Zone} 
 */
module.exports.getZoneByZoneId = function(zoneId) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentalRepository_ZoneRepository__Operation_getZoneByZoneId_body) ENABLED START */
	var promise = Zone.findOne ({zoneId: zoneId}).exec ();
	return promise;
	/* PROTECTED REGION END */
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentalRepository_ZoneRepository_additional) ENABLED START */
/* PROTECTED REGION END */
