/**
* This module represents a repository for the collection Corporate
* @module repository/corporate/CorporateRepository
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateRepository_init) ENABLED START */

var logger = console;
var mongoose = require('mongoose');

var Q = require('q');
/* PROTECTED REGION END */

var Corporate = mongoose.model('Corporate');


/**
 * Returns all corporates with pagination
* @param {Array} optional optional params for the query (offset, limit, _id, name, etc)
* @returns {Corporate} 
 */
module.exports.getAllCorporates = function(optional) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateRepository__Operation_getAllCorporates_body) ENABLED START */
    var limit = optional.limit;
    var offset = optional.offset;
    var orderBy = optional.orderBy;
    var orderType = optional.orderType;

    if (typeof limit === 'undefined' || limit.length == 0 || parseInt(limit) > 100) {
        limit = 10;
    }

    var searchParams = {};

    if (typeof  optional.name != 'undefined') {
        var regex = new RegExp(optional.name, 'i');
        searchParams.name = regex;
    }

    var count = Corporate.count(searchParams);

    var countPromise = count.count().exec();

    var result = {};

    return countPromise.then(function (countResult) {

        result.count = countResult;

        var find = Corporate.find(searchParams);

        if (typeof  limit !== 'undefined') {
            find = find.limit(limit);
        }

        if (typeof  offset !== 'undefined' && parseInt(offset) > 0) {
            find = find.skip(offset);
        }

        if (typeof  orderBy !== 'undefined') {
            var sort = {};
            if (orderType == 1) {
                sort[orderBy] = 1;
                find = find.sort(sort);
            } else {
                sort[orderBy] = -1;
                find = find.sort(sort);
            }
        }

        return find.exec();

    }).then(function (resultList) {
        result.list = resultList;

        var deferred = Q.defer();

        deferred.resolve(result);

        return deferred.promise;

    });
    /* PROTECTED REGION END */
};

/**
 * creates a new corporate
* @param {Corporate} corporate the corporate to be created
* @returns {Corporate} 
 */
module.exports.createCorporate = function(corporate) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateRepository__Operation_createCorporate_body) ENABLED START */
    var promise = Corporate.create(corporate);
    return promise;
    /* PROTECTED REGION END */
};

/**
 * Updates a corporate according to the _id
* @param {string} id the _id of the corporate to be updated
* @param {Corporate} corporate the corporate to be updated
* @returns {Corporate} 
 */
module.exports.updateCorporate = function(id, corporate) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateRepository__Operation_updateCorporate_body) ENABLED START */
    var promise = Corporate.findByIdAndUpdate(id, corporate).exec();

    return promise.then(function (foundCorporate) {
        return Corporate.findOne({_id: id}).exec();
    });
    /* PROTECTED REGION END */
};

/**
 * Returns the corporate according to the _id
* @param {string} id the _id of the corporate to be retrieved
* @returns {Corporate} 
 */
module.exports.getCorporateById = function(id) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateRepository__Operation_getCorporateById_body) ENABLED START */
    var promise = Corporate.findOne({_id: id}).exec();
    return promise;
    /* PROTECTED REGION END */
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateRepository_additional) ENABLED START */
/* PROTECTED REGION END */
