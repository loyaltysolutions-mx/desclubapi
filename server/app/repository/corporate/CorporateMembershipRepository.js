/**
* This module represents a repository for the collection CorporateMembership
* @module repository/corporate/CorporateMembershipRepository
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateMembershipRepository_init) ENABLED START */

var logger = console;
var mongoose = require('mongoose');

var Q = require('q');

var ObjectId = require('mongoose').Types.ObjectId;
/* PROTECTED REGION END */

var CorporateMembership = mongoose.model('CorporateMembership');


/**
 * Returns all corporate membership with pagination
* @param {Array} optional optional params for the query (offset, limit, status, corporate, corporateName, membershipNumber etc)
* @returns {CorporateMembership} 
 */
module.exports.getAllCorporateMemberships = function(optional) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateMembershipRepository__Operation_getAllCorporateMemberships_body) ENABLED START */
    var limit = optional.limit;
    var offset = optional.offset;
    var orderBy = optional.orderBy;
    var orderType = optional.orderType;

    if (typeof limit === 'undefined' || limit.length == 0 || parseInt(limit) > 100) {
        limit = 10;
    }

    var searchParams = {};

    if (typeof  optional.corporateName != 'undefined') {
        var regex = new RegExp(optional.corporateName, 'i');
        searchParams['corporateInfo.name'] = regex;
    }

    if (typeof  optional.name != 'undefined') {
        var regex = new RegExp(optional.name, 'i');
        searchParams['name'] = regex;
    }

    if (typeof  optional.email != 'undefined') {
        var regex = new RegExp(optional.email, 'i');
        searchParams['email'] = regex;
    }

    if (typeof  optional.corporate != 'undefined' && optional.corporate != null && optional.corporate.length > 0) {
        searchParams.corporate = new ObjectId(optional.corporate);
    }

    if (typeof  optional.membershipNumber != 'undefined') {
        var regex = new RegExp(optional.membershipNumber, 'i');
        searchParams.membershipNumber = regex;
    }

    if (typeof  optional.status != 'undefined') {
        searchParams.status = optional.status;
    }

    var count = CorporateMembership.count(searchParams);

    var countPromise = count.count().exec();

    var result = {};

    return countPromise.then(function (countResult) {

        result.count = countResult;

        var find = CorporateMembership.find(searchParams).populate("corporate");

        if (typeof  limit !== 'undefined') {
            find = find.limit(limit);
        }

        if (typeof  offset !== 'undefined' && parseInt(offset) > 0) {
            find = find.skip(offset);
        }

        if (typeof  orderBy !== 'undefined') {
            var sort = {};
            if (orderType == 1) {
                sort[orderBy] = 1;
                find = find.sort(sort);
            } else {
                sort[orderBy] = -1;
                find = find.sort(sort);
            }
        }

        return find.exec();

    }).then(function (resultList) {
        result.list = resultList;

        var deferred = Q.defer();

        deferred.resolve(result);

        return deferred.promise;

    });
    /* PROTECTED REGION END */
};

/**
 * creates a new corporate membership
* @param {CorporateMembership} membership the corporate membership to be created
* @returns {CorporateMembership} 
 */
module.exports.createCorporateMembership = function(membership) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateMembershipRepository__Operation_createCorporateMembership_body) ENABLED START */
    var promise = CorporateMembership.create(membership);
    return promise;
    /* PROTECTED REGION END */
};

/**
 * Updates a corporate membership status according to the _id
* @param {string} id the _id of the corporate to be updated
* @param {boolean} alreadyUsed the corporate membership alreadyUsed field to be updated
* @returns {CorporateMembership} 
 */
module.exports.updateCorporateMembershipStatus = function(id, alreadyUsed) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateMembershipRepository__Operation_updateCorporateMembershipStatus_body) ENABLED START */
    var promise = CorporateMembership.update({_id: id}, {alreadyUsed: alreadyUsed}).exec();

    return promise.then(function (updatedCorporate) {
        return CorporateMembership.findOne({_id: id}).populate('corporate').exec();
    });
    /* PROTECTED REGION END */
};

/**
 * Returns the corporate membership according to the _id
* @param {string} id the _id of the corporate membership to be retrieved
* @returns {CorporateMembership} 
 */
module.exports.getCorporateMembershipById = function(id) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateMembershipRepository__Operation_getCorporateMembershipById_body) ENABLED START */
    var promise = CorporateMembership.findOne({_id: id}).populate("corporate").exec();
    return promise;
    /* PROTECTED REGION END */
};

/**
 * Returns the corporate membership according to the email
* @param {string} email the email of the corporate membership to be retrieved
* @returns {CorporateMembership} 
 */
module.exports.getCorporateMembershipByEmail = function(email) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateMembershipRepository__Operation_getCorporateMembershipByEmail_body) ENABLED START */
    var promise = CorporateMembership.findOne({email: email}).populate("corporate").exec();
    return promise;
    /* PROTECTED REGION END */
};

/**
 * Returns the corporate membership according to the membershipNumber
* @param {string} membershipNumber the membershipNumber of the corporate membership to be retrieved
* @returns {CorporateMembership} 
 */
module.exports.getCorporateMembershipByNumber = function(membershipNumber) {
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateMembershipRepository__Operation_getCorporateMembershipByNumber_body) ENABLED START */
    var promise = CorporateMembership.findOne({membershipNumber: membershipNumber}).populate("corporate").exec();
    return promise;
    /* PROTECTED REGION END */
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepository_CorporateMembershipRepository_additional) ENABLED START */
/* PROTECTED REGION END */
