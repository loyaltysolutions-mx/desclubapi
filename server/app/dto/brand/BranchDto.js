
/**
 * The module representing the BranchDto DTO
 * @module dto/brand/BranchDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BranchDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

var brandDto = require('../brand/BrandDto');
var zoneDto = require('../zone/ZoneDto');
var stateDto = require('../state/StateDto');
var discountDto = require('../discount/DiscountDto');
var branchLocationDto = require('../brand/BranchLocationDto');

module.exports.build = function (input) {


	var BranchRep = {
		_id : input._id,
		branchId : input.branchId,
		brand : input.brand,
		name : input.name,
		street : input.street,
		extNum : input.extNum,
		intNum : input.intNum,
		colony : input.colony,
		zipCode : input.zipCode,
		city : input.city,
		zone : input.zone,
		state : input.state,
		phone : input.phone,
		mainDiscount : input.mainDiscount,
		location : input.location,
		created : input.created,
		location : input.location,
		originalBranch : input.originalBranch
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BranchDto_customBuild) ENABLED START */
	delete BranchRep.originalBranch;
	/* PROTECTED REGION END */

	return BranchRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BranchDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
	
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BranchDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BranchDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
