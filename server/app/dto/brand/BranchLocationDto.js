
/**
 * The module representing the BranchLocationDto DTO
 * @module dto/brand/BranchLocationDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BranchLocationDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */


module.exports.build = function (input) {


	var BranchLocationRep = {
		type : input.type,
		coordinates : input.coordinates
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BranchLocationDto_customBuild) ENABLED START */
	/* PROTECTED REGION END */

	return BranchLocationRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BranchLocationDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
	
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BranchLocationDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BranchLocationDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
