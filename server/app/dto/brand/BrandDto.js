
/**
 * The module representing the BrandDto DTO
 * @module dto/brand/BrandDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BrandDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

var discountDto = require('../discount/DiscountDto');

module.exports.build = function (input) {


	var BrandRep = {
		_id : input._id,
		brandId : input.brandId,
		name : input.name,
		logoSmall : input.logoSmall,
		logoBig : input.logoBig,
		validity_start : input.validity_start,
		validity_end : input.validity_end,
		url : input.url,
		mainDiscount : input.mainDiscount,
		created : input.created,
		updated : input.updated,
		originalBrand : input.originalBrand
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BrandDto_customBuild) ENABLED START */
	delete BrandRep.originalBrand;
	/* PROTECTED REGION END */

	return BrandRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BrandDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
	
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BrandDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Dto_BrandDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
