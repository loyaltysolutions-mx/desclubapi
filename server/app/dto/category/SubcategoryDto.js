
/**
 * The module representing the SubcategoryDto DTO
 * @module dto/category/SubcategoryDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Dto_SubcategoryDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

var categoryDto = require('../category/CategoryDto');

module.exports.build = function (input) {


	var SubcategoryRep = {
		_id : input._id,
		subcategoryId : input.subcategoryId,
		name : input.name,
		category : input.category
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Dto_SubcategoryDto_customBuild) ENABLED START */
	/* PROTECTED REGION END */

	return SubcategoryRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Dto_SubcategoryDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
	
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Dto_SubcategoryDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Dto_SubcategoryDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
