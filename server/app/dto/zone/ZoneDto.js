
/**
 * The module representing the ZoneDto DTO
 * @module dto/zone/ZoneDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_Dto_ZoneDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */


module.exports.build = function (input) {


	var ZoneRep = {
		_id : input._id,
		zoneId : input.zoneId,
		stateId : input.stateId,
		name : input.name
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_Dto_ZoneDto_customBuild) ENABLED START */
	/* PROTECTED REGION END */

	return ZoneRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_Dto_ZoneDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
	
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_Dto_ZoneDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_Dto_ZoneDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
