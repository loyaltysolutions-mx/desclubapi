
/**
 * The module representing the DiscountDto DTO
 * @module dto/discount/DiscountDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Dto_DiscountDto_init) ENABLED START */
var logger = console;

var brandNewDto = require('../brand/BrandDto');
/* PROTECTED REGION END */

var branchDto = require('../brand/BranchDto');
var branchDto = require('../brand/BranchDto');
var categoryDto = require('../category/CategoryDto');
var subcategoryDto = require('../category/SubcategoryDto');

module.exports.build = function (input) {


	var DiscountRep = {
		_id : input._id,
		branch : input.branch,
		brand : input.brand,
		category : input.category,
		subcategory : input.subcategory,
		cash : input.cash,
		card : input.card,
		promo : input.promo,
		restriction : input.restriction,
		location : input.location,
		originalDiscount : input.originalDiscount
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Dto_DiscountDto_customBuild) ENABLED START */
	DiscountRep.branch = branchDto.build(input.branch);
	DiscountRep.brand = brandNewDto.build(input.brand);

	delete DiscountRep.originalDiscount;
	/* PROTECTED REGION END */

	return DiscountRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Dto_DiscountDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
	
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Dto_DiscountDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Dto_DiscountDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
