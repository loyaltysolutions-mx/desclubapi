
/**
 * The module representing the NearByDiscountDto DTO
 * @module dto/discount/NearByDiscountDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Dto_NearByDiscountDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

var discountDto = require('./DiscountDto');

module.exports.build = function (input) {

		var NearByDiscountDtoRep = {
			dis : input.dis,
			discount : input.discount
		};

		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Dto_NearByDiscountDto_customBuild) ENABLED START */
		/* PROTECTED REGION END */

		return NearByDiscountDtoRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Dto_NearByDiscountDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Dto_NearByDiscountDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Dto_NearByDiscountDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
