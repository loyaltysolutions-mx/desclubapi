
/**
 * The module representing the StateDto DTO
 * @module dto/state/StateDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_Dto_StateDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */


module.exports.build = function (input) {


	var StateRep = {
		_id : input._id,
		stateId : input.stateId,
		name : input.name
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_Dto_StateDto_customBuild) ENABLED START */
	/* PROTECTED REGION END */

	return StateRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_Dto_StateDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
	
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_Dto_StateDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_Dto_StateDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
