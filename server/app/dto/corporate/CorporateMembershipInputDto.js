
/**
 * The module representing the CorporateMembershipInputDto DTO
 * @module dto/corporate/CorporateMembershipInputDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipInputDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

var membershipAdditionalDataDto = require('../corporate/MembershipAdditionalDataDto');

module.exports.build = function (input) {


	var CorporateMembershipRep = {
		firstName : input.firstName,
		lastName1 : input.lastName1,
		lastName2 : input.lastName2,
		email : input.email,
		membershipNumber : input.membershipNumber,
		additionalData : input.additionalData
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipInputDto_customBuild) ENABLED START */
	/* PROTECTED REGION END */

	return CorporateMembershipRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipInputDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
	
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipInputDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipInputDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
