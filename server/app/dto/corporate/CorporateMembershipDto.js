
/**
 * The module representing the CorporateMembershipDto DTO
 * @module dto/corporate/CorporateMembershipDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

var corporateDto = require('../corporate/CorporateDto');
var membershipAdditionalDataDto = require('../corporate/MembershipAdditionalDataDto');

module.exports.build = function (input) {


	var CorporateMembershipRep = {
		_id : input._id,
		name : input.name,
		firstName : input.firstName,
		lastName1 : input.lastName1,
		lastName2 : input.lastName2,
		email : input.email,
		membershipNumber : input.membershipNumber,
		alreadyUsed : input.alreadyUsed,
		corporate : input.corporate,
		additionalData : input.additionalData,
		validThru : input.validThru,
		created : input.created
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipDto_customBuild) ENABLED START */
	/* PROTECTED REGION END */

	return CorporateMembershipRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
	
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
