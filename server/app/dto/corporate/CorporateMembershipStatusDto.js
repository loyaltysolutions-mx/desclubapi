
/**
 * The module representing the CorporateMembershipStatusDto DTO
 * @module dto/corporate/CorporateMembershipStatusDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipStatusDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */


module.exports.build = function (input) {

		var CorporateMembershipStatusDtoRep = {
			status : input.status
		};

		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipStatusDto_customBuild) ENABLED START */
		/* PROTECTED REGION END */

		return CorporateMembershipStatusDtoRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipStatusDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipStatusDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipStatusDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
