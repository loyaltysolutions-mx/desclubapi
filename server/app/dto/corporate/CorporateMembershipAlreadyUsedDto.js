
/**
 * The module representing the CorporateMembershipAlreadyUsedDto DTO
 * @module dto/corporate/CorporateMembershipAlreadyUsedDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipAlreadyUsedDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */


module.exports.build = function (input) {

		var CorporateMembershipAlreadyUsedDtoRep = {
			alreadyUsed : input.alreadyUsed
		};

		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipAlreadyUsedDto_customBuild) ENABLED START */
		/* PROTECTED REGION END */

		return CorporateMembershipAlreadyUsedDtoRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipAlreadyUsedDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipAlreadyUsedDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateMembershipAlreadyUsedDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
