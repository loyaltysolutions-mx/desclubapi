
/**
 * The module representing the MembershipAdditionalData DTO
 * @module dto/corporate/MembershipAdditionalData
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_MembershipAdditionalData_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */


module.exports.build = function (input) {


	var CorporateMembershipAdditionalDataRep = {
		bancomer : input.bancomer
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_MembershipAdditionalData_customBuild) ENABLED START */
	/* PROTECTED REGION END */

	return CorporateMembershipAdditionalDataRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_MembershipAdditionalData_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
	
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_MembershipAdditionalData_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_MembershipAdditionalData_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
