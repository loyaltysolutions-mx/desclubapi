
/**
 * The module representing the CorporateInputDto DTO
 * @module dto/corporate/CorporateInputDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateInputDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */


module.exports.build = function (input) {


	var CorporateRep = {
		name : input.name,
		membershipPrefix : input.membershipPrefix
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateInputDto_customBuild) ENABLED START */
	/* PROTECTED REGION END */

	return CorporateRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateInputDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
	
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateInputDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateInputDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
