
/**
 * The module representing the CorporateDto DTO
 * @module dto/corporate/CorporateDto
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateDto_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */


module.exports.build = function (input) {


	var CorporateRep = {
		_id : input._id,
		name : input.name,
		membershipPrefix : input.membershipPrefix,
		created : input.created
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateDto_customBuild) ENABLED START */
	/* PROTECTED REGION END */

	return CorporateRep;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateDto_additionalBuildMethods) ENABLED START */
/* PROTECTED REGION END */

module.exports.buildList = function (inputList) {
	
    if (Array.isArray(inputList)) {
        var arrayResult = [];

        for (var i = 0; i < inputList.length; i++) {
            var builtInput = module.exports.build(inputList[i]);
            arrayResult.push(builtInput);
        }

        /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateDto_customBuildList) ENABLED START */
		/* PROTECTED REGION END */

        return arrayResult;

    } else {
        return module.exports.build(inputList);
    }
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Dto_CorporateDto_additionalBuildListMethods) ENABLED START */
/* PROTECTED REGION END */
