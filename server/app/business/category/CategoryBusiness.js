/**
* This module represents a set of business methods
* @module business/category/CategoryBusiness
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Business_CategoryBusiness_init) ENABLED START */

var logger = console;
var repositoryFactory = require('../../repository/RepositoryFactory').getRepositoryFactory();

// Load configurations according to the selected environment
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];

var categoryRepository = repositoryFactory.getCategoryRepository();

var CategoryDto = require('../../dto/category/CategoryDto');
var ErrorUtil = require('../util/ErrorUtil');

var mongoose = require('mongoose');
var SchemaType = mongoose.SchemaType;
var CastError = SchemaType.CastError;

var ValidationError = mongoose.Error.ValidationError;
var ValidatorError = mongoose.Error.ValidatorError;

var extend = require('util')._extend;
/* PROTECTED REGION END */


/**
 * Returns all categories. Results can be refined with optional parameters
* @param {integer} offset -HTTP Type: QUERY- the index where the records start from
* @param {integer} limit -HTTP Type: QUERY- the limit of records to retrieve
* @param {string} orderBy -HTTP Type: QUERY- the order field
* @param {string} orderType -HTTP Type: QUERY- the order type field
* @param {string} id -HTTP Type: QUERY- the id of the category to be retrieved
* @param {string} name -HTTP Type: QUERY- the name of the category to be retrieved
* @returns {CategoryDto} 
 */
module.exports.getAllCategories = function(req, res){
	var limit = req.query.limit;
	var offset = req.query.offset;
	var orderBy = req.query.orderBy;
	var orderType = req.query.orderType;
	var id = req.query.id;
	var name = req.query.name;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Business_CategoryBusiness_getAllCategories_body) ENABLED START */

    var promise = categoryRepository.getAllCategories(req.query);

    promise.then(function (categories) {

        logger.debug('Total categories: ' + categories.list.length);

        res.header(config.countHeader, categories.count);
        res.status(200).json(CategoryDto.buildList(categories.list));
    }).then(null, function (error) {
        logger.error(error.toString());
        res.status(500).json(ErrorUtil.unknownError(error));
    });
	/* PROTECTED REGION END */
};


/**
 * Create a new category
* @param {CategoryDto} category -HTTP Type: BODY- The category to be created
* @returns {CategoryDto} 
 */
module.exports.createCategory = function(req, res){
	var category = req.body;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Business_CategoryBusiness_createCategory_body) ENABLED START */

    var promise = categoryRepository.createCategory(category);

    promise.then(function (categoryCreated) {
        res.status(200).json(CategoryDto.build(categoryCreated));
    }).then(null, function (error) {
        logger.trace(error);

        if (error.code === 11000 || error.code === 11001) {
            return res.status(409).json(ErrorUtil.duplicateError(error.err, error));
        }

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter category', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));
    });
	/* PROTECTED REGION END */
};


