/**
* This module represents a set of business methods
* @module business/category/SubcategoryBusiness
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Business_SubcategoryBusiness_init) ENABLED START */

var logger = console;
var repositoryFactory = require('../../repository/RepositoryFactory').getRepositoryFactory();

// Load configurations according to the selected environment
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];

var subcategoryRepository = repositoryFactory.getSubcategoryRepository();

var SubcategoryDto = require('../../dto/category/SubcategoryDto');
var ErrorUtil = require('../util/ErrorUtil');

var mongoose = require('mongoose');
var SchemaType = mongoose.SchemaType;
var CastError = SchemaType.CastError;

var ValidationError = mongoose.Error.ValidationError;
var ValidatorError = mongoose.Error.ValidatorError;

var extend = require('util')._extend;
/* PROTECTED REGION END */


/**
 * Returns all sub-categories. Results can be refined with optional parameters
* @param {CategoryDto} category -HTTP Type: NAMED- the category where to search subcategories from
* @param {integer} offset -HTTP Type: QUERY- the index where the records start from
* @param {integer} limit -HTTP Type: QUERY- the limit of records to retrieve
* @param {string} orderBy -HTTP Type: QUERY- the order field
* @param {string} orderType -HTTP Type: QUERY- the order type field
* @param {string} id -HTTP Type: QUERY- the id of the sub-category to be retrieved
* @param {string} name -HTTP Type: QUERY- the name of the sub-category to be retrieved
* @returns {SubcategoryDto} 
 */
module.exports.getAllSubcategories = function(req, res){
	var category = req.params.category;
	var limit = req.query.limit;
	var offset = req.query.offset;
	var orderBy = req.query.orderBy;
	var orderType = req.query.orderType;
	var id = req.query.id;
	var name = req.query.name;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Business_SubcategoryBusiness_getAllSubcategories_body) ENABLED START */

	if(typeof category === 'undefined' || category.length < 5){
		return res.status(400).json(ErrorUtil.invalidCategory(category));
	}

	var promise = subcategoryRepository.getAllSubcategories(category, req.query);

	promise.then(function (subcategories) {

		logger.debug('Total subcategories: ' + subcategories.list.length);

		res.header(config.countHeader, subcategories.count);
		res.status(200).json(SubcategoryDto.buildList(subcategories.list));
	}).then(null, function (error) {
		logger.error(error.toString());

		if (error instanceof  ValidationError || error instanceof ValidatorError) {
			return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
		}

		res.status(500).json(ErrorUtil.unknownError(error));
	});
	/* PROTECTED REGION END */
};


/**
 * Create a new sub-category
* @param {string} category -HTTP Type: NAMED- the category where the sub-category will be assigned to
* @param {SubcategoryDto} subcategory -HTTP Type: BODY- The sub-category to be created
* @returns {SubcategoryDto} 
 */
module.exports.createSubcategory = function(req, res){
	var category = req.params.category;
	var subcategory = req.body;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Business_SubcategoryBusiness_createSubcategory_body) ENABLED START */

	var promise = subcategoryRepository.createSubcategory(category, subcategory);

	promise.then(function (subcategoryCreated) {
		res.status(200).json(SubcategoryDto.build(subcategoryCreated));
	}).then(null, function (error) {
		logger.trace(error);

		if (error.code === 11000 || error.code === 11001) {
			return res.status(409).json(ErrorUtil.duplicateError(error.err, error));
		}

		if (error instanceof  CastError) {
			return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter subcategory', error));
		}

		if (error instanceof  ValidationError || error instanceof ValidatorError) {
			return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
		}

		res.status(500).json(ErrorUtil.unknownError(error));
	});
	/* PROTECTED REGION END */
};


