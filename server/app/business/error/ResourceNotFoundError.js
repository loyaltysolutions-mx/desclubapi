"use strict";

var util = require('util');

/**
 * Error Class ResourceNotFoundError
 * */
function ResourceNotFoundError(message) {
    //Set the name for the ERROR 
    this.name = this.constructor.name; //set our function’s name as error name.

    //Define error message
    this.message =message;
}

// inherit from Error
util.inherits(ResourceNotFoundError, Error);

//Export the constructor function as the export of this module file.
module.exports = ResourceNotFoundError;