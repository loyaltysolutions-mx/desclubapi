"use strict";

var util = require('util');

/**
 * Error Class UnauthorizedError
 * */
function InvalidMembershipNumberError(message) {
    //Set the name for the ERROR 
    this.name = this.constructor.name; //set our function’s name as error name.

    //Define error message
    if(typeof message !== 'undefined'){
        this.message =message;
    }else{
        this.message ='Hotel not found';
    }

}

// inherit from Error
util.inherits(InvalidMembershipNumberError, Error);

//Export the constructor function as the export of this module file.
module.exports = InvalidMembershipNumberError;