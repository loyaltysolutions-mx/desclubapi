/**
* This module represents a set of business methods
* @module business/brand/BrandBusiness
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Business_BrandBusiness_init) ENABLED START */

var logger = console;
var repositoryFactory = require('../../repository/RepositoryFactory').getRepositoryFactory();

// Load configurations according to the selected environment
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];

var brandRepository = repositoryFactory.getBrandRepository();

var BrandDto = require('../../dto/brand/BrandDto');
var ErrorUtil = require('../util/ErrorUtil');

var mongoose = require('mongoose');
var SchemaType = mongoose.SchemaType;
var CastError = SchemaType.CastError;

var ValidationError = mongoose.Error.ValidationError;
var ValidatorError = mongoose.Error.ValidatorError;

var extend = require('util')._extend;
/* PROTECTED REGION END */


/**
 * Returns all brands with pagination. Results can be refined with optional parameters
* @param {integer} offset -HTTP Type: QUERY- the index where the records start from
* @param {integer} limit -HTTP Type: QUERY- the limit of records to retrieve
* @param {string} orderBy -HTTP Type: QUERY- the order field
* @param {string} orderType -HTTP Type: QUERY- the order type field
* @param {string} id -HTTP Type: QUERY- the id of the brand to be retrieved
* @param {string} name -HTTP Type: QUERY- the name of the brand to be retrieved
* @returns {BrandDto} 
 */
module.exports.getAllBrands = function(req, res){
	var limit = req.query.limit;
	var offset = req.query.offset;
	var orderBy = req.query.orderBy;
	var orderType = req.query.orderType;
	var id = req.query.id;
	var name = req.query.name;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Business_BrandBusiness_getAllBrands_body) ENABLED START */
	var promise = brandRepository.getAllBrands(req.query);

	promise.then(function (brands) {

		logger.debug('Total brands: ' + brands.list.length);

		res.header(config.countHeader, brands.count);
		res.status(200).json(BrandDto.buildList(brands.list));
	}).then(null, function (error) {
		logger.error(error.toString());
		res.status(500).json(ErrorUtil.unknownError(error));
	});
	/* PROTECTED REGION END */
};


