/**
* This module represents a set of business methods
* @module business/brand/BranchBusiness
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Business_BranchBusiness_init) ENABLED START */

var logger = console;
var repositoryFactory = require('../../repository/RepositoryFactory').getRepositoryFactory();

// Load configurations according to the selected environment
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];

var branchRepository = repositoryFactory.getBranchRepository();

var BranchDto = require('../../dto/brand/BranchDto');
var ErrorUtil = require('../util/ErrorUtil');

var mongoose = require('mongoose');
var SchemaType = mongoose.SchemaType;
var CastError = SchemaType.CastError;

var ValidationError = mongoose.Error.ValidationError;
var ValidatorError = mongoose.Error.ValidatorError;

var extend = require('util')._extend;

/* PROTECTED REGION END */


/**
 * Returns all branches by brand. Results can be refined with optional parameters
* @param {BrandDto} brand -HTTP Type: NAMED- the brand of the branches
* @param {integer} offset -HTTP Type: QUERY- the index where the records start from
* @param {integer} limit -HTTP Type: QUERY- the limit of records to retrieve
* @param {string} orderBy -HTTP Type: QUERY- the order field
* @param {string} orderType -HTTP Type: QUERY- the order type field
* @param {string} id -HTTP Type: QUERY- the id of the branch to be retrieved
* @param {string} name -HTTP Type: QUERY- the name of the branch to be retrieved
* @returns {BranchDto} 
 */
module.exports.getAllBranchesByBrand = function(req, res){
	var brand = req.params.brand;
	var limit = req.query.limit;
	var offset = req.query.offset;
	var orderBy = req.query.orderBy;
	var orderType = req.query.orderType;
	var id = req.query.id;
	var name = req.query.name;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Business_BranchBusiness_getAllBranchesByBrand_body) ENABLED START */
	var promise = branchRepository.getAllBranches(brand, req.query);

	promise.then(function (branches) {

		logger.debug('Total branches: ' + branches.list.length);

		res.header(config.countHeader, branches.count);
		res.status(200).json(BranchDto.buildList(branches.list));
	}).then(null, function (error) {
		logger.error(error.toString());
		res.status(500).json(ErrorUtil.unknownError(error));
	});
	/* PROTECTED REGION END */
};


