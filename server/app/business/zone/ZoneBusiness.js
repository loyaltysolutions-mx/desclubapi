/**
* This module represents a set of business methods
* @module business/zone/ZoneBusiness
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_Business_ZoneBusiness_init) ENABLED START */

var logger = console;
var repositoryFactory = require('../../repository/RepositoryFactory').getRepositoryFactory();

// Load configurations according to the selected environment
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];

var zoneRepository = repositoryFactory.getZoneRepository();

var ZoneDto = require('../../dto/zone/ZoneDto');
var ErrorUtil = require('../util/ErrorUtil');

var mongoose = require('mongoose');
var SchemaType = mongoose.SchemaType;
var CastError = SchemaType.CastError;

var ValidationError = mongoose.Error.ValidationError;
var ValidatorError = mongoose.Error.ValidatorError;

var extend = require('util')._extend;
/* PROTECTED REGION END */


/**
 * Returns all zones with pagination. Results can be refined with optional parameters
* @param {integer} offset -HTTP Type: QUERY- the index where the records start from
* @param {integer} limit -HTTP Type: QUERY- the limit of records to retrieve
* @param {string} orderBy -HTTP Type: QUERY- the order field
* @param {string} orderType -HTTP Type: QUERY- the order type field
* @param {string} name -HTTP Type: QUERY- the name of the zone to be retrieved
* @param {integer} stateId -HTTP Type: QUERY- the state where the zones belongs to
* @returns {ZoneDto} 
 */
module.exports.getAllZones = function(req, res){
	var limit = req.query.limit;
	var offset = req.query.offset;
	var orderBy = req.query.orderBy;
	var orderType = req.query.orderType;
	var name = req.query.name;
	var stateId = req.query.stateId;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_Business_ZoneBusiness_getAllZones_body) ENABLED START */
	var promise = zoneRepository.getAllZones(req.query);

	promise.then(function (zones) {

		logger.debug('Total zones: ' + zones.list.length);

		res.header(config.countHeader, zones.count);
		res.status(200).json(ZoneDto.buildList(zones.list));
	}).then(null, function (error) {
		logger.error(error.toString());
		res.status(500).json(ErrorUtil.unknownError(error));
	});
	/* PROTECTED REGION END */
};


