/**
 * Exports the resource listing for the Swagger specification v1.2
 * @module business/api-doc/APIDocBusiness
 * @see https://github.com/wordnik/swagger-spec
 */
 
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub__business_api_doc_APIDocBusiness_init) ENABLED START */
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];
/* PROTECTED REGION END */

/**
 * the apiDoc method export a json object containing the documentation
 * @param {Request} req the http request
 * @param {Response} req the http response
 */
module.exports.apiDoc = function (req, res) {

	var apiDoc = {
		swagger: '2.0',
		info: {
			description: 'Desclub API',
			title: 'DesclubAPI',
			version: '0.0.1',
			termsOfService: 'http://www.beepquest.com',
			contact: {
				email: 'jhon@beepquest.com',
			},
			"license": {
				"name": 'MIT',
				"url": 'http://www.beepquest.com/license'
			}
		},
		host: config.app.host + ':' + config.app.inversePort,
		basePath: '/',
		schemes: [
			'http'
		],
		tags: [
			// documentation for the module brand
			// documentation for the context brands
			{
				//the base path of the context
				name: 'brands',
				//a general description of the context/module
				description: 'Operations about brands'
			},
			// documentation for the context branches
			{
				//the base path of the context
				name: 'branches',
				//a general description of the context/module
				description: 'Operations about branches'
			},
			// documentation for the module category
			// documentation for the context categories
			{
				//the base path of the context
				name: 'categories',
				//a general description of the context/module
				description: 'Operations about categories'
			},
			// documentation for the context subcategories
			{
				//the base path of the context
				name: 'subcategories',
				//a general description of the context/module
				description: 'Operations about sub-categories'
			},
			// documentation for the module importer
			// documentation for the context importer
			{
				//the base path of the context
				name: 'importer',
				//a general description of the context/module
				description: 'Operations about importer'
			},
			// documentation for the module discount
			// documentation for the context discounts
			{
				//the base path of the context
				name: 'discounts',
				//a general description of the context/module
				description: 'Operations about discounts'
			},
			// documentation for the context discounts
			{
				//the base path of the context
				name: 'discounts',
				//a general description of the context/module
				description: 'Operations about discounts'
			},
			// documentation for the context discounts
			{
				//the base path of the context
				name: 'discounts',
				//a general description of the context/module
				description: 'Operations about discounts'
			},
			// documentation for the context discounts
			{
				//the base path of the context
				name: 'discounts',
				//a general description of the context/module
				description: 'Operations about discounts'
			},
			// documentation for the module zone
			// documentation for the context zones
			{
				//the base path of the context
				name: 'zones',
				//a general description of the context/module
				description: 'Operations about zones'
			},
			// documentation for the module state
			// documentation for the context states
			{
				//the base path of the context
				name: 'states',
				//a general description of the context/module
				description: 'Operations about states'
			},
			// documentation for the module corporate
			// documentation for the context corporates
			{
				//the base path of the context
				name: 'corporates',
				//a general description of the context/module
				description: 'Operations about corporates'
			},
			// documentation for the context corporates
			{
				//the base path of the context
				name: 'corporates',
				//a general description of the context/module
				description: 'Operations about corporates'
			},
			// documentation for the context corporateMemberships
			{
				//the base path of the context
				name: 'corporateMemberships',
				//a general description of the context/module
				description: 'Operations about corporate memberships'
			},
			// documentation for the context corporateMemberships
			{
				//the base path of the context
				name: 'corporateMemberships',
				//a general description of the context/module
				description: 'Operations about corporate memberships'
			},
			// documentation for the context corporateMemberships
			{
				//the base path of the context
				name: 'corporateMemberships',
				//a general description of the context/module
				description: 'Operations about corporate memberships'
			},
			// documentation for the context corporateMemberships
			{
				//the base path of the context
				name: 'corporateMemberships',
				//a general description of the context/module
				description: 'Operations about corporate memberships'
			}
		],
		definitions: {
			//the DTO module brand
			//the specification of the DTO BrandDto
			BrandDto: {
				properties: {
					_id : {
						type: "string"
					},
					brandId : {
						type: "integer"
					},
					name : {
						type: "string"
					},
					logoSmall : {
						type: "string"
					},
					logoBig : {
						type: "string"
					},
					validity_start : {
						type: "string",
						format: "date"
					},
					validity_end : {
						type: "string",
						format: "date"
					},
					url : {
						type: "string"
					},
					mainDiscount : {
						"$ref": "#/definitions/DiscountDto"
					},
					created : {
						type: "string",
						format: "date"
					},
					updated : {
						type: "boolean"
					},
					originalBrand : {
						type: "Object"
					}
				}
			},
			//the specification of the DTO BranchDto
			BranchDto: {
				properties: {
					_id : {
						type: "string"
					},
					branchId : {
						type: "integer"
					},
					brand : {
						"$ref": "#/definitions/BrandDto"
					},
					name : {
						type: "string"
					},
					street : {
						type: "string"
					},
					extNum : {
						type: "string"
					},
					intNum : {
						type: "string"
					},
					colony : {
						type: "string"
					},
					zipCode : {
						type: "string"
					},
					city : {
						type: "string"
					},
					zone : {
						"$ref": "#/definitions/ZoneDto"
					},
					state : {
						"$ref": "#/definitions/StateDto"
					},
					phone : {
						type: "string"
					},
					mainDiscount : {
						"$ref": "#/definitions/DiscountDto"
					},
					location : {
						"$ref": "#/definitions/BranchLocationDto"
					},
					created : {
						type: "string",
						format: "date"
					},
					location : {
						type: "Object"
					},
					originalBranch : {
						type: "Object"
					}
				}
			},
			//the specification of the DTO BranchLocationDto
			BranchLocationDto: {
				properties: {
					type : {
						type: "string"
					},
					coordinates : {
						type: "Array"
					}
				}
			},
			//the DTO module category
			//the specification of the DTO CategoryDto
			CategoryDto: {
				properties: {
					_id : {
						type: "string"
					},
					categoryId : {
						type: "integer"
					},
					originalName : {
						type: "string"
					},
					name : {
						type: "string"
					}
				}
			},
			//the specification of the DTO SubcategoryDto
			SubcategoryDto: {
				properties: {
					_id : {
						type: "string"
					},
					subcategoryId : {
						type: "integer"
					},
					name : {
						type: "string"
					},
					category : {
						"$ref": "#/definitions/CategoryDto"
					}
				}
			},
			//the DTO module discount
			//the specification of the DTO DiscountDto
			DiscountDto: {
				properties: {
					_id : {
						type: "string"
					},
					branch : {
						"$ref": "#/definitions/BranchDto"
					},
					brand : {
						"$ref": "#/definitions/BranchDto"
					},
					category : {
						"$ref": "#/definitions/CategoryDto"
					},
					subcategory : {
						"$ref": "#/definitions/SubcategoryDto"
					},
					cash : {
						type: "string"
					},
					card : {
						type: "string"
					},
					promo : {
						type: "string"
					},
					restriction : {
						type: "string"
					},
					location : {
						type: "Object"
					},
					originalDiscount : {
						type: "Object"
					}
				}
			},
			//the specification of the DTO NearByDiscountDto
			NearByDiscountDto: {
				properties: {
					dis : {
						type: "integer"
					},
					discount : {
						"$ref": "#/definitions/DiscountDto"
					}
				}
			},
			//the DTO module state
			//the specification of the DTO StateDto
			StateDto: {
				properties: {
					_id : {
						type: "string"
					},
					stateId : {
						type: "integer"
					},
					name : {
						type: "string"
					}
				}
			},
			//the DTO module zone
			//the specification of the DTO ZoneDto
			ZoneDto: {
				properties: {
					_id : {
						type: "string"
					},
					zoneId : {
						type: "integer"
					},
					stateId : {
						type: "string"
					},
					name : {
						type: "string"
					}
				}
			},
			//the DTO module corporate
			//the specification of the DTO CorporateDto
			CorporateDto: {
				properties: {
					_id : {
						type: "string"
					},
					name : {
						type: "string"
					},
					membershipPrefix : {
						type: "string"
					},
					created : {
						type: "string",
						format: "date"
					}
				}
			},
			//the specification of the DTO CorporateInputDto
			CorporateInputDto: {
				properties: {
					name : {
						type: "string"
					},
					membershipPrefix : {
						type: "string"
					}
				}
			},
			//the specification of the DTO CorporateMembershipDto
			CorporateMembershipDto: {
				properties: {
					_id : {
						type: "string"
					},
					name : {
						type: "string"
					},
					firstName : {
						type: "string"
					},
					lastName1 : {
						type: "string"
					},
					lastName2 : {
						type: "string"
					},
					email : {
						type: "string"
					},
					membershipNumber : {
						type: "string"
					},
					alreadyUsed : {
						type: "boolean"
					},
					corporate : {
						"$ref": "#/definitions/CorporateDto"
					},
					additionalData : {
						"$ref": "#/definitions/MembershipAdditionalDataDto"
					},
					validThru : {
						type: "string",
						format: "date"
					},
					created : {
						type: "string",
						format: "date"
					}
				}
			},
			//the specification of the DTO CorporateMembershipInputDto
			CorporateMembershipInputDto: {
				properties: {
					firstName : {
						type: "string"
					},
					lastName1 : {
						type: "string"
					},
					lastName2 : {
						type: "string"
					},
					email : {
						type: "string"
					},
					membershipNumber : {
						type: "string"
					},
					additionalData : {
						"$ref": "#/definitions/MembershipAdditionalDataDto"
					}
				}
			},
			//the specification of the DTO CorporateMembershipAlreadyUsedDto
			CorporateMembershipAlreadyUsedDto: {
				properties: {
					alreadyUsed : {
						type: "boolean"
					}
				}
			},
			//the specification of the DTO MembershipAdditionalDataDto
			MembershipAdditionalDataDto: {
				properties: {
					bancomer : {
						type: "Object"
					}
				}
			}
		},
		paths: {
			// documentation for the module brand
			'/v1/desclubAPI/brands': {
			'get': {
				tags: [
					'brands'
				],
				//a brief description of the operation
				summary: "Returns all brands with pagination. Results can be refined with optional parameters",
				description: "Returns all brands with pagination. Results can be refined with optional parameters",
				operationId: "getAllBrands",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter offset
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'offset',
					description: 'the index where the records start from',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter limit
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'limit',
					description: 'the limit of records to retrieve',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter orderBy
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderBy',
					description: 'the order field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter orderType
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderType',
					description: 'the order type field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter id
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'id',
					description: 'the id of the brand to be retrieved',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter name
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'name',
					description: 'the name of the brand to be retrieved',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							type: 'array',
							items: {
								'$ref': "#/definitions/BrandDto"
							}
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			'/v1/desclubAPI/brands/{brand}/branches': {
			'get': {
				tags: [
					'branches'
				],
				//a brief description of the operation
				summary: "Returns all branches by brand. Results can be refined with optional parameters",
				description: "Returns all branches by brand. Results can be refined with optional parameters",
				operationId: "getAllBranchesByBrand",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter brand
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'path',
					name: 'brand',
					description: 'the brand of the branches',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'BrandDto'
				},
				//documentation for the parameter offset
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'offset',
					description: 'the index where the records start from',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter limit
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'limit',
					description: 'the limit of records to retrieve',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter orderBy
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderBy',
					description: 'the order field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter orderType
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderType',
					description: 'the order type field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter id
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'id',
					description: 'the id of the branch to be retrieved',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter name
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'name',
					description: 'the name of the branch to be retrieved',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							type: 'array',
							items: {
								'$ref': "#/definitions/BranchDto"
							}
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			// documentation for the module category
			'/v1/desclubAPI/categories': {
			'get': {
				tags: [
					'categories'
				],
				//a brief description of the operation
				summary: "Returns all categories. Results can be refined with optional parameters",
				description: "Returns all categories. Results can be refined with optional parameters",
				operationId: "getAllCategories",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter offset
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'offset',
					description: 'the index where the records start from',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter limit
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'limit',
					description: 'the limit of records to retrieve',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter orderBy
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderBy',
					description: 'the order field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter orderType
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderType',
					description: 'the order type field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter id
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'id',
					description: 'the id of the category to be retrieved',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter name
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'name',
					description: 'the name of the category to be retrieved',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							type: 'array',
							items: {
								'$ref': "#/definitions/CategoryDto"
							}
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			},
			'post': {
				tags: [
					'categories'
				],
				//a brief description of the operation
				summary: "Create a new category",
				description: "Create a new category",
				operationId: "createCategory",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter category
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'body',
					name: 'category',
					description: 'The category to be created',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					schema: {
						"$ref": "#/definitions/CategoryDto"
					},
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							'$ref': "#/definitions/CategoryDto"
						}
					},
					'500': {
						description: 'Resource already exists',
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			'/v1/desclubAPI/categories/{category}/subcategories': {
			'get': {
				tags: [
					'subcategories'
				],
				//a brief description of the operation
				summary: "Returns all sub-categories. Results can be refined with optional parameters",
				description: "Returns all sub-categories. Results can be refined with optional parameters",
				operationId: "getAllSubcategories",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter category
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'path',
					name: 'category',
					description: 'the category where to search subcategories from',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'CategoryDto'
				},
				//documentation for the parameter offset
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'offset',
					description: 'the index where the records start from',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter limit
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'limit',
					description: 'the limit of records to retrieve',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter orderBy
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderBy',
					description: 'the order field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter orderType
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderType',
					description: 'the order type field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter id
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'id',
					description: 'the id of the sub-category to be retrieved',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter name
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'name',
					description: 'the name of the sub-category to be retrieved',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							type: 'array',
							items: {
								'$ref': "#/definitions/SubcategoryDto"
							}
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			},
			'post': {
				tags: [
					'subcategories'
				],
				//a brief description of the operation
				summary: "Create a new sub-category",
				description: "Create a new sub-category",
				operationId: "createSubcategory",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter category
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'path',
					name: 'category',
					description: 'the category where the sub-category will be assigned to',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter subcategory
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'body',
					name: 'subcategory',
					description: 'The sub-category to be created',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					schema: {
						"$ref": "#/definitions/SubcategoryDto"
					},
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							'$ref': "#/definitions/SubcategoryDto"
						}
					},
					'500': {
						description: 'Resource already exists',
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			// documentation for the module importer
			'/v1/desclubAPI/importer': {
			'post': {
				tags: [
					'importer'
				],
				//a brief description of the operation
				summary: "Performs the import from the desclub original API",
				description: "Performs the import from the desclub original API",
				operationId: "doImport",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							'$ref': "#/definitions/undefined"
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			// documentation for the module discount
			'/v1/desclubAPI/discounts': {
			'get': {
				tags: [
					'discounts'
				],
				//a brief description of the operation
				summary: "Returns all discounts with pagination. Results can be refined with optional parameters",
				description: "Returns all discounts with pagination. Results can be refined with optional parameters",
				operationId: "getAllDiscounts",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter offset
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'offset',
					description: 'the index where the records start from',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter limit
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'limit',
					description: 'the limit of records to retrieve. Default value is 10. Maximum value is 100',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter orderBy
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderBy',
					description: 'the order field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter orderType
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderType',
					description: 'the order type field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter category
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'category',
					description: 'the id of the category',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter categoryName
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'categoryName',
					description: 'the name of the category',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter subcategory
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'subcategory',
					description: 'the id of the sub-category',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter subcategoryName
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'subcategoryName',
					description: 'the name of the sub-category',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter brand
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'brand',
					description: 'the id of the brand',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter brandName
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'brandName',
					description: 'the name of the brand',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter branch
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'branch',
					description: 'the id of the branch',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter branchName
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'branchName',
					description: 'the name of the branch',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter state
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'state',
					description: 'the _id of the state',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter zone
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'zone',
					description: 'the _id of the zone',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter colonyName
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'colonyName',
					description: 'the name of the colony',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							type: 'array',
							items: {
								'$ref': "#/definitions/DiscountDto"
							}
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			'/v1/desclubAPI/discounts/{id}': {
			'get': {
				tags: [
					'discounts'
				],
				//a brief description of the operation
				summary: "Return a discount according to the given id",
				description: "Return a discount according to the given id",
				operationId: "getDiscountById",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter id
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'path',
					name: 'id',
					description: 'the _id of the discount to be returned',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							'$ref': "#/definitions/DiscountDto"
						}
					},
					'404': {
						description: 'Not found. The discount does not exist',
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			'/v1/desclubAPI/recommendedDiscounts': {
			'get': {
				tags: [
					'discounts'
				],
				//a brief description of the operation
				summary: "Returns all recommended discounts",
				description: "Returns all recommended discounts",
				operationId: "getRecommendedDiscounts",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter offset
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'offset',
					description: 'the index where the records start from',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter limit
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'limit',
					description: 'the limit of records to retrieve. Default value is 10. Maximum value is 100',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter orderBy
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderBy',
					description: 'the order field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter orderType
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderType',
					description: 'the order type field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							type: 'array',
							items: {
								'$ref': "#/definitions/DiscountDto"
							}
						}
					},
					'400': {
						description: 'Invalid parameters. Required fields missing',
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			'/v1/desclubAPI/nearByDiscounts': {
			'get': {
				tags: [
					'discounts'
				],
				//a brief description of the operation
				summary: "Returns all near by discounts with pagination. Results can be refined with optional parameters",
				description: "Returns all near by discounts with pagination. Results can be refined with optional parameters",
				operationId: "getAllNearByDiscounts",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter minDistance
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'minDistance',
					description: 'The minimum distance from the center point that the documents must be. Default 0.',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter maxDistance
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'maxDistance',
					description: 'The maximum distance from the center point that the documents can be.',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter latitude
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'latitude',
					description: 'the latitude coordinate. e.g. 19.423841',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter longitude
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'longitude',
					description: 'the longitude coordinate. e.g. -99.133302',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter limit
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'limit',
					description: 'the limit of records to retrieve. Default value is 10. Maximum value is 100',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter category
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'category',
					description: 'the id of the category',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter categoryName
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'categoryName',
					description: 'the name of the category',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter subcategory
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'subcategory',
					description: 'the id of the sub-category',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter subcategoryName
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'subcategoryName',
					description: 'the name of the sub-category',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter brand
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'brand',
					description: 'the id of the brand',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter brandName
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'brandName',
					description: 'the name of the brand',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter branch
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'branch',
					description: 'the id of the branch',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter branchName
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'branchName',
					description: 'the name of the branch',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter state
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'state',
					description: 'the _id of the state',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter zone
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'zone',
					description: 'the _id of the zone',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter colonyName
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'colonyName',
					description: 'the name of the colony',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							type: 'array',
							items: {
								'$ref': "#/definitions/NearByDiscountDto"
							}
						}
					},
					'400': {
						description: 'Invalid parameters. Required fields missing',
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			// documentation for the module zone
			'/v1/desclubAPI/zones': {
			'get': {
				tags: [
					'zones'
				],
				//a brief description of the operation
				summary: "Returns all zones with pagination. Results can be refined with optional parameters",
				description: "Returns all zones with pagination. Results can be refined with optional parameters",
				operationId: "getAllZones",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter offset
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'offset',
					description: 'the index where the records start from',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter limit
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'limit',
					description: 'the limit of records to retrieve',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter orderBy
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderBy',
					description: 'the order field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter orderType
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderType',
					description: 'the order type field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter name
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'name',
					description: 'the name of the zone to be retrieved',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter stateId
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'stateId',
					description: 'the state where the zones belongs to',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							type: 'array',
							items: {
								'$ref': "#/definitions/ZoneDto"
							}
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			// documentation for the module state
			'/v1/desclubAPI/states': {
			'get': {
				tags: [
					'states'
				],
				//a brief description of the operation
				summary: "Returns all states with pagination. Results can be refined with optional parameters",
				description: "Returns all states with pagination. Results can be refined with optional parameters",
				operationId: "getAllStates",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter offset
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'offset',
					description: 'the index where the records start from',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter limit
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'limit',
					description: 'the limit of records to retrieve',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter orderBy
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderBy',
					description: 'the order field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter orderType
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderType',
					description: 'the order type field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter name
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'name',
					description: 'the name of the state to be retrieved',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							type: 'array',
							items: {
								'$ref': "#/definitions/StateDto"
							}
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			// documentation for the module corporate
			'/v1/desclubAPI/corporates': {
			'get': {
				tags: [
					'corporates'
				],
				//a brief description of the operation
				summary: "Returns all corporate items with pagination. Results can be refined with optional parameters",
				description: "Returns all corporate items with pagination. Results can be refined with optional parameters",
				operationId: "getAllCorporates",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter offset
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'offset',
					description: 'the index where the records start from',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter limit
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'limit',
					description: 'the limit of records to retrieve',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter orderBy
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderBy',
					description: 'the order field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter orderType
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderType',
					description: 'the order type field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter name
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'name',
					description: 'the name of the corporate to be retrieved',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							type: 'array',
							items: {
								'$ref': "#/definitions/CorporateDto"
							}
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			},
			'post': {
				tags: [
					'corporates'
				],
				//a brief description of the operation
				summary: "Creates a new corporate",
				description: "Creates a new corporate",
				operationId: "createCorporate",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter corporate
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'body',
					name: 'corporate',
					description: 'the corporate to be created',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					schema: {
						"$ref": "#/definitions/CorporateInputDto"
					},
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							'$ref': "#/definitions/CorporateDto"
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			'/v1/desclubAPI/corporates/{id}': {
			'get': {
				tags: [
					'corporates'
				],
				//a brief description of the operation
				summary: "Retrieves a corporate by id",
				description: "Retrieves a corporate by id",
				operationId: "getCorporateById",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter id
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'path',
					name: 'id',
					description: 'the _id of the corporate to retrieve',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							'$ref': "#/definitions/CorporateDto"
						}
					},
					'404': {
						description: 'Not found',
					},
					'500': {
						description: 'Unknown error',
					}
				}
			},
			'put': {
				tags: [
					'corporates'
				],
				//a brief description of the operation
				summary: "Updates a corporate",
				description: "Updates a corporate",
				operationId: "updateCorporate",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter id
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'path',
					name: 'id',
					description: 'the _id of the corporate to update',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter corporate
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'body',
					name: 'corporate',
					description: 'the corporate to update',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					schema: {
						"$ref": "#/definitions/CorporateInputDto"
					},
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							'$ref': "#/definitions/CorporateDto"
						}
					},
					'404': {
						description: 'Not found',
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			'/v1/desclubAPI/corporateMemberships': {
			'get': {
				tags: [
					'corporateMemberships'
				],
				//a brief description of the operation
				summary: "Returns all corporate memberships with pagination. Results can be refined with optional parameters",
				description: "Returns all corporate memberships with pagination. Results can be refined with optional parameters",
				operationId: "getAllCorporateMemberships",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter offset
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'offset',
					description: 'the index where the records start from',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter limit
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'limit',
					description: 'the limit of records to retrieve',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'integer'
				},
				//documentation for the parameter orderBy
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderBy',
					description: 'the order field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter orderType
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'orderType',
					description: 'the order type field',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter name
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'name',
					description: 'the name of the owner of the membership',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter corporate
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'corporate',
					description: 'the id of the corporate where the membership belongs to',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter corporateName
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'corporateName',
					description: 'the name of the corporate where the membership belongs to',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter membershipNumber
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'query',
					name: 'membershipNumber',
					description: 'the id of the corporate where the membership belongs to',
					//if the parameter is mandatory or not
					required: false,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							type: 'array',
							items: {
								'$ref': "#/definitions/CorporateMembershipDto"
							}
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			},
			'post': {
				tags: [
					'corporateMemberships'
				],
				//a brief description of the operation
				summary: "Creates a single corporate membership",
				description: "Creates a single corporate membership",
				operationId: "createSingleCorporateMembership",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter membership
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'body',
					name: 'membership',
					description: 'the corporate membership to be created',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					schema: {
						"$ref": "#/definitions/CorporateMembershipInputDto"
					},
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							'$ref': "#/definitions/CorporateMembershipDto"
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			'/v1/desclubAPI/corporateMemberships/{id}': {
			'put': {
				tags: [
					'corporateMemberships'
				],
				//a brief description of the operation
				summary: "Updates a corporate membership status",
				description: "Updates a corporate membership status",
				operationId: "updateCorporateMembershipStatus",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter id
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'path',
					name: 'id',
					description: 'the _id of the corporate membership to update',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter alreadyUsed
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'body',
					name: 'alreadyUsed',
					description: 'the corporate membership alreadyUsed field to update. Must be true if the membership should be marked as used.',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					schema: {
						"$ref": "#/definitions/CorporateMembershipAlreadyUsedDto"
					},
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							'$ref': "#/definitions/CorporateMembershipDto"
						}
					},
					'404': {
						description: 'Not found',
					},
					'400': {
						description: 'Invalid alreadyUsed entity. Check desclub documentation',
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			'/v1/desclubAPI/corporateMembershipsByNumber/{number}': {
			'get': {
				tags: [
					'corporateMemberships'
				],
				//a brief description of the operation
				summary: "Retrieves a corporate membership by number",
				description: "Retrieves a corporate membership by number",
				operationId: "getCorporateMembershipByNumber",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter number
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'path',
					name: 'number',
					description: 'the number of the corporate membership to retrieve',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'string'
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							'$ref': "#/definitions/CorporateMembershipDto"
						}
					},
					'404': {
						description: 'Not found',
					},
					'500': {
						description: 'Unknown error',
					}
				}
			},
			'put': {
				tags: [
					'corporateMemberships'
				],
				//a brief description of the operation
				summary: "updates the corporate membership email by number",
				description: "updates the corporate membership email by number",
				operationId: "updateCorporateMembershipByNumber",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter number
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'path',
					name: 'number',
					description: 'the number of the corporate membership to retrieve',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter email
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'body',
					name: 'email',
					description: 'the email to update',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					schema: {
						"$ref": "#/definitions/string"
					},
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							'$ref': "#/definitions/CorporateMembershipDto"
						}
					},
					'404': {
						description: 'Not found',
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			},
			'/v1/desclubAPI/corporate/{corporate}/corporateMemberships': {
			'post': {
				tags: [
					'corporateMemberships'
				],
				//a brief description of the operation
				summary: "Creates one or multiple corporate memberships",
				description: "Creates one or multiple corporate memberships",
				operationId: "createCorporateMembership",
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				],
				//the list of parameters the operation is accepting
				parameters: [
				//documentation for the parameter corporate
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'path',
					name: 'corporate',
					description: 'the corporate _id where the memberships are being created',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					type: 'string'
				},
				//documentation for the parameter memberships
				{
					//the type according to the HTTP protocol (e.g. body, query)
					in: 'body',
					name: 'memberships',
					description: 'the array of corporate memberships to be created. additionalData, membershipNumber and email are optional. If membershipNumber is not present, a random one is generated',
					//if the parameter is mandatory or not
					required: true,
					//the data type of the parameter
					schema: {
						"$ref": "#/definitions/CorporateMembershipInputDto"
					},
				}
				],
				//the corresponding response messages for this operation
				responses: {
					'200': {
						description: 'OK',
						schema: {
							type: 'array',
							items: {
								'$ref': "#/definitions/CorporateMembershipDto"
							}
						}
					},
					'500': {
						description: 'Unknown error',
					}
				}
			}
			}
		}
	};
	
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub__business_api_doc_APIDocBusiness_additionalRoutes) ENABLED START */
	/* PROTECTED REGION END */
	
	res.status(200).json(apiDoc);
};

