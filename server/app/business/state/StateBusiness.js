/**
* This module represents a set of business methods
* @module business/state/StateBusiness
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_Business_StateBusiness_init) ENABLED START */

var logger = console;
var repositoryFactory = require('../../repository/RepositoryFactory').getRepositoryFactory();

// Load configurations according to the selected environment
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];

var stateRepository = repositoryFactory.getStateRepository();

var StateDto = require('../../dto/state/StateDto');
var ErrorUtil = require('../util/ErrorUtil');

var mongoose = require('mongoose');
var SchemaType = mongoose.SchemaType;
var CastError = SchemaType.CastError;

var ValidationError = mongoose.Error.ValidationError;
var ValidatorError = mongoose.Error.ValidatorError;

var extend = require('util')._extend;
/* PROTECTED REGION END */


/**
 * Returns all states with pagination. Results can be refined with optional parameters
* @param {integer} offset -HTTP Type: QUERY- the index where the records start from
* @param {integer} limit -HTTP Type: QUERY- the limit of records to retrieve
* @param {string} orderBy -HTTP Type: QUERY- the order field
* @param {string} orderType -HTTP Type: QUERY- the order type field
* @param {string} name -HTTP Type: QUERY- the name of the state to be retrieved
* @returns {StateDto} 
 */
module.exports.getAllStates = function(req, res){
	var limit = req.query.limit;
	var offset = req.query.offset;
	var orderBy = req.query.orderBy;
	var orderType = req.query.orderType;
	var name = req.query.name;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_Business_StateBusiness_getAllStates_body) ENABLED START */
    var promise = stateRepository.getAllStates(req.query);

    promise.then(function (states) {

        logger.debug('Total states: ' + states.list.length);

        res.header(config.countHeader, states.count);
        res.status(200).json(StateDto.buildList(states.list));
    }).then(null, function (error) {
        logger.error(error.toString());
        res.status(500).json(ErrorUtil.unknownError(error));
    });
    /* PROTECTED REGION END */
};


