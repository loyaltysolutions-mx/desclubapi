/**
* This module represents a set of business methods
* @module business/importer/ImporterBusiness
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_importer_Business_ImporterBusiness_init) ENABLED START */

var logger = console;
var repositoryFactory = require('../../repository/RepositoryFactory').getRepositoryFactory();

// Load configurations according to the selected environment
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];

var querystring = require('querystring');

var ErrorUtil = require('../util/ErrorUtil');
var HttpUtil = require('../util/HttpUtil');

var brandRepository = repositoryFactory.getBrandRepository();
var branchRepository = repositoryFactory.getBranchRepository();
var subcategoryRepository = repositoryFactory.getSubcategoryRepository();
var discountRepository = repositoryFactory.getDiscountRepository();
var zoneRepository = repositoryFactory.getZoneRepository();
var stateRepository = repositoryFactory.getStateRepository();

var Q = require('q');

var mongoose = require('mongoose');

var extend = require('util')._extend;
/* PROTECTED REGION END */


/**
 * Performs the import from the desclub original API
* @returns {undefined} 
 */
module.exports.doImport = function(req, res){

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_importer_Business_ImporterBusiness_doImport_body) ENABLED START */

    /**
     *
     * @param brand
     * @param branch
     * @param branchFromBrand
     * @param subcategory
     * @param discount
     * @returns {{originalDiscount: *, branch: *, branchInfo: *, brand: *, brandInfo: *, categoryInfo, category: *, subcategory: *, subcategoryInfo: *, cash: *, card: *, promo: *, restriction: *, location}}
     */
    var mapDiscountIntoDiscount = function (brand, branch, branchFromBrand, subcategory, discount) {

        delete discount["Sucursales"];

        var categoryInfo = JSON.parse(JSON.stringify(subcategory.category));
        var location = JSON.parse(JSON.stringify(branch.location));

        var mappedDiscount = {
            originalDiscount: discount,

            branch: branch._id,
            branchInfo: branch,

            brand: brand._id,
            brandInfo: brand,

            categoryInfo: categoryInfo,
            category: categoryInfo._id,

            subcategory: subcategory._id,
            subcategoryInfo: subcategory,


            cash: branchFromBrand['Efectivo'],
            card: branchFromBrand['Tarjeta'],
            promo: branchFromBrand['Promocion'],
            restriction: branchFromBrand['Restricciones'],
            location: location,
            updated: true

        };

        return mappedDiscount;

    };

    /**
     *
     * @param brand
     * @param branch
     * @param branchFromBrand
     * @param subcategory
     * @param discount
     * @returns {Promise|MPromise}
     */
    var checkDiscount = function (brand, branch, branchFromBrand, subcategory, discount) {

        var discountPromise = discountRepository.getDiscountByBranch(branch._id);

        return discountPromise.then(function (checkedDiscount) {

            //logger.info('++++ Terminando descuento: ' + discountCounter);
            discountCounter = discountCounter + 1;

            var mappedDiscount = mapDiscountIntoDiscount(brand, branch, branchFromBrand, subcategory, discount);

            //if already exists, update
            if (typeof checkedDiscount != 'undefined' && checkedDiscount != null) {
                return discountRepository.updateDiscount(checkedDiscount._id, mappedDiscount);
            } else {
                return discountRepository.createDiscount(mappedDiscount);
            }

        }).then(null, function (error) {
            logger.error(error.toString());
            logger.trace(error);

        });

    };

    /**
     *
     * @param brand
     * @param branchFromBrand
     * @returns {{originalBranch: *, branchId: *, brand: *, name: *, street: *, extNum: *, intNum: *, colony: *, zipCode: *, city: *, state: *, phone: *, location: {type: string, coordinates: number[]}, updated: boolean}}
     */
    var mapDiscountIntoBranch = function (brand, branchFromBrand, state, zone) {

        var location = {
            type: 'Point',
            coordinates: [0, 0]
        };

        if (typeof branchFromBrand['Longitud'] != 'undefined' && branchFromBrand['Longitud'].length > 0 && branchFromBrand['Latitud'] != 'undefined' && branchFromBrand['Latitud'].length > 0) {
            location = {
                type: 'Point',
                coordinates: [parseFloat(branchFromBrand['Longitud']), parseFloat(branchFromBrand['Latitud'])]
            };
        } else {
            logger.info('La sucursal ' + branchFromBrand['id_Sucursal'] + ' de la marca ' + branchFromBrand['Marca'] + ' no tiene posiciones geográficas: ' + branchFromBrand['Longitud'] + ' ' + branchFromBrand['Latitud']);
        }

        var branch = {
            originalBranch: branchFromBrand,
            branchId: branchFromBrand['id_Sucursal'],
            brand: brand,
            name: branchFromBrand['Sucursal'],
            street: branchFromBrand['Calle'],
            extNum: branchFromBrand['Numero_exterior'],
            intNum: branchFromBrand['Numero_interior'],
            colony: branchFromBrand['Colonia'],
            zipCode: branchFromBrand['Codigo_Postal'],
            city: branchFromBrand['Municipio'],
            state: state,
            zone: zone,
            phone: branchFromBrand['Telefono'],
            location: location,
            updated: true
        };

        if (branch.name.length == 0) {
            logger.info('La sucursal ' + branch.branchId + ' de la marca ' + branchFromBrand['Marca'] + ' no tiene nombre.');
            branch.name = branchFromBrand['Marca'];
        }

        return branch;

    };

    /**
     *
     * @param branchPromise
     * @param brand
     * @param branchFromBrand
     * @param globalBranch
     * @param discount
     * @returns {Promise|MPromise}
     */
    var performBranchOperation = function (branchPromise, brand, branchFromBrand, globalBranch, discount, state, zone) {

        //logger.debug('performBranchOperation ====>>> estado: ' + state);

        return branchPromise.then(function (branch) {
            var mappedBranch = mapDiscountIntoBranch(brand._id, branchFromBrand, state, zone);

            //logger.debug("+++++ Procesando sucursal" + mappedBranch.name);

            //if already exists, update
            if (typeof branch != 'undefined' && branch != null) {
                //logger.debug("La sucursal " + branch.name + ' ya existe');
                return branchRepository.updateBranch(brand._id, branch._id, mappedBranch);
            } else {
                //logger.debug("---------- La sucursal con id " + mappedBranch.branchId + ' es nueva');
                return branchRepository.createBranch(brand._id, mappedBranch);
            }

        }).then(function (createdOrUpdatedBranch) {

            globalBranch = createdOrUpdatedBranch;
            return subcategoryRepository.getSubcategoryByDesclubId(discount['id_subcategoria']);

        }).then(function (subcategory) {

            if (typeof subcategory != 'undefined' && subcategory != null) {
                return checkDiscount(brand, globalBranch, branchFromBrand, subcategory, discount);
            } else {
                throw new Error('Subcategory: ' + discount['id_subcategoria'] + ' not found');
            }

        }).then(null, function (error) {
            //logger.trace(error);
            logger.trace(error);
        });
    };

    /**
     * Find zone for branch
     * @param branchPromise
     * @param brand
     * @param branchFromBrand
     * @param globalBranch
     * @param discount
     * @param state
     * @returns {Promise|MPromise|*}
     */
    var findZoneForBranch = function(branchPromise, brand, branchFromBrand, globalBranch, discount, state){

        //logger.debug('findZoneForBranch ==== estado: ' + state);

        return zoneRepository.getZoneByZoneId(branchFromBrand['id_Zona_Sucursal']).then(function (foundZone) {
            if (typeof foundZone != 'undefined' && foundZone != null) {

                return performBranchOperation(branchPromise, brand, branchFromBrand, globalBranch, discount, state, foundZone._id);

            } else {
                logger.error('La zona: ' + branchFromBrand['id_Zona_Sucursal'] + ' para la marca ' + brand.name + ' no fue encontrada. Asignando fake');
                return performBranchOperation(branchPromise, brand, branchFromBrand, globalBranch, discount, state, mongoose.Types.ObjectId());
            }
        });
    };

    /**
     * Find state for branch
     * @param branchPromise
     * @param brand
     * @param branchFromBrand
     * @param globalBranch
     * @param discount
     * @returns {Promise|MPromise|*}
     */
    var findStateForBranch = function(branchPromise, brand, branchFromBrand, globalBranch, discount){

        return stateRepository.getStateByStateId(branchFromBrand['id_Estado']).then(function (foundState) {

            //logger.info('Estado a buscar: ' + branchFromBrand['id_Estado'] + ' estado encontrado: ' + foundState.stateId + ' -- ' + foundState.name + ' ** '+ foundState._id);

            if (typeof foundState != 'undefined' && foundState != null) {
                return findZoneForBranch(branchPromise, brand, branchFromBrand, globalBranch, discount, foundState._id)
            } else {
                logger.error('El estado: ' + branchFromBrand['id_Estado'] + ' para la marca ' + brand.name + ' no fue encontrado. Asignando fake');
                return findZoneForBranch(branchPromise, brand, branchFromBrand, globalBranch, discount, mongoose.Types.ObjectId())
            }
        });
    };

    /**
     *
     * @param brand
     * @param discount
     * @returns {*}
     */
    var checkBranch = function (brand, discount) {

        //logger.debug("Procesando " + discount["Sucursales"].length + ' sucursales de la marca: ' + brand.name);

        var promisesArray = [];

        for (var i = 0; i < discount["Sucursales"].length; i++) {

            var branchFromBrand = discount["Sucursales"][i];

            var branchPromise = branchRepository.getBranchByDesclubId(branchFromBrand['id_Sucursal']);
            var globalBranch = null;


            promisesArray.push(findStateForBranch(branchPromise, brand, branchFromBrand, globalBranch, discount));

        }

        return Q.all(promisesArray);

    };

    /**
     *
     * @param discount
     * @returns {{originalBrand: *, brandId: *, name: *, logo: *, validity_start: *, validity_end: *, url: *, updated: boolean}}
     */
    var mapDiscountIntoBrand = function (discount) {

        var discountCopy = extend({}, discount);

        delete discountCopy["Sucursales"];

        var brand = {
            originalBrand: discountCopy,
            brandId: discount['Id_Marca'],
            name: discount['Marca'],
            logoSmall: discount['logo_chico'],
            logoBig: discount['logo_grande'],
            validity_start: discount['inicia_contrato'],
            validity_end: discount['termina_contrato'],
            url: discount['URL_pagina'],
            updated: true
        };

        return brand;
    };

    /**
     *
     * @param discount
     * @returns {Promise|MPromise}
     */
    var checkBrand = function (discount) {

        var brandPromise = brandRepository.getBrandByDesclubId(discount['Id_Marca']);

        return brandPromise.then(function (brand) {
            var mappedBrand = mapDiscountIntoBrand(discount);

            //if already exists, update
            if (typeof brand != 'undefined' && brand != null) {
                return brandRepository.updateBrand(brand._id, mappedBrand);
            } else {
                return brandRepository.createBrand(mappedBrand);
            }
        }).then(function (createdOrUpdatedBrand) {

            return checkBranch(createdOrUpdatedBrand, discount);

        }).then(null, function (error) {
            logger.trace(error);
        });

    };

    /**
     *
     * @param discountList
     * @param offset
     * @returns {*}
     */
    var mapDiscountList = function (discountList, offset) {

        var discounts = discountList.Descuentos;
        if (typeof  discounts === 'undefined') {
            throw Error("No hay descuentos disponibles")
        }

        //logger.debug('Procesando ' + discounts.length + ' descuentos... desde el ' + offset);

        var promisesArray = [];

        for (var i = 0; i < discounts.length; i++) {
            promisesArray.push(checkBrand(discounts[i]));
        }


        return Q.all(promisesArray);

    };

    /**
     *
     * @param offset
     * @param limit
	 hostname: 'www.desclub.com.mx',
            path: '/intranet/wsdesclub/index.php',
     */
    var makeRequest = function (offset, limit) {

        var options = {
            host: 'ws.desclub.com.mx',
            path: '/wsdesclub/index.php',

            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        var queryParams = {
            m: 'descuentos',
            o: offset,
            l: limit
        };

        options.path = options.path + '?' + querystring.stringify(queryParams);

        var promise = HttpUtil.makePromisedRequest(options);

        promise.then(function (response) {

            if (response.response != null) {
                return mapDiscountList(response.response, offset);
            } else {
                logger.error('******************************');
                logger.error('******************************');
                logger.error('Hubo un error con el descuento #' + discountCounter);
                logger.error('******************************');
                logger.error('******************************');

                var deferred = Q.defer();

                deferred.resolve(['dummy', 'result']);

                return deferred.promise;
            }

        }).then(function (results) {

            if (typeof results !== 'undefined' && results != null) {

                offset = offset + limit;

                if (results.length > 0 && offset < maxDiscount) {
                    makeRequest(offset, limit);
                } else {
                    //remove not updated discounts
                    discountRepository.removeAllWithUpdated(false);

                    logger.info('---- Finished!!!')
                }
            }

        }).then(null, function (error) {
            logger.trace(error);
            return res.status(500).json(ErrorUtil.unknownError("Unknown error", error));
        });
    };

    //the most secure implementation :P
    if (typeof req.body.doIt != 'undefined') {

        var discountCounter = 1;

        var maxDiscount = 300000;

        var offset = 0;
        var limit = 1;

        logger.info("++++ Import started!!");

        //first mark all discounts as not updated
        var promise = discountRepository.markAllUpdated(false);
        promise.then(function (markedDiscounts) {

            makeRequest(offset, limit);

            return res.status(200).json({message: markedDiscounts});

        }).then(null, function (error) {
            logger.error(error);
        });


    } else {
        //security breach, get out of there
        return res.status(500).json({error: 'Manual imports are only allowed for admins'})
    }
    /* PROTECTED REGION END */
};


