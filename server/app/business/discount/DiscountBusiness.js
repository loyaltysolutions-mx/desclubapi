/**
* This module represents a set of business methods
* @module business/discount/DiscountBusiness
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Business_DiscountBusiness_init) ENABLED START */

var logger = console;
var repositoryFactory = require('../../repository/RepositoryFactory').getRepositoryFactory();

// Load configurations according to the selected environment
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];

var discountRepository = repositoryFactory.getDiscountRepository();

var DiscountDto = require('../../dto/discount/DiscountDto');
var ErrorUtil = require('../util/ErrorUtil');

var mongoose = require('mongoose');
var SchemaType = mongoose.SchemaType;
var CastError = SchemaType.CastError;

var ValidationError = mongoose.Error.ValidationError;
var ValidatorError = mongoose.Error.ValidatorError;


/* PROTECTED REGION END */


/**
 * Returns all discounts with pagination. Results can be refined with optional parameters
* @param {integer} offset -HTTP Type: QUERY- the index where the records start from
* @param {integer} limit -HTTP Type: QUERY- the limit of records to retrieve. Default value is 10. Maximum value is 100
* @param {string} orderBy -HTTP Type: QUERY- the order field
* @param {string} orderType -HTTP Type: QUERY- the order type field
* @param {string} category -HTTP Type: QUERY- the id of the category
* @param {string} categoryName -HTTP Type: QUERY- the name of the category
* @param {string} subcategory -HTTP Type: QUERY- the id of the sub-category
* @param {string} subcategoryName -HTTP Type: QUERY- the name of the sub-category
* @param {string} brand -HTTP Type: QUERY- the id of the brand
* @param {string} brandName -HTTP Type: QUERY- the name of the brand
* @param {string} branch -HTTP Type: QUERY- the id of the branch
* @param {string} branchName -HTTP Type: QUERY- the name of the branch
* @param {string} state -HTTP Type: QUERY- the _id of the state
* @param {string} zone -HTTP Type: QUERY- the _id of the zone
* @param {string} colonyName -HTTP Type: QUERY- the name of the colony
* @returns {DiscountDto} 
 */
module.exports.getAllDiscounts = function(req, res){
	var limit = req.query.limit;
	var offset = req.query.offset;
	var orderBy = req.query.orderBy;
	var orderType = req.query.orderType;
	var category = req.query.category;
	var categoryName = req.query.categoryName;
	var subcategory = req.query.subcategory;
	var subcategoryName = req.query.subcategoryName;
	var brand = req.query.brand;
	var brandName = req.query.brandName;
	var branch = req.query.branch;
	var branchName = req.query.branchName;
	var state = req.query.state;
	var zone = req.query.zone;
	var colonyName = req.query.colonyName;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Business_DiscountBusiness_getAllDiscounts_body) ENABLED START */
    var promise = discountRepository.getAllDiscounts(req.query);

    promise.then(function (discounts) {

        logger.debug('Total discounts: ' + discounts.list.length);

        res.header(config.countHeader, discounts.count);
        res.status(200).json(DiscountDto.buildList(discounts.list));
    }).then(null, function (error) {

        logger.trace(error);

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));

    });
    /* PROTECTED REGION END */
};


/**
 * Returns all near by discounts with pagination. Results can be refined with optional parameters
* @param {integer} minDistance -HTTP Type: QUERY- The minimum distance from the center point that the documents must be. Default 0.
* @param {integer} maxDistance -HTTP Type: QUERY- The maximum distance from the center point that the documents can be.
* @param {integer} latitude -HTTP Type: QUERY- the latitude coordinate. e.g. 19.423841
* @param {integer} longitude -HTTP Type: QUERY- the longitude coordinate. e.g. -99.133302
* @param {integer} limit -HTTP Type: QUERY- the limit of records to retrieve. Default value is 10. Maximum value is 100
* @param {string} category -HTTP Type: QUERY- the id of the category
* @param {string} categoryName -HTTP Type: QUERY- the name of the category
* @param {string} subcategory -HTTP Type: QUERY- the id of the sub-category
* @param {string} subcategoryName -HTTP Type: QUERY- the name of the sub-category
* @param {string} brand -HTTP Type: QUERY- the id of the brand
* @param {string} brandName -HTTP Type: QUERY- the name of the brand
* @param {string} branch -HTTP Type: QUERY- the id of the branch
* @param {string} branchName -HTTP Type: QUERY- the name of the branch
* @param {string} state -HTTP Type: QUERY- the _id of the state
* @param {string} zone -HTTP Type: QUERY- the _id of the zone
* @param {string} colonyName -HTTP Type: QUERY- the name of the colony
* @returns {NearByDiscountDto} 
 */
module.exports.getAllNearByDiscounts = function(req, res){
	var minDistance = req.query.minDistance;
	var maxDistance = req.query.maxDistance;
	var latitude = req.query.latitude;
	var longitude = req.query.longitude;
	var limit = req.query.limit;
	var category = req.query.category;
	var categoryName = req.query.categoryName;
	var subcategory = req.query.subcategory;
	var subcategoryName = req.query.subcategoryName;
	var brand = req.query.brand;
	var brandName = req.query.brandName;
	var branch = req.query.branch;
	var branchName = req.query.branchName;
	var state = req.query.state;
	var zone = req.query.zone;
	var colonyName = req.query.colonyName;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Business_DiscountBusiness_getAllNearByDiscounts_body) ENABLED START */

    if (typeof latitude == 'undefined' || latitude.length == 0 || typeof longitude == 'undefined' || longitude.length == 0) {
        return res.status(400).json(ErrorUtil.invalidParamError('valid latitude and longitude coordinates must be provided'));
    }

    var promise = discountRepository.getAllNearByDiscounts(req.query);

    promise.then(function (discounts) {

        logger.debug('Total discounts: ' + discounts.length);

        var formattedDiscounts = [];
        for (var i = 0; i < discounts.length; i++) {

            var originalDiscount = JSON.parse(JSON.stringify(discounts[i]));

            //fix logos
            originalDiscount.obj.brandInfo.logoSmall = originalDiscount.obj.originalDiscount.logo_chico;
            originalDiscount.obj.brandInfo.logoBig = originalDiscount.obj.originalDiscount.logo_grande;

            originalDiscount.obj.brand = originalDiscount.obj.brandInfo;
            originalDiscount.obj.branch = originalDiscount.obj.branchInfo;
            originalDiscount.obj.category = originalDiscount.obj.categoryInfo;
            originalDiscount.obj.subcategory = originalDiscount.obj.subcategoryInfo;

            var formattedDiscount = {
                dis: originalDiscount.dis,
                discount: DiscountDto.build(originalDiscount.obj)
            };
            formattedDiscounts.push(formattedDiscount)
        }

        res.status(200).json(formattedDiscounts);

    }).then(null, function (error) {
        logger.trace(error);

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));
    });
    /* PROTECTED REGION END */
};


/**
 * Return a discount according to the given id
* @param {string} id -HTTP Type: NAMED- the _id of the discount to be returned
* @returns {DiscountDto} 
 */
module.exports.getDiscountById = function(req, res){
	var id = req.params.id;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Business_DiscountBusiness_getDiscountById_body) ENABLED START */

    var promise = discountRepository.getDiscountById(id);

    promise.then(function(foundDiscount){

        if(typeof foundDiscount != 'undefined' && foundDiscount != null){
            res.status(200).json(DiscountDto.build(foundDiscount));
        }else{
            res.status(404).json(ErrorUtil.notFoundError('Discount does not exist'));
        }

    }).then(null, function(error){
        logger.trace(error);

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter id', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));
    });

	/* PROTECTED REGION END */
};


/**
 * Returns all recommended discounts
* @param {integer} offset -HTTP Type: QUERY- the index where the records start from
* @param {integer} limit -HTTP Type: QUERY- the limit of records to retrieve. Default value is 10. Maximum value is 100
* @param {string} orderBy -HTTP Type: QUERY- the order field
* @param {string} orderType -HTTP Type: QUERY- the order type field
* @returns {DiscountDto} 
 */
module.exports.getRecommendedDiscounts = function(req, res){
	var offset = req.query.offset;
	var limit = req.query.limit;
	var orderBy = req.query.orderBy;
	var orderType = req.query.orderType;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Business_DiscountBusiness_getRecommendedDiscounts_body) ENABLED START */
    if (typeof limit == 'undefined' || limit.length == 0) {
        return res.status(400).json(ErrorUtil.invalidParamError('valid limit param must be provided'));
    }

    var promise = discountRepository.getAllDiscounts(req.query);

    promise.then(function (discounts) {

        logger.debug('Total recommended discounts: ' + discounts.list.length);

        res.header(config.countHeader, discounts.count);
        res.status(200).json(DiscountDto.buildList(discounts.list));

    }).then(null, function (error) {

        logger.trace(error);

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));

    });
	/* PROTECTED REGION END */
};


