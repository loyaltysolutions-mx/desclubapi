//logging config
var logger = console;

// Load configurations according to the selected environment
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];

var extend = require('util')._extend;

var Q = require("q");

var fs = require("fs");

var doT = require('dot');

module.exports.getHeader = function () {

    var deferred = Q.defer();

    fs.readFile(__dirname + '/templates/header.html', 'utf8', function (error, html) {
        if (error) {
            deferred.reject(new Error(error));
        } else {
            deferred.resolve(html);
        }
    });

    return deferred.promise;
};


module.exports.getFooter = function () {

    var deferred = Q.defer();

    fs.readFile(__dirname + '/templates/footer.html', 'utf8', function (error, html) {
        if (error) {
            deferred.reject(new Error(error));
        } else {

            deferred.resolve(html);
        }
    });

    return deferred.promise;
};

module.exports.getDesclubTemplate = function (data) {

    var deferred = Q.defer();

    fs.readFile(__dirname + '/templates/desclub.html', 'utf8', function (error, html) {
        if (error) {
            deferred.reject(new Error(error));
        } else {

            var tempFn = doT.template(html);

            var result = tempFn(data);

            deferred.resolve(result);
        }
    });

    return deferred.promise;

};