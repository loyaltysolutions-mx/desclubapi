//logging config
var logger = console;

// Load configurations according to the selected environment
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];

var extend = require('util')._extend;

var templates = require('./EmailTemplates');

var nodemailer = require('nodemailer');

var Q = require("q");

// create reusable transporter object using SMTP transport
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'xxx@test.com',
        pass: 'xxxx'
    }
});


module.exports.sendEmail = function (to, subject, message) {

    var promiseHeader = templates.getHeader();
    var promiseFooter = templates.getFooter();

    Q.all([promiseHeader, promiseFooter]).then(function (htmls) {

        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: 'Desclub <info@beepquest.com>', // sender address
            to: to, // list of receivers
            subject: subject, // Subject line
            html: htmls[0] + message + htmls[1] // html body
        };

// send mail with defined transport object
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                logger.log(error);
            } else {
                logger.log('Message sent: ' + info.response);
            }
        });

    });
};