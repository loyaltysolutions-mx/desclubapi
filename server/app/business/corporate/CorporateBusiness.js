/**
* This module represents a set of business methods
* @module business/corporate/CorporateBusiness
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Business_CorporateBusiness_init) ENABLED START */

var logger = console;
var repositoryFactory = require('../../repository/RepositoryFactory').getRepositoryFactory();

// Load configurations according to the selected environment
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];


var CorporateDto = require('../../dto/corporate/CorporateDto');
var ErrorUtil = require('../util/ErrorUtil');

var mongoose = require('mongoose');
var SchemaType = mongoose.SchemaType;
var CastError = SchemaType.CastError;

var ValidationError = mongoose.Error.ValidationError;
var ValidatorError = mongoose.Error.ValidatorError;

var extend = require('util')._extend;

var Q = require('q');

var corporateRepository = repositoryFactory.getCorporateRepository();

/* PROTECTED REGION END */


/**
 * Returns all corporate items with pagination. Results can be refined with optional parameters
* @param {integer} offset -HTTP Type: QUERY- the index where the records start from
* @param {integer} limit -HTTP Type: QUERY- the limit of records to retrieve
* @param {string} orderBy -HTTP Type: QUERY- the order field
* @param {string} orderType -HTTP Type: QUERY- the order type field
* @param {string} name -HTTP Type: QUERY- the name of the corporate to be retrieved
* @returns {CorporateDto} 
 */
module.exports.getAllCorporates = function(req, res){
	var limit = req.query.limit;
	var offset = req.query.offset;
	var orderBy = req.query.orderBy;
	var orderType = req.query.orderType;
	var name = req.query.name;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Business_CorporateBusiness_getAllCorporates_body) ENABLED START */
    var promise = corporateRepository.getAllCorporates(req.query);

    promise.then(function (corporates) {
        if (typeof corporates != 'undefined' && corporates != null) {
            res.header(config.countHeader, corporates.count);
            return res.status(200).json(CorporateDto.buildList(corporates.list));
        } else {
            return res.status(404).json(ErrorUtil.notFoundError('Corporates not found'));
        }
    }).then(null, function (error) {

        logger.trace(error);

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter corporate', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));
    });
    /* PROTECTED REGION END */
};


/**
 * Creates a new corporate
* @param {CorporateInputDto} corporate -HTTP Type: BODY- the corporate to be created
* @returns {CorporateDto} 
 */
module.exports.createCorporate = function(req, res){
	var corporate = req.body;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Business_CorporateBusiness_createCorporate_body) ENABLED START */
    var promise = corporateRepository.createCorporate(corporate);

    promise.then(function (corporateCreated) {
        res.status(200).json(CorporateDto.build(corporateCreated));
    }).then(null, function (error) {

        logger.trace(error);

        if (error.code === 11000 || error.code === 11001) {
            return res.status(409).json(ErrorUtil.duplicateError(error.err, error));
        }

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter corporate', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));

    });

    /* PROTECTED REGION END */
};


/**
 * Retrieves a corporate by id
* @param {string} id -HTTP Type: NAMED- the _id of the corporate to retrieve
* @returns {CorporateDto} 
 */
module.exports.getCorporateById = function(req, res){
	var id = req.params.id;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Business_CorporateBusiness_getCorporateById_body) ENABLED START */
    var promise = corporateRepository.getCorporateById(id);

    promise.then(function (corporateRetrieved) {
        res.status(200).json(CorporateDto.build(corporateRetrieved));
    }).then(null, function (error) {

        logger.trace(error);

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter corporate', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));

    });
    /* PROTECTED REGION END */
};


/**
 * Updates a corporate
* @param {string} id -HTTP Type: NAMED- the _id of the corporate to update
* @param {CorporateInputDto} corporate -HTTP Type: BODY- the corporate to update
* @returns {CorporateDto} 
 */
module.exports.updateCorporate = function(req, res){
	var id = req.params.id;
	var corporate = req.body;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Business_CorporateBusiness_updateCorporate_body) ENABLED START */
    var promise = corporateRepository.updateCorporate(id, corporate);

    promise.then(function (corporateUpdated) {
        res.status(200).json(CorporateDto.build(corporateUpdated));
    }).then(null, function (error) {

        logger.trace(error);

        if (error.code === 11000 || error.code === 11001) {
            return res.status(409).json(ErrorUtil.duplicateError(error.err, error));
        }

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter corporate', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));

    });
    /* PROTECTED REGION END */
};


