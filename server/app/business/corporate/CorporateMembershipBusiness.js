/**
* This module represents a set of business methods
* @module business/corporate/CorporateMembershipBusiness
*/
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Business_CorporateMembershipBusiness_init) ENABLED START */

var logger = console;
var repositoryFactory = require('../../repository/RepositoryFactory').getRepositoryFactory();

// Load configurations according to the selected environment
var env = process.env.NODE_ENV || 'development';
var config = require('../../../config/config')[env];

var CorporateMembershipDto = require('../../dto/corporate/CorporateMembershipDto');
var ErrorUtil = require('../util/ErrorUtil');

var EmailAlreadyUsedError = require('../error/EmailAlreadyUsedError');
var CorporateNotFoundError = require('../error/CorporateNotFoundError');
var InvalidMembershipNumberError = require('../error/InvalidMembershipNumberError');
var MembershipNumberAlreadyUsedError = require('../error/MembershipNumberAlreadyUsedError');

var mongoose = require('mongoose');
var SchemaType = mongoose.SchemaType;
var CastError = SchemaType.CastError;

var ValidationError = mongoose.Error.ValidationError;
var ValidatorError = mongoose.Error.ValidatorError;

var extend = require('util')._extend;

var Q = require('q');

var crypto = require('crypto');
var biguint = require('biguint-format');
var moment = require('moment');
var util = require('util');

var sendEmail = require('../util/SendEmail');
var templates = require('../util/EmailTemplates');

var corporateMembershipRepository = repositoryFactory.getCorporateMembershipRepository();
var corporateRepository = repositoryFactory.getCorporateRepository();


var random = function (qty) {
    return crypto.randomBytes(qty);
};

/**
 *
 * @param email
 * @param foundCorporate
 * @param successList
 * @param errorList
 * @returns {{email: *, emailPromise: CorporateMembership}}
 */
var createMembershipForSingleEmail = function (membership, foundCorporate, successList, errorList) {

    var email = membership.email;

    var deferredEmail = Q.defer();

    var deferred = Q.defer();

    var emailPromise;
    //2.1 if the email was not provided, don't check it
    if (typeof email == 'undefined' || email == null || email.length == 0) {
        emailPromise = deferredEmail.promise;
        deferredEmail.resolve(null);
    } else {
        //2.2: find out if email has been already assigned to another membership
        emailPromise = corporateMembershipRepository.getCorporateMembershipByEmail(email);
    }

    emailPromise.then(function (foundMembershipByEmail) {
        if (typeof foundMembershipByEmail != 'undefined' && foundMembershipByEmail != null) {
            throw new EmailAlreadyUsedError('Email: ' + email + ' already has a membership');
        } else {

            //3: build membership document
            var newMembership = extend({}, membership);

            //assign random membership if it wasn't provided
            if (typeof membership.membershipNumber == 'undefined' || membership.membershipNumber == null || membership.membershipNumber.length == 0) {
                //generate random numbers -> 12 digits and append then to the corporate prefix
                var randomMembershipNumber = foundCorporate.membershipPrefix + biguint(random(8), 'dec').substring(0, 12);
                newMembership.membershipNumber = randomMembershipNumber;
            }

            //validate membership
            if (newMembership.membershipNumber.length != 16) {
                throw new InvalidMembershipNumberError('Invalid number: ' + membership.membershipNumber);
            }

            //assign other properties
            if (typeof membership.firstName != 'undefined') {
                newMembership.name = membership.firstName + " " + membership.lastName1 + " " + membership.lastName2;
            }

            newMembership.email = email;
            newMembership.alreadyUsed = false;
            newMembership.corporate = foundCorporate._id;
            newMembership.corporateInfo = foundCorporate;
            newMembership.validThru = moment().add(1, 'years'); //default 1 year of validity

            //create the membership
            return corporateMembershipRepository.createCorporateMembership(newMembership);

        }
    }).then(function (corporateMembershipCreated) {

        logger.debug('Membership created');

        //4: add to success list if everything ok
        successList.push(corporateMembershipCreated);

        deferred.resolve(true);

    }).then(null, function (error) {

        //5: add to error list when error
        errorList.push({
            error: error,
            forMembership: {
                number: membership.membershipNumber,
                email: membership.email,
                firstName: membership.firstName,
                lastName1: membership.lastName1,
                lastName2: membership.lastName2
            }
        });

        deferred.resolve(false);

        logger.trace(error);

    });

    return deferred.promise;
};

/* PROTECTED REGION END */


/**
 * Returns all corporate memberships with pagination. Results can be refined with optional parameters
* @param {integer} offset -HTTP Type: QUERY- the index where the records start from
* @param {integer} limit -HTTP Type: QUERY- the limit of records to retrieve
* @param {string} orderBy -HTTP Type: QUERY- the order field
* @param {string} orderType -HTTP Type: QUERY- the order type field
* @param {string} name -HTTP Type: QUERY- the name of the owner of the membership
* @param {string} corporate -HTTP Type: QUERY- the id of the corporate where the membership belongs to
* @param {string} corporateName -HTTP Type: QUERY- the name of the corporate where the membership belongs to
* @param {string} membershipNumber -HTTP Type: QUERY- the id of the corporate where the membership belongs to
* @returns {CorporateMembershipDto} 
 */
module.exports.getAllCorporateMemberships = function(req, res){
	var limit = req.query.limit;
	var offset = req.query.offset;
	var orderBy = req.query.orderBy;
	var orderType = req.query.orderType;
	var corporate = req.query.corporate;
	var corporateName = req.query.corporateName;
	var membershipNumber = req.query.membershipNumber;
	var name = req.query.name;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Business_CorporateMembershipBusiness_getAllCorporateMemberships_body) ENABLED START */
    var promise = corporateMembershipRepository.getAllCorporateMemberships(req.query);

    promise.then(function (corporateMemberships) {
        if (typeof corporateMemberships != 'undefined' && corporateMemberships != null) {
            res.header(config.countHeader, corporateMemberships.count);
            return res.status(200).json(CorporateMembershipDto.buildList(corporateMemberships.list));
        } else {
            return res.status(404).json(ErrorUtil.notFoundError('Corporate memberships not found'));
        }
    }).then(null, function (error) {

        logger.trace(error);

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter corporate memberships', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));
    });
    /* PROTECTED REGION END */
};


/**
 * Creates a single corporate membership
* @param {CorporateMembershipInputDto} membership -HTTP Type: BODY- the corporate membership to be created
* @returns {CorporateMembershipDto} 
 */
module.exports.createSingleCorporateMembership = function(req, res){
	var membership = req.body;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Business_CorporateMembershipBusiness_createSingleCorporateMembership_body) ENABLED START */

    //1: find if corporate exists
    var findCorporateByIdPromise = corporateRepository.getCorporateById(membership.corporate);
    findCorporateByIdPromise.then(function (foundCorporate) {

        if (typeof foundCorporate != 'undefined' && foundCorporate != null) {
            var successList = [];
            var errorList = [];

            var singleCreationPromise = createMembershipForSingleEmail(membership, foundCorporate, successList, errorList);

            singleCreationPromise.then(function (responses) {

                if (successList.length > 0) {
                    res.status(200).json(CorporateMembershipDto.build(successList[0]));
                } else {
                    res.status(409).json(ErrorUtil.duplicateError('E-mail already registered', errorList[0]));
                }
            });

        } else {
            throw new CorporateNotFoundError('Corporate ' + membership.corporate + ' not found');
        }

    }).then(null, function (error) {

        logger.trace(error);

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter corporate', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));

    });

    /* PROTECTED REGION END */
};


/**
 * Creates one or multiple corporate memberships
* @param {string} corporate -HTTP Type: NAMED- the corporate _id where the memberships are being created
* @param {CorporateMembershipInputDto} memberships -HTTP Type: BODY- the array of corporate memberships to be created. additionalData, membershipNumber and email are optional. If membershipNumber is not present, a random one is generated
* @returns {CorporateMembershipDto} 
 */
module.exports.createCorporateMembership = function(req, res){
	var corporate = req.params.corporate;
	var memberships = req.body;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Business_CorporateMembershipBusiness_createCorporateMembership_body) ENABLED START */

    //0 perform validations before starting
    if (typeof corporate == 'undefined' || corporate == null || corporate.length == 0) {
        return res.status(400).json(ErrorUtil.validationError('Invalid corporate'));
    }

    if (typeof memberships == 'undefined' || !util.isArray(memberships)) {
        return res.status(400).json(ErrorUtil.validationError('Body must be an array of memberships'));
    }

    //1: find if corporate exists
    var findCorporateByIdPromise = corporateRepository.getCorporateById(corporate);
    findCorporateByIdPromise.then(function (foundCorporate) {

        if (typeof foundCorporate != 'undefined' && foundCorporate != null) {

            var successList = [];
            var errorList = [];
            var promisesList = [];

            //iterate over memberships
            for (var i = 0; i < memberships.length; i++) {
                var membership = memberships[i];
                var singleCreationPromise = createMembershipForSingleEmail(membership, foundCorporate, successList, errorList);
                promisesList.push(singleCreationPromise);
            }

            Q.all(promisesList).then(function (responses) {
                logger.debug(responses);
                res.status(200).json({success: successList, error: errorList});

            });

        } else {
            throw new CorporateNotFoundError('Corporate ' + corporate + ' not found');
        }

    }).then(null, function (error) {

        logger.trace(error);

        if (error instanceof  CorporateNotFoundError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Corporate not found', error));
        }

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter corporate', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));

    });
    /* PROTECTED REGION END */
};


/**
 * Retrieves a corporate membership by number
* @param {string} number -HTTP Type: NAMED- the number of the corporate membership to retrieve
* @returns {CorporateMembershipDto} 
 */
module.exports.getCorporateMembershipByNumber = function(req, res){
	var number = req.params.number;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Business_CorporateMembershipBusiness_getCorporateMembershipByNumber_body) ENABLED START */
    var promise = corporateMembershipRepository.getCorporateMembershipByNumber(number);

    promise.then(function (corporateRetrieved) {
        if (typeof corporateRetrieved != 'undefined' && corporateRetrieved != null) {
            res.status(200).json(CorporateMembershipDto.build(corporateRetrieved));
        } else {
            return res.status(404).json(ErrorUtil.notFoundError('Membership with number: ' + number + ' not found'));
        }
    }).then(null, function (error) {

        logger.trace(error);

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter number', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));

    });
    /* PROTECTED REGION END */
};


/**
 * updates the corporate membership email by number
* @param {string} number -HTTP Type: NAMED- the number of the corporate membership to retrieve
* @param {string} email -HTTP Type: BODY- the email to update
* @returns {CorporateMembershipDto} 
 */
module.exports.updateCorporateMembershipByNumber = function(req, res){
	var number = req.params.number;
	var email = req.body;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Business_CorporateMembershipBusiness_updateCorporateMembershipByNumber_body) ENABLED START */
    var promise = corporateMembershipRepository.getCorporateMembershipByNumber(number);

    promise.then(function (corporateRetrieved) {
        if (typeof corporateRetrieved != 'undefined' && corporateRetrieved != null) {
            res.status(200).json(CorporateMembershipDto.build(corporateRetrieved));
        } else {
            return res.status(404).json(ErrorUtil.notFoundError('Membership with number: ' + number + ' not found'));
        }

        corporateRetrieved.email = email.email;
        corporateRetrieved.save();

    }).then(null, function (error) {

        logger.trace(error);

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter number', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));

    });
	/* PROTECTED REGION END */
};


/**
 * Updates a corporate membership status
* @param {string} id -HTTP Type: NAMED- the _id of the corporate membership to update
* @param {CorporateMembershipAlreadyUsedDto} alreadyUsed -HTTP Type: BODY- the corporate membership alreadyUsed field to update. Must be true if the membership should be marked as used.
* @returns {CorporateMembershipDto} 
 */
module.exports.updateCorporateMembershipStatus = function(req, res){
	var id = req.params.id;
	var alreadyUsed = req.body;

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Business_CorporateMembershipBusiness_updateCorporateMembershipStatus_body) ENABLED START */
    //validate status
    if (typeof alreadyUsed == 'undefined' || alreadyUsed == null || typeof alreadyUsed.alreadyUsed == 'undefined' || alreadyUsed.alreadyUsed == null) {
        return res.status(400).json(ErrorUtil.validationError('Invalid alreadyUsed entity ' + alreadyUsed));
    }


    var promise = corporateMembershipRepository.updateCorporateMembershipStatus(id, alreadyUsed.alreadyUsed);

    promise.then(function (corporateMembershipUpdated) {
        res.status(200).json(CorporateMembershipDto.build(corporateMembershipUpdated));
    }).then(null, function (error) {

        logger.trace(error);

        if (error.code === 11000 || error.code === 11001) {
            return res.status(409).json(ErrorUtil.duplicateError(error.err, error));
        }

        if (error instanceof  CastError) {
            return res.status(404).json(ErrorUtil.invalidParamError('Invalid parameter corporate', error));
        }

        if (error instanceof  ValidationError || error instanceof ValidatorError) {
            return res.status(400).json(ErrorUtil.validationError('Validation failed', error));
        }

        res.status(500).json(ErrorUtil.unknownError(error));

    });
    /* PROTECTED REGION END */
};


