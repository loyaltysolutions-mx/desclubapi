
/**
 * The module representing the discount routes module
 * @module route/discount
 */

var discountBusiness = require("../business/discount/DiscountBusiness");

/**
 * Following are the routes for discount
 * @param {Express} app the app element from express
 */
module.exports = function (app, passport) {

	
	/**
	 * Routes for the context discounts with basePath "/v1/desclubAPI/discounts".
	 * Description: Operations about discounts
	 */
	

	app.get('/v1/desclubAPI/discounts', discountBusiness.getAllDiscounts);

	
	/**
	 * Routes for the context discounts with basePath "/v1/desclubAPI/discounts/{id}".
	 * Description: Operations about discounts
	 */
	

	app.get('/v1/desclubAPI/discounts/:id', discountBusiness.getDiscountById);

	
	/**
	 * Routes for the context discounts with basePath "/v1/desclubAPI/recommendedDiscounts".
	 * Description: Operations about discounts
	 */
	

	app.get('/v1/desclubAPI/recommendedDiscounts', discountBusiness.getRecommendedDiscounts);

	
	/**
	 * Routes for the context discounts with basePath "/v1/desclubAPI/nearByDiscounts".
	 * Description: Operations about discounts
	 */
	

	app.get('/v1/desclubAPI/nearByDiscounts', discountBusiness.getAllNearByDiscounts);

};

