
/**
 * The module representing the importer routes module
 * @module route/importer
 */

var importerBusiness = require("../business/importer/ImporterBusiness");

/**
 * Following are the routes for importer
 * @param {Express} app the app element from express
 */
module.exports = function (app, passport) {

	
	/**
	 * Routes for the context importer with basePath "/v1/desclubAPI/importer".
	 * Description: Operations about importer
	 */
	

	app.post('/v1/desclubAPI/importer', importerBusiness.doImport);

};

