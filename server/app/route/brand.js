
/**
 * The module representing the brand routes module
 * @module route/brand
 */

var brandBusiness = require("../business/brand/BrandBusiness");
var branchBusiness = require("../business/brand/BranchBusiness");

/**
 * Following are the routes for brand
 * @param {Express} app the app element from express
 */
module.exports = function (app, passport) {

	
	/**
	 * Routes for the context brands with basePath "/v1/desclubAPI/brands".
	 * Description: Operations about brands
	 */
	

	app.get('/v1/desclubAPI/brands', brandBusiness.getAllBrands);

	
	/**
	 * Routes for the context branches with basePath "/v1/desclubAPI/brands/{brand}/branches".
	 * Description: Operations about branches
	 */
	

	app.get('/v1/desclubAPI/brands/:brand/branches', branchBusiness.getAllBranchesByBrand);

};

