
/**
 * The module representing the corporate routes module
 * @module route/corporate
 */

var corporateBusiness = require("../business/corporate/CorporateBusiness");
var corporateMembershipBusiness = require("../business/corporate/CorporateMembershipBusiness");

/**
 * Following are the routes for corporate
 * @param {Express} app the app element from express
 */
module.exports = function (app, passport) {

	
	/**
	 * Routes for the context corporates with basePath "/v1/desclubAPI/corporates".
	 * Description: Operations about corporates
	 */
	

	app.get('/v1/desclubAPI/corporates', corporateBusiness.getAllCorporates);


	app.post('/v1/desclubAPI/corporates', corporateBusiness.createCorporate);

	
	/**
	 * Routes for the context corporates with basePath "/v1/desclubAPI/corporates/{id}".
	 * Description: Operations about corporates
	 */
	

	app.get('/v1/desclubAPI/corporates/:id', corporateBusiness.getCorporateById);


	app.put('/v1/desclubAPI/corporates/:id', corporateBusiness.updateCorporate);

	
	/**
	 * Routes for the context corporateMemberships with basePath "/v1/desclubAPI/corporateMemberships".
	 * Description: Operations about corporate memberships
	 */
	

	app.get('/v1/desclubAPI/corporateMemberships', corporateMembershipBusiness.getAllCorporateMemberships);


	app.post('/v1/desclubAPI/corporateMembership', corporateMembershipBusiness.createSingleCorporateMembership);

	
	/**
	 * Routes for the context corporateMemberships with basePath "/v1/desclubAPI/corporateMemberships/{id}".
	 * Description: Operations about corporate memberships
	 */
	

	app.put('/v1/desclubAPI/corporateMemberships/:id', corporateMembershipBusiness.updateCorporateMembershipStatus);

	
	/**
	 * Routes for the context corporateMemberships with basePath "/v1/desclubAPI/corporateMembershipsByNumber/{number}".
	 * Description: Operations about corporate memberships
	 */
	

	app.get('/v1/desclubAPI/corporateMembershipsByNumber/:number', corporateMembershipBusiness.getCorporateMembershipByNumber);


	app.put('/v1/desclubAPI/corporateMembershipsByNumber/:number', corporateMembershipBusiness.updateCorporateMembershipByNumber);

	
	/**
	 * Routes for the context corporateMemberships with basePath "/v1/desclubAPI/corporate/{corporate}/corporateMemberships".
	 * Description: Operations about corporate memberships
	 */
	

	app.post('/v1/desclubAPI/corporate/:corporate/corporateMemberships', corporateMembershipBusiness.createCorporateMembership);

};

