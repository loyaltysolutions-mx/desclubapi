
/**
 * The module representing the state routes module
 * @module route/state
 */

var stateBusiness = require("../business/state/StateBusiness");

/**
 * Following are the routes for state
 * @param {Express} app the app element from express
 */
module.exports = function (app, passport) {

	
	/**
	 * Routes for the context states with basePath "/v1/desclubAPI/states".
	 * Description: Operations about states
	 */
	

	app.get('/v1/desclubAPI/states', stateBusiness.getAllStates);

};

