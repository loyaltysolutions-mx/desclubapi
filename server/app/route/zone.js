
/**
 * The module representing the zone routes module
 * @module route/zone
 */

var zoneBusiness = require("../business/zone/ZoneBusiness");

/**
 * Following are the routes for zone
 * @param {Express} app the app element from express
 */
module.exports = function (app, passport) {

	
	/**
	 * Routes for the context zones with basePath "/v1/desclubAPI/zones".
	 * Description: Operations about zones
	 */
	

	app.get('/v1/desclubAPI/zones', zoneBusiness.getAllZones);

};

