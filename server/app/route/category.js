
/**
 * The module representing the category routes module
 * @module route/category
 */

var categoryBusiness = require("../business/category/CategoryBusiness");
var subcategoryBusiness = require("../business/category/SubcategoryBusiness");

/**
 * Following are the routes for category
 * @param {Express} app the app element from express
 */
module.exports = function (app, passport) {

	
	/**
	 * Routes for the context categories with basePath "/v1/desclubAPI/categories".
	 * Description: Operations about categories
	 */
	

	app.get('/v1/desclubAPI/categories', categoryBusiness.getAllCategories);


	app.post('/v1/desclubAPI/categories', categoryBusiness.createCategory);

	
	/**
	 * Routes for the context subcategories with basePath "/v1/desclubAPI/categories/{category}/subcategories".
	 * Description: Operations about sub-categories
	 */
	

	app.get('/v1/desclubAPI/categories/:category/subcategories', subcategoryBusiness.getAllSubcategories);


	app.post('/v1/desclubAPI/categories/:category/subcategories', subcategoryBusiness.createSubcategory);

};

