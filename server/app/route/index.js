	var brand = require("./brand");
	var category = require("./category");
	var importer = require("./importer");
	var discount = require("./discount");
	var zone = require("./zone");
	var state = require("./state");
	var corporate = require("./corporate");
	
	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub__route_RouteIndex_init) ENABLED START */
	
	var logger = console;
	var env = process.env.NODE_ENV || 'development';
	var config = require('../../config/config')[env];
	
	/* PROTECTED REGION END */
	
	var apiDocBusiness = require('../business/api-doc/APIDocBusiness');
	
	
	/**
	 * Main function to bootstrap all routes of this app
	 * @param app the express app
	 * @param passport the passport object for auth
	 */
	module.exports = function (app, passport) {
		
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub__route_RouteIndex_commonRoute) ENABLED START */
        app.all('*', function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, "+config.tokenHeader+", "+config.businessEventHeader+", "+config.userHeader+", "+config.countHeader);
            res.header("Access-Control-Expose-Headers", "X-Requested-With, Content-Type, "+config.tokenHeader+", "+config.businessEventHeader+", "+config.userHeader+", "+config.countHeader);

            if (req.method === 'OPTIONS') {
                res.send (204);
            }else{
                next();
            }

        });
		/* PROTECTED REGION END */

		brand(app, passport);
		category(app, passport);
		importer(app, passport);
		discount(app, passport);
		zone(app, passport);
		state(app, passport);
		corporate(app, passport);


		//routes for the api-doc
		app.get('/api-docs', apiDocBusiness.apiDoc);
		

		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub__route_RouteIndex_additionalRoutes) ENABLED START */
		/* PROTECTED REGION END */
	}
