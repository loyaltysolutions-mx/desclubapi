/**
 * The document module with the mongoose Discount schema
 * @module model/discount/Discount
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Document_Discount_init) ENABLED START */
var logger = console;

var mongoose = require('mongoose'),
Schema = mongoose.Schema;


/* PROTECTED REGION END */





	/**
	 * DiscountLocation - SubDoc
	 */
	var DiscountLocationSchema = {
	
		type : {
				type: String
		, default: 'Point'
		, required: true
		},
		coordinates : {
				type: Array
		}
	};

/**
 * Discount Schema
 */
var DiscountSchema = new Schema({

	originalDiscount : {
		type: Schema.Types.Mixed, required: true
	},
	branch : {
		type: Schema.ObjectId, ref: 'Branch', required: true
	},
	branchInfo : {
		type: Schema.Types.Mixed, required: true
	},
	brand : {
		type: Schema.ObjectId, ref: 'Brand', required: true
	},
	brandInfo : {
		type: Schema.Types.Mixed, required: true
	},
	subcategory : {
		type: Schema.ObjectId, ref: 'Subcategory', required: true
	},
	subcategoryInfo : {
		type: Schema.Types.Mixed, required: true
	},
	category : {
		type: Schema.ObjectId, ref: 'Category', required: true
	},
	categoryInfo : {
		type: Schema.Types.Mixed, required: true
	},
	cash : {
		type: String
	},
	card : {
		type: String
	},
	promo : {
		type: String
	},
	restriction : {
		type: String
	},
	location : DiscountLocationSchema,
	updated : {
		type: Boolean
	},
	created : {
		type: Date
	, default: Date.now
	}
});


module.exports.model = function(){
    mongoose.model('Discount', DiscountSchema);
};

module.exports.schema = DiscountSchema;

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_Document_Discount_additionalMethods) ENABLED START */
DiscountSchema.index({ location: '2dsphere' });
/* PROTECTED REGION END */
