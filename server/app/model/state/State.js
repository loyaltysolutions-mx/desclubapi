/**
 * The document module with the mongoose State schema
 * @module model/state/State
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_Document_State_init) ENABLED START */
var logger = console;

var mongoose = require('mongoose'),
Schema = mongoose.Schema;


/* PROTECTED REGION END */


/**
 * State Schema
 */
var StateSchema = new Schema({

	stateId : {
		type: Number, required: true, index: true
	},
	name : {
		type: String, required: true, index: true
	}
});


module.exports.model = function(){
    mongoose.model('State', StateSchema);
};

module.exports.schema = StateSchema;

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_Document_State_additionalMethods) ENABLED START */
/* PROTECTED REGION END */
