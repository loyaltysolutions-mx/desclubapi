/**
 * The document module with the mongoose CorporateMembership schema
 * @module model/corporate/CorporateMembership
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Document_CorporateMembership_init) ENABLED START */
var logger = console;

var mongoose = require('mongoose'),
Schema = mongoose.Schema;


/* PROTECTED REGION END */





	/**
	 * CorporateMembershipAdditionalData - SubDoc
	 */
	var CorporateMembershipAdditionalDataSchema = {
	
		bancomer : {
				type: Schema.Types.Mixed
		}
	};

/**
 * CorporateMembership Schema
 */
var CorporateMembershipSchema = new Schema({

	name : {
		type: String, required: true
	},
	firstName : {
		type: String
	},
	lastName1 : {
		type: String
	},
	lastName2 : {
		type: String
	},
	email : {
		type: String, index: true
	},
	membershipNumber : {
		type: String, unique: true, required: true, index: true
	},
	alreadyUsed : {
		type: Boolean, required: true, index: true
	},
	corporate : {
		type: Schema.ObjectId, ref: 'Corporate', required: true
	},
	corporateInfo : {
		type: Schema.Types.Mixed, required: true
	},
	additionalData : CorporateMembershipAdditionalDataSchema,
	validThru : {
		type: Date
	},
	created : {
		type: Date
	, default: Date.now
	}
});


module.exports.model = function(){
    mongoose.model('CorporateMembership', CorporateMembershipSchema);
};

module.exports.schema = CorporateMembershipSchema;

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Document_CorporateMembership_additionalMethods) ENABLED START */
/* PROTECTED REGION END */
