/**
 * The document module with the mongoose Corporate schema
 * @module model/corporate/Corporate
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Document_Corporate_init) ENABLED START */
var logger = console;

var mongoose = require('mongoose'),
Schema = mongoose.Schema;


/* PROTECTED REGION END */


/**
 * Corporate Schema
 */
var CorporateSchema = new Schema({

	name : {
		type: String, required: true
	},
	membershipPrefix : {
		type: String, unique: true, required: true
	},
	created : {
		type: Date
	, default: Date.now
	}
});


module.exports.model = function(){
    mongoose.model('Corporate', CorporateSchema);
};

module.exports.schema = CorporateSchema;

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_Document_Corporate_additionalMethods) ENABLED START */
/* PROTECTED REGION END */
