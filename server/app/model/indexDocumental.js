
/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_DocumentalIndexModel__init) ENABLED START */
var mongoose = require('mongoose');
var logger = console;

var env = process.env.NODE_ENV || 'development';
var config = require('../../config/config')[env];

/* PROTECTED REGION END */

//setup mongoDB connection

var connectionString = 'mongodb://'+config.mongo.host+':'+config.mongo.port+'/'+config.mongo.name+'';

if(config.mongo.user.length > 0 && config.mongo.password.length > 0){
	connectionString = 'mongodb://'+config.mongo.user+':'+config.mongo.password+'@'+config.mongo.host+':'+config.mongo.port+'/'+config.mongo.name+'';
}

mongoose.connect(connectionString, function (err, res) {
    if (err) throw err;
    logger.debug('Successful connection to MongoDB');
});

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_DocumentalIndexModel__mid) ENABLED START */
mongoose.set('debug', false);
/* PROTECTED REGION END */

require('./brand/Brand').model();
require('./brand/Branch').model();
require('./category/Category').model();
require('./category/Subcategory').model();
require('./discount/Discount').model();
require('./zone/Zone').model();
require('./state/State').model();
require('./corporate/Corporate').model();
require('./corporate/CorporateMembership').model();

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_DocumentalIndexModel__end) ENABLED START */
/* PROTECTED REGION END */
