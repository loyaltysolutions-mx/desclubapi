/**
 * The document module with the mongoose Zone schema
 * @module model/zone/Zone
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_Document_Zone_init) ENABLED START */
var logger = console;

var mongoose = require('mongoose'),
Schema = mongoose.Schema;


/* PROTECTED REGION END */


/**
 * Zone Schema
 */
var ZoneSchema = new Schema({

	zoneId : {
		type: Number, required: true, index: true
	},
	stateId : {
		type: Number, required: true
	},
	name : {
		type: String, required: true, index: true
	}
});


module.exports.model = function(){
    mongoose.model('Zone', ZoneSchema);
};

module.exports.schema = ZoneSchema;

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_Document_Zone_additionalMethods) ENABLED START */
/* PROTECTED REGION END */
