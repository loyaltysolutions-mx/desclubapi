/**
 * The document module with the mongoose Subcategory schema
 * @module model/category/Subcategory
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Document_Subcategory_init) ENABLED START */
var logger = console;

var mongoose = require('mongoose'),
Schema = mongoose.Schema;


/* PROTECTED REGION END */


/**
 * Subcategory Schema
 */
var SubcategorySchema = new Schema({

	subcategoryId : {
		type: Number, required: true
	},
	name : {
		type: String
	},
	category : {
		type: Schema.ObjectId, ref: 'Category', required: true
	}
});


module.exports.model = function(){
    mongoose.model('Subcategory', SubcategorySchema);
};

module.exports.schema = SubcategorySchema;

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Document_Subcategory_additionalMethods) ENABLED START */
/* PROTECTED REGION END */
