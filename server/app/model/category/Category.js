/**
 * The document module with the mongoose Category schema
 * @module model/category/Category
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Document_Category_init) ENABLED START */
var logger = console;

var mongoose = require('mongoose'),
Schema = mongoose.Schema;


/* PROTECTED REGION END */


/**
 * Category Schema
 */
var CategorySchema = new Schema({

	categoryId : {
		type: Number, required: true
	},
	originalName : {
		type: String, required: true
	},
	name : {
		type: String, required: true
	}
});


module.exports.model = function(){
    mongoose.model('Category', CategorySchema);
};

module.exports.schema = CategorySchema;

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_Document_Category_additionalMethods) ENABLED START */
/* PROTECTED REGION END */
