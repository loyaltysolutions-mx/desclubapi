/**
 * The document module with the mongoose Branch schema
 * @module model/brand/Branch
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Document_Branch_init) ENABLED START */
var logger = console;

var mongoose = require('mongoose'),
Schema = mongoose.Schema;


/* PROTECTED REGION END */





	/**
	 * BranchLocation - SubDoc
	 */
	var BranchLocationSchema = {
	
		type : {
				type: String
		, default: 'Point'
		, required: true
		},
		coordinates : {
				type: Array
		}
	};

/**
 * Branch Schema
 */
var BranchSchema = new Schema({

	originalBranch : {
		type: Schema.Types.Mixed, required: true
	},
	branchId : {
		type: Number, unique: true, required: true
	},
	brand : {
		type: Schema.ObjectId, ref: 'Brand'
	},
	name : {
		type: String, required: true, index: true
	},
	street : {
		type: String
	},
	extNum : {
		type: String
	},
	intNum : {
		type: String
	},
	colony : {
		type: String
	},
	zipCode : {
		type: String
	},
	city : {
		type: String
	},
	state : {
		type: Schema.ObjectId, ref: 'State'
	},
	zone : {
		type: Schema.ObjectId, ref: 'Zone'
	},
	phone : {
		type: String
	},
	mainDiscount : {
		type: Schema.ObjectId, ref: 'Discount'
	},
	location : BranchLocationSchema,
	updated : {
		type: Boolean, required: true
	},
	created : {
		type: Date
	, default: Date.now
	}
});


module.exports.model = function(){
    mongoose.model('Branch', BranchSchema);
};

module.exports.schema = BranchSchema;

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Document_Branch_additionalMethods) ENABLED START */
BranchSchema.index({ location: '2dsphere' });

/* PROTECTED REGION END */
