/**
 * The document module with the mongoose Brand schema
 * @module model/brand/Brand
 */

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Document_Brand_init) ENABLED START */
var logger = console;

var mongoose = require('mongoose'),
Schema = mongoose.Schema;


/* PROTECTED REGION END */


/**
 * Brand Schema
 */
var BrandSchema = new Schema({

	originalBrand : {
		type: Schema.Types.Mixed, required: true
	},
	brandId : {
		type: Number, required: true, index: true
	},
	name : {
		type: String, required: true, index: true
	},
	logoSmall : {
		type: String, required: true
	},
	logoBig : {
		type: String, required: true
	},
	validity_start : {
		type: Date, required: true
	},
	validity_end : {
		type: Date, required: true
	},
	url : {
		type: String
	},
	mainDiscount : {
		type: Schema.ObjectId, ref: 'Discount'
	},
	updated : {
		type: Boolean, required: true
	},
	created : {
		type: Date
	, default: Date.now
	}
});


module.exports.model = function(){
    mongoose.model('Brand', BrandSchema);
};

module.exports.schema = BrandSchema;

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_Document_Brand_additionalMethods) ENABLED START */
/* PROTECTED REGION END */
