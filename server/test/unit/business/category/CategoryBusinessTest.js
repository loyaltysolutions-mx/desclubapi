/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_BusinessTest_CategoryBusiness_init) ENABLED START */

var logger = console;
var rewire = require("rewire");
var Promise = require('mpromise');

var chai = require("chai");
chai.should();

/* PROTECTED REGION END */
/**
 * Unit tests for the business module CategoryBusiness
 */
 
describe('CategoryBusiness tests', function () {


	/**
	 * Unit test cases for the operation getAllCategories
	 */
	 
	describe('getAllCategories function', function () {
		
		/**
		 * Test case for the normal flow of the operation getAllCategories
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_BusinessTest_CategoryBusiness_getAllCategories_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var categoryBusiness = rewire("../../../../app/business/category/CategoryBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            categoryBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            categoryBusiness.getAllCategories(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation createCategory
	 */
	 
	describe('createCategory function', function () {
		
		/**
		 * Test case for the normal flow of the operation createCategory
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_BusinessTest_CategoryBusiness_createCategory_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var categoryBusiness = rewire("../../../../app/business/category/CategoryBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            categoryBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            categoryBusiness.createCategory(request, response);
		});
		/* PROTECTED REGION END */
});

});
