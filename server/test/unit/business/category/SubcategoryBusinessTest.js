/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_BusinessTest_SubcategoryBusiness_init) ENABLED START */

var logger = console;
var rewire = require("rewire");
var Promise = require('mpromise');

var chai = require("chai");
chai.should();

/* PROTECTED REGION END */
/**
 * Unit tests for the business module SubcategoryBusiness
 */
 
describe('SubcategoryBusiness tests', function () {


	/**
	 * Unit test cases for the operation getAllSubcategories
	 */
	 
	describe('getAllSubcategories function', function () {
		
		/**
		 * Test case for the normal flow of the operation getAllSubcategories
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_BusinessTest_SubcategoryBusiness_getAllSubcategories_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var subcategoryBusiness = rewire("../../../../app/business/category/SubcategoryBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            subcategoryBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            subcategoryBusiness.getAllSubcategories(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation createSubcategory
	 */
	 
	describe('createSubcategory function', function () {
		
		/**
		 * Test case for the normal flow of the operation createSubcategory
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_BusinessTest_SubcategoryBusiness_createSubcategory_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var subcategoryBusiness = rewire("../../../../app/business/category/SubcategoryBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            subcategoryBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            subcategoryBusiness.createSubcategory(request, response);
		});
		/* PROTECTED REGION END */
});

});
