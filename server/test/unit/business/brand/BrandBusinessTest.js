/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_BusinessTest_BrandBusiness_init) ENABLED START */

var logger = console;
var rewire = require("rewire");
var Promise = require('mpromise');

var chai = require("chai");
chai.should();

/* PROTECTED REGION END */
/**
 * Unit tests for the business module BrandBusiness
 */
 
describe('BrandBusiness tests', function () {


	/**
	 * Unit test cases for the operation getAllBrands
	 */
	 
	describe('getAllBrands function', function () {
		
		/**
		 * Test case for the normal flow of the operation getAllBrands
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_BusinessTest_BrandBusiness_getAllBrands_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var brandBusiness = rewire("../../../../app/business/brand/BrandBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            brandBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            brandBusiness.getAllBrands(request, response);
		});
		/* PROTECTED REGION END */
});

});
