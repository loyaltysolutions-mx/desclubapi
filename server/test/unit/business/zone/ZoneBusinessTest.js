/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_BusinessTest_ZoneBusiness_init) ENABLED START */

var logger = console;
var rewire = require("rewire");
var Promise = require('mpromise');

var chai = require("chai");
chai.should();

/* PROTECTED REGION END */
/**
 * Unit tests for the business module ZoneBusiness
 */
 
describe('ZoneBusiness tests', function () {


	/**
	 * Unit test cases for the operation getAllZones
	 */
	 
	describe('getAllZones function', function () {
		
		/**
		 * Test case for the normal flow of the operation getAllZones
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_BusinessTest_ZoneBusiness_getAllZones_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var zoneBusiness = rewire("../../../../app/business/zone/ZoneBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            zoneBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            zoneBusiness.getAllZones(request, response);
		});
		/* PROTECTED REGION END */
});

});
