/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateMembership_init) ENABLED START */

var logger = console;
var rewire = require("rewire");
var Promise = require('mpromise');

var chai = require("chai");
chai.should();

/* PROTECTED REGION END */
/**
 * Unit tests for the business module CorporateMembership
 */
 
describe('CorporateMembership tests', function () {


	/**
	 * Unit test cases for the operation getAllCorporateMemberships
	 */
	 
	describe('getAllCorporateMemberships function', function () {
		
		/**
		 * Test case for the normal flow of the operation getAllCorporateMemberships
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateMembership_getAllCorporateMemberships_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateMembership = rewire("../../../../app/business/corporate/CorporateMembership");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateMembership.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateMembership.getAllCorporateMemberships(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation createCorporateMembership
	 */
	 
	describe('createCorporateMembership function', function () {
		
		/**
		 * Test case for the normal flow of the operation createCorporateMembership
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateMembership_createCorporateMembership_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateMembership = rewire("../../../../app/business/corporate/CorporateMembership");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateMembership.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateMembership.createCorporateMembership(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation getCorporateMembershipByNumber
	 */
	 
	describe('getCorporateMembershipByNumber function', function () {
		
		/**
		 * Test case for the normal flow of the operation getCorporateMembershipByNumber
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateMembership_getCorporateMembershipByNumber_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateMembership = rewire("../../../../app/business/corporate/CorporateMembership");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateMembership.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateMembership.getCorporateMembershipByNumber(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation updateCorporateMembershipStatus
	 */
	 
	describe('updateCorporateMembershipStatus function', function () {
		
		/**
		 * Test case for the normal flow of the operation updateCorporateMembershipStatus
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateMembership_updateCorporateMembershipStatus_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateMembership = rewire("../../../../app/business/corporate/CorporateMembership");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateMembership.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateMembership.updateCorporateMembershipStatus(request, response);
		});
		/* PROTECTED REGION END */
});

});
