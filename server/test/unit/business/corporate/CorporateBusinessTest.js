/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateBusiness_init) ENABLED START */

var logger = console;
var rewire = require("rewire");
var Promise = require('mpromise');

var chai = require("chai");
chai.should();

/* PROTECTED REGION END */
/**
 * Unit tests for the business module CorporateBusiness
 */
 
describe('CorporateBusiness tests', function () {


	/**
	 * Unit test cases for the operation getAllCorporates
	 */
	 
	describe('getAllCorporates function', function () {
		
		/**
		 * Test case for the normal flow of the operation getAllCorporates
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateBusiness_getAllCorporates_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateBusiness = rewire("../../../../app/business/corporate/CorporateBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateBusiness.getAllCorporates(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation createCorporate
	 */
	 
	describe('createCorporate function', function () {
		
		/**
		 * Test case for the normal flow of the operation createCorporate
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateBusiness_createCorporate_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateBusiness = rewire("../../../../app/business/corporate/CorporateBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateBusiness.createCorporate(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation getCorporateById
	 */
	 
	describe('getCorporateById function', function () {
		
		/**
		 * Test case for the normal flow of the operation getCorporateById
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateBusiness_getCorporateById_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateBusiness = rewire("../../../../app/business/corporate/CorporateBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateBusiness.getCorporateById(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation updateCorporate
	 */
	 
	describe('updateCorporate function', function () {
		
		/**
		 * Test case for the normal flow of the operation updateCorporate
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateBusiness_updateCorporate_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateBusiness = rewire("../../../../app/business/corporate/CorporateBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateBusiness.updateCorporate(request, response);
		});
		/* PROTECTED REGION END */
});

});
