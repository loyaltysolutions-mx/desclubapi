/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateMembershipBusiness_init) ENABLED START */

var logger = console;
var rewire = require("rewire");
var Promise = require('mpromise');

var chai = require("chai");
chai.should();

/* PROTECTED REGION END */
/**
 * Unit tests for the business module CorporateMembershipBusiness
 */
 
describe('CorporateMembershipBusiness tests', function () {


	/**
	 * Unit test cases for the operation getAllCorporateMemberships
	 */
	 
	describe('getAllCorporateMemberships function', function () {
		
		/**
		 * Test case for the normal flow of the operation getAllCorporateMemberships
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateMembershipBusiness_getAllCorporateMemberships_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateMembershipBusiness = rewire("../../../../app/business/corporate/CorporateMembershipBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateMembershipBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateMembershipBusiness.getAllCorporateMemberships(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation createSingleCorporateMembership
	 */
	 
	describe('createSingleCorporateMembership function', function () {
		
		/**
		 * Test case for the normal flow of the operation createSingleCorporateMembership
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateMembershipBusiness_createSingleCorporateMembership_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateMembershipBusiness = rewire("../../../../app/business/corporate/CorporateMembershipBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateMembershipBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateMembershipBusiness.createSingleCorporateMembership(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation createCorporateMembership
	 */
	 
	describe('createCorporateMembership function', function () {
		
		/**
		 * Test case for the normal flow of the operation createCorporateMembership
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateMembershipBusiness_createCorporateMembership_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateMembershipBusiness = rewire("../../../../app/business/corporate/CorporateMembershipBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateMembershipBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateMembershipBusiness.createCorporateMembership(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation getCorporateMembershipByNumber
	 */
	 
	describe('getCorporateMembershipByNumber function', function () {
		
		/**
		 * Test case for the normal flow of the operation getCorporateMembershipByNumber
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateMembershipBusiness_getCorporateMembershipByNumber_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateMembershipBusiness = rewire("../../../../app/business/corporate/CorporateMembershipBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateMembershipBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateMembershipBusiness.getCorporateMembershipByNumber(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation updateCorporateMembershipByNumber
	 */
	 
	describe('updateCorporateMembershipByNumber function', function () {
		
		/**
		 * Test case for the normal flow of the operation updateCorporateMembershipByNumber
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateMembershipBusiness_updateCorporateMembershipByNumber_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateMembershipBusiness = rewire("../../../../app/business/corporate/CorporateMembershipBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateMembershipBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateMembershipBusiness.updateCorporateMembershipByNumber(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation updateCorporateMembershipStatus
	 */
	 
	describe('updateCorporateMembershipStatus function', function () {
		
		/**
		 * Test case for the normal flow of the operation updateCorporateMembershipStatus
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_BusinessTest_CorporateMembershipBusiness_updateCorporateMembershipStatus_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var corporateMembershipBusiness = rewire("../../../../app/business/corporate/CorporateMembershipBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            corporateMembershipBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            corporateMembershipBusiness.updateCorporateMembershipStatus(request, response);
		});
		/* PROTECTED REGION END */
});

});
