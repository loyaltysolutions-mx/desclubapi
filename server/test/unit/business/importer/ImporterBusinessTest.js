/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_importer_BusinessTest_ImporterBusiness_init) ENABLED START */

var logger = console;
var rewire = require("rewire");
var Promise = require('mpromise');

var chai = require("chai");
chai.should();

/* PROTECTED REGION END */
/**
 * Unit tests for the business module ImporterBusiness
 */
 
describe('ImporterBusiness tests', function () {


	/**
	 * Unit test cases for the operation doImport
	 */
	 
	describe('doImport function', function () {
		
		/**
		 * Test case for the normal flow of the operation doImport
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_importer_BusinessTest_ImporterBusiness_doImport_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var importerBusiness = rewire("../../../../app/business/importer/ImporterBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            importerBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            importerBusiness.doImport(request, response);
		});
		/* PROTECTED REGION END */
});

});
