/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_BusinessTest_DiscountBusiness_init) ENABLED START */

var logger = console;
var rewire = require("rewire");
var Promise = require('mpromise');

var chai = require("chai");
chai.should();

/* PROTECTED REGION END */
/**
 * Unit tests for the business module DiscountBusiness
 */
 
describe('DiscountBusiness tests', function () {


	/**
	 * Unit test cases for the operation getAllDiscounts
	 */
	 
	describe('getAllDiscounts function', function () {
		
		/**
		 * Test case for the normal flow of the operation getAllDiscounts
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_BusinessTest_DiscountBusiness_getAllDiscounts_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var discountBusiness = rewire("../../../../app/business/discount/DiscountBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            discountBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            discountBusiness.getAllDiscounts(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation getAllNearByDiscounts
	 */
	 
	describe('getAllNearByDiscounts function', function () {
		
		/**
		 * Test case for the normal flow of the operation getAllNearByDiscounts
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_BusinessTest_DiscountBusiness_getAllNearByDiscounts_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var discountBusiness = rewire("../../../../app/business/discount/DiscountBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            discountBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            discountBusiness.getAllNearByDiscounts(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation getDiscountById
	 */
	 
	describe('getDiscountById function', function () {
		
		/**
		 * Test case for the normal flow of the operation getDiscountById
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_BusinessTest_DiscountBusiness_getDiscountById_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var discountBusiness = rewire("../../../../app/business/discount/DiscountBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            discountBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            discountBusiness.getDiscountById(request, response);
		});
		/* PROTECTED REGION END */
});

	/**
	 * Unit test cases for the operation getRecommendedDiscounts
	 */
	 
	describe('getRecommendedDiscounts function', function () {
		
		/**
		 * Test case for the normal flow of the operation getRecommendedDiscounts
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_BusinessTest_DiscountBusiness_getRecommendedDiscounts_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var discountBusiness = rewire("../../../../app/business/discount/DiscountBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            discountBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            discountBusiness.getRecommendedDiscounts(request, response);
		});
		/* PROTECTED REGION END */
});

});
