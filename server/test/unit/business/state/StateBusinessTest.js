/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_BusinessTest_StateBusiness_init) ENABLED START */

var logger = console;
var rewire = require("rewire");
var Promise = require('mpromise');

var chai = require("chai");
chai.should();

/* PROTECTED REGION END */
/**
 * Unit tests for the business module StateBusiness
 */
 
describe('StateBusiness tests', function () {


	/**
	 * Unit test cases for the operation getAllStates
	 */
	 
	describe('getAllStates function', function () {
		
		/**
		 * Test case for the normal flow of the operation getAllStates
		 */
		/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_BusinessTest_StateBusiness_getAllStates_bodyTest) ENABLED START */
		it('should be OK', function(done){
			//import dependencies with rewire. Ready to be mocked
			var stateBusiness = rewire("../../../../app/business/state/StateBusiness");

			 //set expected values
			 var expectedResult = {
			 	foo: 1
			 };

			 var expectedStatus = 200;

            //mock the request
            var request = {

            };

            //mock the response
            var response = {
                status: function (status) {
                    status.should.be.equal(expectedStatus);
                    return this;
                },
                json: function (obj) {
                    obj.should.be.equal(expectedResult);
                    done();
                }
            };
            
            //mock persistence methods
            stateBusiness.__set__('repositoryFactory', {

            });
            
            //call the method we want to test
            stateBusiness.getAllStates(request, response);
		});
		/* PROTECTED REGION END */
});

});
