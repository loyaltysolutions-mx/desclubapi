/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentDataTestDiscountTest_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

module.exports.generate = function (id) {
	var DiscountLocation = {
		type: 'type' + id,
		coordinates: 'coordinates' + id
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentDataTestDiscountTest_custom_subdocs) ENABLED START */
	/* PROTECTED REGION END */

    var Discount = {
    	originalDiscount: 'originalDiscount' + id,
    	branch: {},
    	branchInfo: 'branchInfo' + id,
    	brand: {},
    	brandInfo: 'brandInfo' + id,
    	subcategory: {},
    	subcategoryInfo: 'subcategoryInfo' + id,
    	category: {},
    	categoryInfo: 'categoryInfo' + id,
    	cash: 'cash' + id,
    	card: 'card' + id,
    	promo: 'promo' + id,
    	restriction: 'restriction' + id,
    	location: DiscountLocation,
    	updated: true,
    	created: new Date()
    };

    /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentDataTestDiscountTest_custom) ENABLED START */
	/* PROTECTED REGION END */

    return Discount;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentDataTestDiscountTest_additional) ENABLED START */
/* PROTECTED REGION END */
