/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepositoryTest_DiscountRepositoryTest_init) ENABLED START */
var logger = console;
var mongoose = require('mongoose');
var Q = require("q");
var env = process.env.NODE_ENV || 'test';
var config = require('../../../../config/config')[env];

var chai = require("chai");
chai.should();
chai.use(require("chai-as-promised"));

var repositoryFactory = require("../../../../app/repository/RepositoryFactory").getRepositoryFactory();
var discountRepository = null;

var discountRepositoryData = require('./data/DiscountRepositoryData');


var initialize = function(){

	require('../../../../app/model/discount/Discount').model();

	discountRepository = repositoryFactory.getDiscountRepository();
};

/* PROTECTED REGION END */

/**
 * Unit tests for the repository DiscountRepository
 */
 
describe('DiscountRepository tests', function(){

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepositoryTest_DiscountRepositoryTest_config) ENABLED START */
	before(function(done){
		mongoose.connect('mongodb://'+config.mongo.host+':'+config.mongo.port+'/'+config.mongo.name+'', function (err, res) {
			if (err) throw err;

            initialize();

            done();
        });
	});
	
	after(function(done){
        mongoose.connection.db.dropDatabase(function(){
            mongoose.connection.close(done);
        });
    });
	/* PROTECTED REGION END */
	

		/**
		 * Unit test cases for the operation getAllDiscounts
		 */
		describe('getAllDiscounts function', function () {
			
			/**
			 * Test case for the normal flow of the operation getAllDiscounts
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepositoryTest_DiscountRepositoryTest__Operation_getAllDiscounts_body) ENABLED START */
			it('should be fulfilled', function(){

				var optional = null;

				var promise = discountRepository.getAllDiscounts(optional);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getAllDiscounts
			 */
			it('should be rejected', function(){
				
				var optional = null;
				
				var promise = discountRepository.getAllDiscounts(optional);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getAllNearByDiscounts
		 */
		describe('getAllNearByDiscounts function', function () {
			
			/**
			 * Test case for the normal flow of the operation getAllNearByDiscounts
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepositoryTest_DiscountRepositoryTest__Operation_getAllNearByDiscounts_body) ENABLED START */
			it('should be fulfilled', function(){

				var optional = null;

				var promise = discountRepository.getAllNearByDiscounts(optional);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getAllNearByDiscounts
			 */
			it('should be rejected', function(){
				
				var optional = null;
				
				var promise = discountRepository.getAllNearByDiscounts(optional);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation createDiscount
		 */
		describe('createDiscount function', function () {
			
			/**
			 * Test case for the normal flow of the operation createDiscount
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepositoryTest_DiscountRepositoryTest__Operation_createDiscount_body) ENABLED START */
			it('should be fulfilled', function(){

				var discount = null;

				var promise = discountRepository.createDiscount(discount);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation createDiscount
			 */
			it('should be rejected', function(){
				
				var discount = null;
				
				var promise = discountRepository.createDiscount(discount);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation updateDiscount
		 */
		describe('updateDiscount function', function () {
			
			/**
			 * Test case for the normal flow of the operation updateDiscount
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepositoryTest_DiscountRepositoryTest__Operation_updateDiscount_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;
				var discount = null;

				var promise = discountRepository.updateDiscount(id, discount);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation updateDiscount
			 */
			it('should be rejected', function(){
				
				var id = null;
				var discount = null;
				
				var promise = discountRepository.updateDiscount(id, discount);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getDiscountById
		 */
		describe('getDiscountById function', function () {
			
			/**
			 * Test case for the normal flow of the operation getDiscountById
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepositoryTest_DiscountRepositoryTest__Operation_getDiscountById_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;

				var promise = discountRepository.getDiscountById(id);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getDiscountById
			 */
			it('should be rejected', function(){
				
				var id = null;
				
				var promise = discountRepository.getDiscountById(id);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getDiscountByBranch
		 */
		describe('getDiscountByBranch function', function () {
			
			/**
			 * Test case for the normal flow of the operation getDiscountByBranch
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepositoryTest_DiscountRepositoryTest__Operation_getDiscountByBranch_body) ENABLED START */
			it('should be fulfilled', function(){

				var branch = null;

				var promise = discountRepository.getDiscountByBranch(branch);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getDiscountByBranch
			 */
			it('should be rejected', function(){
				
				var branch = null;
				
				var promise = discountRepository.getDiscountByBranch(branch);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation markAllUpdated
		 */
		describe('markAllUpdated function', function () {
			
			/**
			 * Test case for the normal flow of the operation markAllUpdated
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepositoryTest_DiscountRepositoryTest__Operation_markAllUpdated_body) ENABLED START */
			it('should be fulfilled', function(){

				var updated = null;

				var promise = discountRepository.markAllUpdated(updated);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation markAllUpdated
			 */
			it('should be rejected', function(){
				
				var updated = null;
				
				var promise = discountRepository.markAllUpdated(updated);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation removeAllWithUpdated
		 */
		describe('removeAllWithUpdated function', function () {
			
			/**
			 * Test case for the normal flow of the operation removeAllWithUpdated
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepositoryTest_DiscountRepositoryTest__Operation_removeAllWithUpdated_body) ENABLED START */
			it('should be fulfilled', function(){

				var updated = null;

				var promise = discountRepository.removeAllWithUpdated(updated);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation removeAllWithUpdated
			 */
			it('should be rejected', function(){
				
				var updated = null;
				
				var promise = discountRepository.removeAllWithUpdated(updated);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_discount_DocumentalRepositoryTest_DiscountRepositoryTest_additional) ENABLED START */
	/* PROTECTED REGION END */
});
