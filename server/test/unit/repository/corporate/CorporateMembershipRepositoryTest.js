/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateMembershipRepositoryTest_init) ENABLED START */
var logger = console;
var mongoose = require('mongoose');
var Q = require("q");
var env = process.env.NODE_ENV || 'test';
var config = require('../../../../config/config')[env];

var chai = require("chai");
chai.should();
chai.use(require("chai-as-promised"));

var repositoryFactory = require("../../../../app/repository/RepositoryFactory").getRepositoryFactory();
var corporateMembershipRepository = null;

var corporateMembershipRepositoryData = require('./data/CorporateMembershipRepositoryData');


var initialize = function(){

	require('../../../../app/model/corporate/CorporateMembership').model();

	corporateMembershipRepository = repositoryFactory.getCorporateMembershipRepository();
};

/* PROTECTED REGION END */

/**
 * Unit tests for the repository CorporateMembershipRepository
 */
 
describe('CorporateMembershipRepository tests', function(){

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateMembershipRepositoryTest_config) ENABLED START */
	before(function(done){
		mongoose.connect('mongodb://'+config.mongo.host+':'+config.mongo.port+'/'+config.mongo.name+'', function (err, res) {
			if (err) throw err;

            initialize();

            done();
        });
	});
	
	after(function(done){
        mongoose.connection.db.dropDatabase(function(){
            mongoose.connection.close(done);
        });
    });
	/* PROTECTED REGION END */
	

		/**
		 * Unit test cases for the operation getAllCorporateMemberships
		 */
		describe('getAllCorporateMemberships function', function () {
			
			/**
			 * Test case for the normal flow of the operation getAllCorporateMemberships
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateMembershipRepositoryTest__Operation_getAllCorporateMemberships_body) ENABLED START */
			it('should be fulfilled', function(){

				var optional = null;

				var promise = corporateMembershipRepository.getAllCorporateMemberships(optional);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getAllCorporateMemberships
			 */
			it('should be rejected', function(){
				
				var optional = null;
				
				var promise = corporateMembershipRepository.getAllCorporateMemberships(optional);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation createCorporateMembership
		 */
		describe('createCorporateMembership function', function () {
			
			/**
			 * Test case for the normal flow of the operation createCorporateMembership
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateMembershipRepositoryTest__Operation_createCorporateMembership_body) ENABLED START */
			it('should be fulfilled', function(){

				var membership = null;

				var promise = corporateMembershipRepository.createCorporateMembership(membership);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation createCorporateMembership
			 */
			it('should be rejected', function(){
				
				var membership = null;
				
				var promise = corporateMembershipRepository.createCorporateMembership(membership);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation updateCorporateMembershipStatus
		 */
		describe('updateCorporateMembershipStatus function', function () {
			
			/**
			 * Test case for the normal flow of the operation updateCorporateMembershipStatus
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateMembershipRepositoryTest__Operation_updateCorporateMembershipStatus_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;
				var status = null;

				var promise = corporateMembershipRepository.updateCorporateMembershipStatus(id, status);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation updateCorporateMembershipStatus
			 */
			it('should be rejected', function(){
				
				var id = null;
				var status = null;
				
				var promise = corporateMembershipRepository.updateCorporateMembershipStatus(id, status);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getCorporateMembershipById
		 */
		describe('getCorporateMembershipById function', function () {
			
			/**
			 * Test case for the normal flow of the operation getCorporateMembershipById
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateMembershipRepositoryTest__Operation_getCorporateMembershipById_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;

				var promise = corporateMembershipRepository.getCorporateMembershipById(id);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getCorporateMembershipById
			 */
			it('should be rejected', function(){
				
				var id = null;
				
				var promise = corporateMembershipRepository.getCorporateMembershipById(id);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getCorporateMembershipByEmail
		 */
		describe('getCorporateMembershipByEmail function', function () {
			
			/**
			 * Test case for the normal flow of the operation getCorporateMembershipByEmail
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateMembershipRepositoryTest__Operation_getCorporateMembershipByEmail_body) ENABLED START */
			it('should be fulfilled', function(){

				var email = null;

				var promise = corporateMembershipRepository.getCorporateMembershipByEmail(email);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getCorporateMembershipByEmail
			 */
			it('should be rejected', function(){
				
				var email = null;
				
				var promise = corporateMembershipRepository.getCorporateMembershipByEmail(email);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getCorporateMembershipByNumber
		 */
		describe('getCorporateMembershipByNumber function', function () {
			
			/**
			 * Test case for the normal flow of the operation getCorporateMembershipByNumber
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateMembershipRepositoryTest__Operation_getCorporateMembershipByNumber_body) ENABLED START */
			it('should be fulfilled', function(){

				var membershipNumber = null;

				var promise = corporateMembershipRepository.getCorporateMembershipByNumber(membershipNumber);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getCorporateMembershipByNumber
			 */
			it('should be rejected', function(){
				
				var membershipNumber = null;
				
				var promise = corporateMembershipRepository.getCorporateMembershipByNumber(membershipNumber);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateMembershipRepositoryTest_additional) ENABLED START */
	/* PROTECTED REGION END */
});
