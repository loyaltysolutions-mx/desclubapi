/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentDataTestCorporateMembershipTest_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

module.exports.generate = function (id) {
	var CorporateMembershipAdditionalData = {
		bancomer: 'bancomer' + id
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentDataTestCorporateMembershipTest_custom_subdocs) ENABLED START */
	/* PROTECTED REGION END */

    var CorporateMembership = {
    	name: 'name' + id,
    	firstName: 'firstName' + id,
    	lastName1: 'lastName1' + id,
    	lastName2: 'lastName2' + id,
    	email: 'email' + id,
    	membershipNumber: 'membershipNumber' + id,
    	alreadyUsed: true,
    	corporate: {},
    	corporateInfo: 'corporateInfo' + id,
    	additionalData: CorporateMembershipAdditionalData,
    	validThru: new Date(),
    	created: new Date()
    };

    /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentDataTestCorporateMembershipTest_custom) ENABLED START */
	/* PROTECTED REGION END */

    return CorporateMembership;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentDataTestCorporateMembershipTest_additional) ENABLED START */
/* PROTECTED REGION END */
