/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentDataTestCorporateTest_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

module.exports.generate = function (id) {

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentDataTestCorporateTest_custom_subdocs) ENABLED START */
	/* PROTECTED REGION END */

    var Corporate = {
    	name: 'name' + id,
    	membershipPrefix: 'membershipPrefix' + id,
    	created: new Date()
    };

    /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentDataTestCorporateTest_custom) ENABLED START */
	/* PROTECTED REGION END */

    return Corporate;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentDataTestCorporateTest_additional) ENABLED START */
/* PROTECTED REGION END */
