/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateRepositoryTest_init) ENABLED START */
var logger = console;
var mongoose = require('mongoose');
var Q = require("q");
var env = process.env.NODE_ENV || 'test';
var config = require('../../../../config/config')[env];

var chai = require("chai");
chai.should();
chai.use(require("chai-as-promised"));

var repositoryFactory = require("../../../../app/repository/RepositoryFactory").getRepositoryFactory();
var corporateRepository = null;

var corporateRepositoryData = require('./data/CorporateRepositoryData');


var initialize = function(){

	require('../../../../app/model/corporate/Corporate').model();

	corporateRepository = repositoryFactory.getCorporateRepository();
};

/* PROTECTED REGION END */

/**
 * Unit tests for the repository CorporateRepository
 */
 
describe('CorporateRepository tests', function(){

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateRepositoryTest_config) ENABLED START */
	before(function(done){
		mongoose.connect('mongodb://'+config.mongo.host+':'+config.mongo.port+'/'+config.mongo.name+'', function (err, res) {
			if (err) throw err;

            initialize();

            done();
        });
	});
	
	after(function(done){
        mongoose.connection.db.dropDatabase(function(){
            mongoose.connection.close(done);
        });
    });
	/* PROTECTED REGION END */
	

		/**
		 * Unit test cases for the operation getAllCorporates
		 */
		describe('getAllCorporates function', function () {
			
			/**
			 * Test case for the normal flow of the operation getAllCorporates
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateRepositoryTest__Operation_getAllCorporates_body) ENABLED START */
			it('should be fulfilled', function(){

				var optional = null;

				var promise = corporateRepository.getAllCorporates(optional);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getAllCorporates
			 */
			it('should be rejected', function(){
				
				var optional = null;
				
				var promise = corporateRepository.getAllCorporates(optional);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation createCorporate
		 */
		describe('createCorporate function', function () {
			
			/**
			 * Test case for the normal flow of the operation createCorporate
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateRepositoryTest__Operation_createCorporate_body) ENABLED START */
			it('should be fulfilled', function(){

				var corporate = null;

				var promise = corporateRepository.createCorporate(corporate);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation createCorporate
			 */
			it('should be rejected', function(){
				
				var corporate = null;
				
				var promise = corporateRepository.createCorporate(corporate);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation updateCorporate
		 */
		describe('updateCorporate function', function () {
			
			/**
			 * Test case for the normal flow of the operation updateCorporate
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateRepositoryTest__Operation_updateCorporate_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;
				var corporate = null;

				var promise = corporateRepository.updateCorporate(id, corporate);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation updateCorporate
			 */
			it('should be rejected', function(){
				
				var id = null;
				var corporate = null;
				
				var promise = corporateRepository.updateCorporate(id, corporate);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getCorporateById
		 */
		describe('getCorporateById function', function () {
			
			/**
			 * Test case for the normal flow of the operation getCorporateById
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateRepositoryTest__Operation_getCorporateById_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;

				var promise = corporateRepository.getCorporateById(id);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getCorporateById
			 */
			it('should be rejected', function(){
				
				var id = null;
				
				var promise = corporateRepository.getCorporateById(id);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_corporate_DocumentalRepositoryTest_CorporateRepositoryTest_additional) ENABLED START */
	/* PROTECTED REGION END */
});
