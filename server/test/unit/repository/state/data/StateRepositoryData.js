/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentDataTestStateTest_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

module.exports.generate = function (id) {

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentDataTestStateTest_custom_subdocs) ENABLED START */
	/* PROTECTED REGION END */

    var State = {
    	stateId: id,
    	name: 'name' + id
    };

    /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentDataTestStateTest_custom) ENABLED START */
	/* PROTECTED REGION END */

    return State;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentDataTestStateTest_additional) ENABLED START */
/* PROTECTED REGION END */
