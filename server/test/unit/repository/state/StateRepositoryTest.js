/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentalRepositoryTest_StateRepositoryTest_init) ENABLED START */
var logger = console;
var mongoose = require('mongoose');
var Q = require("q");
var env = process.env.NODE_ENV || 'test';
var config = require('../../../../config/config')[env];

var chai = require("chai");
chai.should();
chai.use(require("chai-as-promised"));

var repositoryFactory = require("../../../../app/repository/RepositoryFactory").getRepositoryFactory();
var stateRepository = null;

var stateRepositoryData = require('./data/StateRepositoryData');


var initialize = function(){

	require('../../../../app/model/state/State').model();

	stateRepository = repositoryFactory.getStateRepository();
};

/* PROTECTED REGION END */

/**
 * Unit tests for the repository StateRepository
 */
 
describe('StateRepository tests', function(){

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentalRepositoryTest_StateRepositoryTest_config) ENABLED START */
	before(function(done){
		mongoose.connect('mongodb://'+config.mongo.host+':'+config.mongo.port+'/'+config.mongo.name+'', function (err, res) {
			if (err) throw err;

            initialize();

            done();
        });
	});
	
	after(function(done){
        mongoose.connection.db.dropDatabase(function(){
            mongoose.connection.close(done);
        });
    });
	/* PROTECTED REGION END */
	

		/**
		 * Unit test cases for the operation getAllStates
		 */
		describe('getAllStates function', function () {
			
			/**
			 * Test case for the normal flow of the operation getAllStates
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentalRepositoryTest_StateRepositoryTest__Operation_getAllStates_body) ENABLED START */
			it('should be fulfilled', function(){

				var optional = null;

				var promise = stateRepository.getAllStates(optional);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getAllStates
			 */
			it('should be rejected', function(){
				
				var optional = null;
				
				var promise = stateRepository.getAllStates(optional);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getStateById
		 */
		describe('getStateById function', function () {
			
			/**
			 * Test case for the normal flow of the operation getStateById
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentalRepositoryTest_StateRepositoryTest__Operation_getStateById_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;

				var promise = stateRepository.getStateById(id);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getStateById
			 */
			it('should be rejected', function(){
				
				var id = null;
				
				var promise = stateRepository.getStateById(id);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getStateByStateId
		 */
		describe('getStateByStateId function', function () {
			
			/**
			 * Test case for the normal flow of the operation getStateByStateId
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentalRepositoryTest_StateRepositoryTest__Operation_getStateByStateId_body) ENABLED START */
			it('should be fulfilled', function(){

				var stateId = null;

				var promise = stateRepository.getStateByStateId(stateId);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getStateByStateId
			 */
			it('should be rejected', function(){
				
				var stateId = null;
				
				var promise = stateRepository.getStateByStateId(stateId);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_state_DocumentalRepositoryTest_StateRepositoryTest_additional) ENABLED START */
	/* PROTECTED REGION END */
});
