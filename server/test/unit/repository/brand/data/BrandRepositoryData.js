/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentDataTestBrandTest_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

module.exports.generate = function (id) {

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentDataTestBrandTest_custom_subdocs) ENABLED START */
	/* PROTECTED REGION END */

    var Brand = {
    	originalBrand: 'originalBrand' + id,
    	brandId: id,
    	name: 'name' + id,
    	logoSmall: 'logoSmall' + id,
    	logoBig: 'logoBig' + id,
    	validity_start: new Date(),
    	validity_end: new Date(),
    	url: 'url' + id,
    	mainDiscount: {},
    	updated: true,
    	created: new Date()
    };

    /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentDataTestBrandTest_custom) ENABLED START */
	/* PROTECTED REGION END */

    return Brand;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentDataTestBrandTest_additional) ENABLED START */
/* PROTECTED REGION END */
