/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentDataTestBranchTest_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

module.exports.generate = function (id) {
	var BranchLocation = {
		type: 'type' + id,
		coordinates: 'coordinates' + id
	};

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentDataTestBranchTest_custom_subdocs) ENABLED START */
	/* PROTECTED REGION END */

    var Branch = {
    	originalBranch: 'originalBranch' + id,
    	branchId: id,
    	brand: {},
    	name: 'name' + id,
    	street: 'street' + id,
    	extNum: 'extNum' + id,
    	intNum: 'intNum' + id,
    	colony: 'colony' + id,
    	zipCode: 'zipCode' + id,
    	city: 'city' + id,
    	state: {},
    	zone: {},
    	phone: 'phone' + id,
    	mainDiscount: {},
    	location: BranchLocation,
    	updated: true,
    	created: new Date()
    };

    /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentDataTestBranchTest_custom) ENABLED START */
	/* PROTECTED REGION END */

    return Branch;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentDataTestBranchTest_additional) ENABLED START */
/* PROTECTED REGION END */
