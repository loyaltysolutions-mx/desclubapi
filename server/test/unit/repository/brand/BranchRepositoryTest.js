/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BranchRepositoryTest_init) ENABLED START */
var logger = console;
var mongoose = require('mongoose');
var Q = require("q");
var env = process.env.NODE_ENV || 'test';
var config = require('../../../../config/config')[env];

var chai = require("chai");
chai.should();
chai.use(require("chai-as-promised"));

var repositoryFactory = require("../../../../app/repository/RepositoryFactory").getRepositoryFactory();
var branchRepository = null;

var branchRepositoryData = require('./data/BranchRepositoryData');


var initialize = function(){

	require('../../../../app/model/brand/Branch').model();

	branchRepository = repositoryFactory.getBranchRepository();
};

/* PROTECTED REGION END */

/**
 * Unit tests for the repository BranchRepository
 */
 
describe('BranchRepository tests', function(){

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BranchRepositoryTest_config) ENABLED START */
	before(function(done){
		mongoose.connect('mongodb://'+config.mongo.host+':'+config.mongo.port+'/'+config.mongo.name+'', function (err, res) {
			if (err) throw err;

            initialize();

            done();
        });
	});
	
	after(function(done){
        mongoose.connection.db.dropDatabase(function(){
            mongoose.connection.close(done);
        });
    });
	/* PROTECTED REGION END */
	

		/**
		 * Unit test cases for the operation getAllBranches
		 */
		describe('getAllBranches function', function () {
			
			/**
			 * Test case for the normal flow of the operation getAllBranches
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BranchRepositoryTest__Operation_getAllBranches_body) ENABLED START */
			it('should be fulfilled', function(){

				var brand = null;
				var optional = null;

				var promise = branchRepository.getAllBranches(brand, optional);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getAllBranches
			 */
			it('should be rejected', function(){
				
				var brand = null;
				var optional = null;
				
				var promise = branchRepository.getAllBranches(brand, optional);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation createBranch
		 */
		describe('createBranch function', function () {
			
			/**
			 * Test case for the normal flow of the operation createBranch
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BranchRepositoryTest__Operation_createBranch_body) ENABLED START */
			it('should be fulfilled', function(){

				var brand = null;
				var branch = null;

				var promise = branchRepository.createBranch(brand, branch);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation createBranch
			 */
			it('should be rejected', function(){
				
				var brand = null;
				var branch = null;
				
				var promise = branchRepository.createBranch(brand, branch);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation updateBranch
		 */
		describe('updateBranch function', function () {
			
			/**
			 * Test case for the normal flow of the operation updateBranch
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BranchRepositoryTest__Operation_updateBranch_body) ENABLED START */
			it('should be fulfilled', function(){

				var brand = null;
				var id = null;
				var branch = null;

				var promise = branchRepository.updateBranch(brand, id, branch);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation updateBranch
			 */
			it('should be rejected', function(){
				
				var brand = null;
				var id = null;
				var branch = null;
				
				var promise = branchRepository.updateBranch(brand, id, branch);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getBranchById
		 */
		describe('getBranchById function', function () {
			
			/**
			 * Test case for the normal flow of the operation getBranchById
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BranchRepositoryTest__Operation_getBranchById_body) ENABLED START */
			it('should be fulfilled', function(){

				var brand = null;
				var id = null;

				var promise = branchRepository.getBranchById(brand, id);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getBranchById
			 */
			it('should be rejected', function(){
				
				var brand = null;
				var id = null;
				
				var promise = branchRepository.getBranchById(brand, id);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getBranchByDesclubId
		 */
		describe('getBranchByDesclubId function', function () {
			
			/**
			 * Test case for the normal flow of the operation getBranchByDesclubId
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BranchRepositoryTest__Operation_getBranchByDesclubId_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;

				var promise = branchRepository.getBranchByDesclubId(id);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getBranchByDesclubId
			 */
			it('should be rejected', function(){
				
				var id = null;
				
				var promise = branchRepository.getBranchByDesclubId(id);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BranchRepositoryTest_additional) ENABLED START */
	/* PROTECTED REGION END */
});
