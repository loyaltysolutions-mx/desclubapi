/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BrandRepositoryTest_init) ENABLED START */
var logger = console;
var mongoose = require('mongoose');
var Q = require("q");
var env = process.env.NODE_ENV || 'test';
var config = require('../../../../config/config')[env];

var chai = require("chai");
chai.should();
chai.use(require("chai-as-promised"));

var repositoryFactory = require("../../../../app/repository/RepositoryFactory").getRepositoryFactory();
var brandRepository = null;

var brandRepositoryData = require('./data/BrandRepositoryData');


var initialize = function(){

	require('../../../../app/model/brand/Brand').model();

	brandRepository = repositoryFactory.getBrandRepository();
};

/* PROTECTED REGION END */

/**
 * Unit tests for the repository BrandRepository
 */
 
describe('BrandRepository tests', function(){

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BrandRepositoryTest_config) ENABLED START */
	before(function(done){
		mongoose.connect('mongodb://'+config.mongo.host+':'+config.mongo.port+'/'+config.mongo.name+'', function (err, res) {
			if (err) throw err;

            initialize();

            done();
        });
	});
	
	after(function(done){
        mongoose.connection.db.dropDatabase(function(){
            mongoose.connection.close(done);
        });
    });
	/* PROTECTED REGION END */
	

		/**
		 * Unit test cases for the operation getAllBrands
		 */
		describe('getAllBrands function', function () {
			
			/**
			 * Test case for the normal flow of the operation getAllBrands
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BrandRepositoryTest__Operation_getAllBrands_body) ENABLED START */
			it('should be fulfilled', function(){

				var optional = null;

				var promise = brandRepository.getAllBrands(optional);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getAllBrands
			 */
			it('should be rejected', function(){
				
				var optional = null;
				
				var promise = brandRepository.getAllBrands(optional);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation createBrand
		 */
		describe('createBrand function', function () {
			
			/**
			 * Test case for the normal flow of the operation createBrand
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BrandRepositoryTest__Operation_createBrand_body) ENABLED START */
			it('should be fulfilled', function(){

				var brand = null;

				var promise = brandRepository.createBrand(brand);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation createBrand
			 */
			it('should be rejected', function(){
				
				var brand = null;
				
				var promise = brandRepository.createBrand(brand);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation updateBrand
		 */
		describe('updateBrand function', function () {
			
			/**
			 * Test case for the normal flow of the operation updateBrand
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BrandRepositoryTest__Operation_updateBrand_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;
				var brand = null;

				var promise = brandRepository.updateBrand(id, brand);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation updateBrand
			 */
			it('should be rejected', function(){
				
				var id = null;
				var brand = null;
				
				var promise = brandRepository.updateBrand(id, brand);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getBrandById
		 */
		describe('getBrandById function', function () {
			
			/**
			 * Test case for the normal flow of the operation getBrandById
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BrandRepositoryTest__Operation_getBrandById_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;

				var promise = brandRepository.getBrandById(id);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getBrandById
			 */
			it('should be rejected', function(){
				
				var id = null;
				
				var promise = brandRepository.getBrandById(id);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getBrandByDesclubId
		 */
		describe('getBrandByDesclubId function', function () {
			
			/**
			 * Test case for the normal flow of the operation getBrandByDesclubId
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BrandRepositoryTest__Operation_getBrandByDesclubId_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;

				var promise = brandRepository.getBrandByDesclubId(id);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getBrandByDesclubId
			 */
			it('should be rejected', function(){
				
				var id = null;
				
				var promise = brandRepository.getBrandByDesclubId(id);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_brand_DocumentalRepositoryTest_BrandRepositoryTest_additional) ENABLED START */
	/* PROTECTED REGION END */
});
