/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentalRepositoryTest_ZoneRepositoryTest_init) ENABLED START */
var logger = console;
var mongoose = require('mongoose');
var Q = require("q");
var env = process.env.NODE_ENV || 'test';
var config = require('../../../../config/config')[env];

var chai = require("chai");
chai.should();
chai.use(require("chai-as-promised"));

var repositoryFactory = require("../../../../app/repository/RepositoryFactory").getRepositoryFactory();
var zoneRepository = null;

var zoneRepositoryData = require('./data/ZoneRepositoryData');


var initialize = function(){

	require('../../../../app/model/zone/Zone').model();

	zoneRepository = repositoryFactory.getZoneRepository();
};

/* PROTECTED REGION END */

/**
 * Unit tests for the repository ZoneRepository
 */
 
describe('ZoneRepository tests', function(){

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentalRepositoryTest_ZoneRepositoryTest_config) ENABLED START */
	before(function(done){
		mongoose.connect('mongodb://'+config.mongo.host+':'+config.mongo.port+'/'+config.mongo.name+'', function (err, res) {
			if (err) throw err;

            initialize();

            done();
        });
	});
	
	after(function(done){
        mongoose.connection.db.dropDatabase(function(){
            mongoose.connection.close(done);
        });
    });
	/* PROTECTED REGION END */
	

		/**
		 * Unit test cases for the operation getAllZones
		 */
		describe('getAllZones function', function () {
			
			/**
			 * Test case for the normal flow of the operation getAllZones
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentalRepositoryTest_ZoneRepositoryTest__Operation_getAllZones_body) ENABLED START */
			it('should be fulfilled', function(){

				var optional = null;

				var promise = zoneRepository.getAllZones(optional);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getAllZones
			 */
			it('should be rejected', function(){
				
				var optional = null;
				
				var promise = zoneRepository.getAllZones(optional);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getZoneById
		 */
		describe('getZoneById function', function () {
			
			/**
			 * Test case for the normal flow of the operation getZoneById
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentalRepositoryTest_ZoneRepositoryTest__Operation_getZoneById_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;

				var promise = zoneRepository.getZoneById(id);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getZoneById
			 */
			it('should be rejected', function(){
				
				var id = null;
				
				var promise = zoneRepository.getZoneById(id);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getZoneByZoneId
		 */
		describe('getZoneByZoneId function', function () {
			
			/**
			 * Test case for the normal flow of the operation getZoneByZoneId
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentalRepositoryTest_ZoneRepositoryTest__Operation_getZoneByZoneId_body) ENABLED START */
			it('should be fulfilled', function(){

				var zoneId = null;

				var promise = zoneRepository.getZoneByZoneId(zoneId);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getZoneByZoneId
			 */
			it('should be rejected', function(){
				
				var zoneId = null;
				
				var promise = zoneRepository.getZoneByZoneId(zoneId);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentalRepositoryTest_ZoneRepositoryTest_additional) ENABLED START */
	/* PROTECTED REGION END */
});
