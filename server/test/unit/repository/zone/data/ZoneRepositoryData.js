/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentDataTestZoneTest_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

module.exports.generate = function (id) {

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentDataTestZoneTest_custom_subdocs) ENABLED START */
	/* PROTECTED REGION END */

    var Zone = {
    	zoneId: id,
    	stateId: id,
    	name: 'name' + id
    };

    /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentDataTestZoneTest_custom) ENABLED START */
	/* PROTECTED REGION END */

    return Zone;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_zone_DocumentDataTestZoneTest_additional) ENABLED START */
/* PROTECTED REGION END */
