/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepositoryTest_SubcategoryRepositoryTest_init) ENABLED START */
var logger = console;
var mongoose = require('mongoose');
var Q = require("q");
var env = process.env.NODE_ENV || 'test';
var config = require('../../../../config/config')[env];

var chai = require("chai");
chai.should();
chai.use(require("chai-as-promised"));

var repositoryFactory = require("../../../../app/repository/RepositoryFactory").getRepositoryFactory();
var subcategoryRepository = null;

var subcategoryRepositoryData = require('./data/SubcategoryRepositoryData');


var initialize = function(){

	require('../../../../app/model/category/Subcategory').model();

	subcategoryRepository = repositoryFactory.getSubcategoryRepository();
};

/* PROTECTED REGION END */

/**
 * Unit tests for the repository SubcategoryRepository
 */
 
describe('SubcategoryRepository tests', function(){

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepositoryTest_SubcategoryRepositoryTest_config) ENABLED START */
	before(function(done){
		mongoose.connect('mongodb://'+config.mongo.host+':'+config.mongo.port+'/'+config.mongo.name+'', function (err, res) {
			if (err) throw err;

            initialize();

            done();
        });
	});
	
	after(function(done){
        mongoose.connection.db.dropDatabase(function(){
            mongoose.connection.close(done);
        });
    });
	/* PROTECTED REGION END */
	

		/**
		 * Unit test cases for the operation getAllSubcategories
		 */
		describe('getAllSubcategories function', function () {
			
			/**
			 * Test case for the normal flow of the operation getAllSubcategories
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepositoryTest_SubcategoryRepositoryTest__Operation_getAllSubcategories_body) ENABLED START */
			it('should be fulfilled', function(){

				var category = null;
				var optional = null;

				var promise = subcategoryRepository.getAllSubcategories(category, optional);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getAllSubcategories
			 */
			it('should be rejected', function(){
				
				var category = null;
				var optional = null;
				
				var promise = subcategoryRepository.getAllSubcategories(category, optional);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation createSubcategory
		 */
		describe('createSubcategory function', function () {
			
			/**
			 * Test case for the normal flow of the operation createSubcategory
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepositoryTest_SubcategoryRepositoryTest__Operation_createSubcategory_body) ENABLED START */
			it('should be fulfilled', function(){

				var category = null;
				var subcategory = null;

				var promise = subcategoryRepository.createSubcategory(category, subcategory);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation createSubcategory
			 */
			it('should be rejected', function(){
				
				var category = null;
				var subcategory = null;
				
				var promise = subcategoryRepository.createSubcategory(category, subcategory);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getSubcategoryByDesclubId
		 */
		describe('getSubcategoryByDesclubId function', function () {
			
			/**
			 * Test case for the normal flow of the operation getSubcategoryByDesclubId
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepositoryTest_SubcategoryRepositoryTest__Operation_getSubcategoryByDesclubId_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;

				var promise = subcategoryRepository.getSubcategoryByDesclubId(id);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getSubcategoryByDesclubId
			 */
			it('should be rejected', function(){
				
				var id = null;
				
				var promise = subcategoryRepository.getSubcategoryByDesclubId(id);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepositoryTest_SubcategoryRepositoryTest_additional) ENABLED START */
	/* PROTECTED REGION END */
});
