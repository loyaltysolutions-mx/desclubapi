/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentDataTestCategoryTest_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

module.exports.generate = function (id) {

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentDataTestCategoryTest_custom_subdocs) ENABLED START */
	/* PROTECTED REGION END */

    var Category = {
    	categoryId: id,
    	originalName: 'originalName' + id,
    	name: 'name' + id
    };

    /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentDataTestCategoryTest_custom) ENABLED START */
	/* PROTECTED REGION END */

    return Category;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentDataTestCategoryTest_additional) ENABLED START */
/* PROTECTED REGION END */
