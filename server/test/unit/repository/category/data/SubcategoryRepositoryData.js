/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentDataTestSubcategoryTest_init) ENABLED START */
var logger = console;
/* PROTECTED REGION END */

module.exports.generate = function (id) {

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentDataTestSubcategoryTest_custom_subdocs) ENABLED START */
	/* PROTECTED REGION END */

    var Subcategory = {
    	subcategoryId: id,
    	name: 'name' + id,
    	category: {}
    };

    /* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentDataTestSubcategoryTest_custom) ENABLED START */
	/* PROTECTED REGION END */

    return Subcategory;
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentDataTestSubcategoryTest_additional) ENABLED START */
/* PROTECTED REGION END */
