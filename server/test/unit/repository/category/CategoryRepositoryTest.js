/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepositoryTest_CategoryRepositoryTest_init) ENABLED START */
var logger = console;
var mongoose = require('mongoose');
var Q = require("q");
var env = process.env.NODE_ENV || 'test';
var config = require('../../../../config/config')[env];

var chai = require("chai");
chai.should();
chai.use(require("chai-as-promised"));

var repositoryFactory = require("../../../../app/repository/RepositoryFactory").getRepositoryFactory();
var categoryRepository = null;

var categoryRepositoryData = require('./data/CategoryRepositoryData');


var initialize = function(){

	require('../../../../app/model/category/Category').model();

	categoryRepository = repositoryFactory.getCategoryRepository();
};

/* PROTECTED REGION END */

/**
 * Unit tests for the repository CategoryRepository
 */
 
describe('CategoryRepository tests', function(){

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepositoryTest_CategoryRepositoryTest_config) ENABLED START */
	before(function(done){
		mongoose.connect('mongodb://'+config.mongo.host+':'+config.mongo.port+'/'+config.mongo.name+'', function (err, res) {
			if (err) throw err;

            initialize();

            done();
        });
	});
	
	after(function(done){
        mongoose.connection.db.dropDatabase(function(){
            mongoose.connection.close(done);
        });
    });
	/* PROTECTED REGION END */
	

		/**
		 * Unit test cases for the operation getAllCategories
		 */
		describe('getAllCategories function', function () {
			
			/**
			 * Test case for the normal flow of the operation getAllCategories
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepositoryTest_CategoryRepositoryTest__Operation_getAllCategories_body) ENABLED START */
			it('should be fulfilled', function(){

				var optional = null;

				var promise = categoryRepository.getAllCategories(optional);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getAllCategories
			 */
			it('should be rejected', function(){
				
				var optional = null;
				
				var promise = categoryRepository.getAllCategories(optional);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation createCategory
		 */
		describe('createCategory function', function () {
			
			/**
			 * Test case for the normal flow of the operation createCategory
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepositoryTest_CategoryRepositoryTest__Operation_createCategory_body) ENABLED START */
			it('should be fulfilled', function(){

				var category = null;

				var promise = categoryRepository.createCategory(category);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation createCategory
			 */
			it('should be rejected', function(){
				
				var category = null;
				
				var promise = categoryRepository.createCategory(category);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

		/**
		 * Unit test cases for the operation getCategoryById
		 */
		describe('getCategoryById function', function () {
			
			/**
			 * Test case for the normal flow of the operation getCategoryById
			 */
			/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepositoryTest_CategoryRepositoryTest__Operation_getCategoryById_body) ENABLED START */
			it('should be fulfilled', function(){

				var id = null;

				var promise = categoryRepository.getCategoryById(id);

				return Q.all([
	                promise.should.be.fulfilled
	            ]);
			});
			
			/**
			 * Test case for the exceptional flow of the operation getCategoryById
			 */
			it('should be rejected', function(){
				
				var id = null;
				
				var promise = categoryRepository.getCategoryById(id);

				return Q.all([
	                promise.should.be.rejected
	            ]);
			});
			/* PROTECTED REGION END */
		});

	/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_category_DocumentalRepositoryTest_CategoryRepositoryTest_additional) ENABLED START */
	/* PROTECTED REGION END */
});
