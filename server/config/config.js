/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_config_config_init) ENABLED START */

var path = require('path');
var rootPath = path.normalize(__dirname + '/..');

/* PROTECTED REGION END */

module.exports = {
  	development: {
  	 	loggerLevel: "debug",
  	 	root: rootPath,
  	 	tokenHeader: "Secure-Token",
  	 	mongo: {
  	 		host: "localhost",
  	 		port: "27017",
  	 		name: "DesclubAPI",
  	 		user: "",
  	 		password: ""
  	 	},
  	 	
		app: {
			name: 'DesclubAPI',
			host: 'localhost',
			port: 6543,
			inversePort: 6543,
			tokenExpiration: 3600000*2
		}
	},
	test: {
		loggerLevel: "debug",
		root: rootPath,
		tokenHeader: "Secure-Token",
		db: {
			dialect: "sqlite"
		},
	 	mongo: {
	 		host: "localhost",
	 		port: "27017",
	 		name: "DesclubAPI",
	 		user: "",
	 		password: ""
	 	},
		app: {
			name: 'DesclubAPI',
			host: 'localhost',
			port: 6543,
			inversePort: 6543,
			tokenExpiration: 3600000*2
		}
	},
	qa: {
  	 	loggerLevel: "info",
  	 	root: rootPath,
  	 	tokenHeader: "Secure-Token",
  	 	mongo: {
  	 		host: "localhost",
  	 		port: "27017",
  	 		name: "DesclubAPI",
  	 		user: "desclubapi",
  	 		password: "beepquest2015"
  	 	},
  	 	
		app: {
			name: 'DesclubAPI',
			host: 'desclubapi.beepquest.net',
			port: 6543,
			inversePort: 80,
			tokenExpiration: 3600000*2
		}
	},
	production: {
		loggerLevel: "info",
		root: rootPath,
		tokenHeader: "Secure-Token",
	mongo: {
		host: "localhost",
		port: "27017",
		name: "DesclubAPI",
		user: "",
		password: ""
	},
		app: {
			name: 'DesclubAPI',
			host: 'localhost',
			port: 6543,
			inversePort: 80,
			tokenExpiration: 3600000*2
		}
	}
};

/* PROTECTED REGION ID(DesclubAPI_mx.com.desclub_config_config_additional) ENABLED START */

module.exports.qa.loggerLevel = 'debug';
module.exports.production.loggerLevel = 'debug';

module.exports.development.businessEventHeader = 'business-event';
module.exports.test.businessEventHeader = 'business-event';
module.exports.qa.businessEventHeader = 'business-event';
module.exports.production.businessEventHeader = 'business-event';

module.exports.development.userHeader = 'gq-user';
module.exports.test.userHeader = 'gq-user';
module.exports.qa.userHeader = 'gq-user';
module.exports.production.userHeader = 'gq-user';

module.exports.development.countHeader = 'gq-count';
module.exports.test.countHeader = 'gq-count';
module.exports.qa.countHeader = 'gq-count';
module.exports.production.countHeader = 'gq-count';

/* PROTECTED REGION END */

